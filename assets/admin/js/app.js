'use strict';

$(function () {
  $('#sidebar__menuWrapper').slimScroll({
    height: 'calc(100vh - 86.75px)'
  });
});

$(function () {
  $('.dropdown-menu__body').slimScroll({
    height: '270px'
  });
});

// modal-dialog-scrollable
$(function () {
  $('.modal-dialog-scrollable .modal-body').slimScroll({
    height: '100%'
  });
});

// activity-list 
$(function () {
  $('.activity-list').slimScroll({
    height: '385px'
  });
});

// recent ticket list 
$(function () {
  $('.recent-ticket-list__body').slimScroll({
    height: '295px'
  });
});





$('#navbar-search__field').on('input', function () {
  var search = $(this).val().toLowerCase();


  var search_result_pane = $('.navbar_search_result');
  $(search_result_pane).html('');
  if (search.length == 0) {
    return;
  }

  // search
  var match = $('.sidebar__menu-wrapper .nav-link').filter(function (idx, elem) {
    return $(elem).text().trim().toLowerCase().indexOf(search) >= 0 ? elem : null;
  }).sort();




  // show search result
  // search not found
  if (match.length == 0) {
    $(search_result_pane).append('<li class="text-muted pl-5">No search result found.</li>');
    return;
  }
  // search found
  match.each(function (idx, elem) {
    var item_url = $(elem).attr('href') || $(elem).data('default-url');
    var item_text = $(elem).text().replace(/(\d+)/g, '').trim();
    $(search_result_pane).append(`<li><a href="${item_url}">${item_text}</a></li>`);
  });


});

let img = $('.bg_img');
img.css('background-image', function () {
  let bg = ('url(' + $(this).data('background') + ')');
  return bg;
});

const navTgg = $('.navbar__expand');
navTgg.on('click', function () {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active');
  $('.navbar-wrapper').toggleClass('active');
  $('.body-wrapper').toggleClass('active');
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

// navbar-search 
$('.navbar-search__btn-open').on('click', function () {
  $('.navbar-search').addClass('active');
});

$('.navbar-search__close').on('click', function () {
  $('.navbar-search').removeClass('active');
});

// responsive sidebar expand js 
$('.res-sidebar-open-btn').on('click', function () {
  $('.sidebar').addClass('open');
});

$('.res-sidebar-close-btn').on('click', function () {
  $('.sidebar').removeClass('open');
});

/* Get the documentElement (<html>) to display the page in fullscreen */
let elem = document.documentElement;

/* View in fullscreen */
function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

/* Close fullscreen */
function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen();
  }
}

$('.fullscreen-btn').on('click', function () {
  $(this).toggleClass('active');
});

$('.sidebar-dropdown > a').on('click', function () {
  if ($(this).parent().find('.sidebar-submenu').length) {
    if ($(this).parent().find('.sidebar-submenu').first().is(':visible')) {
      $(this).find('.side-menu__sub-icon').removeClass('transform rotate-180');
      $(this).removeClass('side-menu--open');
      $(this).parent().find('.sidebar-submenu').first().slideUp({
        done: function done() {
          $(this).removeClass('sidebar-submenu__open');
        }
      });
    } else {
      $(this).find('.side-menu__sub-icon').addClass('transform rotate-180');
      $(this).addClass('side-menu--open');
      $(this).parent().find('.sidebar-submenu').first().slideDown({
        done: function done() {
          $(this).addClass('sidebar-submenu__open');
        }
      });
    }
  }
});

// select-2 init
$('.select2-basic').select2();
$('.select2-multi-select').select2();
$(".select2-auto-tokenize").select2({
  tags: true,
  tokenSeparators: [',']
});


function proPicURL(input) {
  console.log("test")
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var preview = $(input).parents('.thumb').find('.profilePicPreview');
      $(preview).css('background-image', 'url(' + e.target.result + ')');
      $(preview).addClass('has-image');
      $(preview).hide();
      $(preview).fadeIn(650);
    }
    reader.readAsDataURL(input.files[0]);
  }
}
$(".profilePicUpload").on('change', function () {
  console.log("changed")
  proPicURL(this);
});

$(".remove-image").on('click', function () {
  $(this).parents(".profilePicPreview").css('background-image', 'none');
  $(this).parents(".profilePicPreview").removeClass('has-image');
  $(this).parents(".thumb").find('input[type=file]').val('');
});

$("form").on("change", ".file-upload-field", function () {
  $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
});




//Custom Data Table
$('.custom-data-table').closest('.card').prepend('<div class="card-header" style="border-bottom: none;"><div class="text-right"><div class="form-inline float-sm-right bg--white"><input type="text" name="search_table" class="form-control" placeholder="Search"></div></div></div>');
$('.custom-data-table').closest('.card').find('.card-body').attr('style', 'padding-top:0px');
var tr_elements = $('.custom-data-table tbody tr');
$(document).on('input', 'input[name=search_table]', function () {
  var search = $(this).val().toUpperCase();
  var match = tr_elements.filter(function (idx, elem) {
    return $(elem).text().trim().toUpperCase().indexOf(search) >= 0 ? elem : null;
  }).sort();
  var table_content = $('.custom-data-table tbody');
  if (match.length == 0) {
    table_content.html('<tr><td colspan="100%" class="text-center">Data Not Found</td></tr>');
  } else {
    table_content.html(match);
  }
});


function convertRGBAToRGBAString(rgb, alpha) {
  return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${alpha})`;
}


function addGlassEffect(rgbColor, element = undefined) {
  const colorTrigger = element ?? colorInputContainer.find(".color-panel-trigger");
  // Convert the base color to two different RGBA strings with varying alpha values for the gradient
  const colorTop = convertRGBAToRGBAString(rgbColor,
    0.5); // 50% transparency
  const colorBottom = convertRGBAToRGBAString(rgbColor,
    0.1); // 10% transparency

  // Update the CSS styles dynamically with the new color values
  colorTrigger.css('border-bottom',
    `1px solid rgba(${rgbColor.r}, ${rgbColor.g}, ${rgbColor.b}, 1)`
  );
  colorTrigger.css('background',
    `linear-gradient(180deg, ${colorTop} 24.31%, ${colorBottom} 90.92%)`);
  colorTrigger.css('box-shadow', '0px 4px 4px 0px rgba(0, 0, 0, 0.25)');
  colorTrigger.css('backdrop-filter', 'blur(15px)');
}


function rgbaStringToObject(rgbaStr) {
  // Use a regular expression to extract the numeric values from the string.
  const match = rgbaStr.match(/rgba?\((\d+),\s*(\d+),\s*(\d+),?\s*(\d*\.?\d+)?\)/i);

  if (match) {
    return {
      r: parseInt(match[1], 10),
      g: parseInt(match[2], 10),
      b: parseInt(match[3], 10),
      a: match[4] !== undefined ? parseFloat(match[4]) : 1 // Default alpha to 1 if not specified
    };
  } else {
    throw new Error('Invalid RGBA color string');
  }
}

function removeGlassEffect(element = undefined, color = undefined, no_color = undefined) {

  const colorTrigger = element ?? colorInputContainer.find(".color-panel-trigger");
  const colorValue = color ?? colorInputContainer.find(".color-value").val();
  const NoColorInputValue = no_color ?? colorInputContainer.find(".no-color-value").val();
  // Resetting the styles to remove the glass effect
  colorTrigger.css('border-bottom', ''); // Remove custom border-bottom
  colorTrigger.css('background', NoColorInputValue == 'true' ? '' :
    colorValue); // Remove custom background
  colorTrigger.css('box-shadow', ''); // Remove custom box-shadow
  colorTrigger.css('backdrop-filter', ''); // Remove backdrop-filter

}




function addGlassEffectWithHover(color, element, styleDataId, className) {
  debugger
  $(`[data-id='${styleDataId}']`).remove()
  // Convert the base color to two different RGBA strings with varying alpha values for the gradient
  const colorTop = convertRGBAToRGBAString(color, 0.5); // 50% transparency
  const colorBottom = convertRGBAToRGBAString(color, 0.1); // 10% transparency

  // Prepare the CSS for the hover effect
  const hoverCss = `
      .${className}:hover {
          border-bottom: 1px solid rgba(${color.r}, ${color.g}, ${color.b}, 1) !important;
          background: linear-gradient(180deg, ${colorTop} 24.31%, ${colorBottom} 90.92%) !important;
          box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25) !important;
          backdrop-filter: blur(15px) !important;
      }
  `;

  // Find or create the style element
  let styleElement = $(`style[data-id='${styleDataId}']`);
  if (styleElement.length === 0) {
    styleElement = $('<style>').attr('data-id', styleDataId).appendTo('head');
  }

  // Replace the content of the style element
  styleElement.html(hoverCss);

  // Ensure the element has the specified class
  element.addClass(className);
}


function removeHoverGlassEffect(dataId) {
  $(`[data-id='${dataId}']`).remove()
}


function removeStyleByDataId(dataId) {
  $(`[data-id='${dataId}']`).remove()
}

function addHoverColorChange(color, element, dataId, className) {
  // Prepare the CSS for the hover effect with the specified color
  $(`[data-id='${dataId}']`).remove()
  const hoverCss = `
      .${className}:hover {
          color: ${color} !important;
      }
  `;

  // Find or create the style element with the given data-id
  let styleElement = $(`style[data-id='${dataId}']`);
  if (styleElement.length === 0) {
    // Create a new style element if it doesn't exist
    styleElement = $('<style>')
      .attr('data-id', dataId)
      .appendTo('head');
  }

  // Update or set the content of the style element
  styleElement.html(hoverCss);

  // Ensure the target element has the specified class
  element.addClass(className);
}




function convertToRgba(color) {
  // Function to handle hexadecimal input
  function hexToRgba(hex) {
    hex = hex.replace(/^#/, '');
    let r, g, b, a = 1;

    if (hex.length === 3) {
      r = parseInt(hex.charAt(0) + hex.charAt(0), 16);
      g = parseInt(hex.charAt(1) + hex.charAt(1), 16);
      b = parseInt(hex.charAt(2) + hex.charAt(2), 16);
    } else if (hex.length === 6) {
      r = parseInt(hex.slice(0, 2), 16);
      g = parseInt(hex.slice(2, 4), 16);
      b = parseInt(hex.slice(4, 6), 16);
    } else if (hex.length === 8) {
      r = parseInt(hex.slice(0, 2), 16);
      g = parseInt(hex.slice(2, 4), 16);
      b = parseInt(hex.slice(4, 6), 16);
      a = parseInt(hex.slice(6, 8), 16) / 255;
    } else {
      throw new Error('Invalid Hex Color Format');
    }
    return `rgba(${r}, ${g}, ${b}, ${a})`;
  }

  // Function to handle RGB or RGBA input as an array or string
  function rgbToRgba(rgb) {
    let parts = rgb.includes(',') ? rgb.split(',') : rgb;
    parts = parts.map(part => parseInt(part.trim(), 10));

    if (parts.length === 3) {
      parts.push(1); // Add default alpha value if not present
    } else if (parts.length !== 4) {
      throw new Error('Invalid RGB(A) Color Format');
    }
    return `rgba(${parts[0]}, ${parts[1]}, ${parts[2]}, ${parts[3]})`;
  }

  // Determine the type of input and process it
  if (typeof color === 'string') {
    if (color.startsWith('#')) {
      return hexToRgba(color);
    } else if (color.startsWith('rgb')) {
      return rgbToRgba(color.replace(/[rgba()]/g, ''));
    } else {
      throw new Error('Unsupported Color Format');
    }
  } else if (Array.isArray(color)) {
    return rgbToRgba(color);
  } else {
    throw new Error('Unsupported Color Format');
  }
}


function identifyColorFormat(color) {
  // Regular expression for checking hex color format
  const hexPattern = /^#?([0-9A-F]{3}){1,2}$/i;

  // Regular expression for checking RGB or RGBA color format
  const rgbPattern = /^rgba?\(\d{1,3},\s*\d{1,3},\s*\d{1,3}(?:,\s*[\d.]+)?\)$/i;

  // Check if the color matches the hex pattern
  if (hexPattern.test(color)) {
    return "hex";
  }

  // Check if the color matches the RGB or RGBA pattern
  if (rgbPattern.test(color)) {
    return "rgb";
  }

  // Return 'unknown' if the format does not match known patterns
  return "unknown";
}