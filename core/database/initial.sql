-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: jamie2
-- ------------------------------------------------------
-- Server version	8.0.36-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_notifications`
--

# DROP TABLE IF EXISTS `admin_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_notifications` (
                                       `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                       `user_id` int unsigned NOT NULL DEFAULT '0',
                                       `merchant_id` int unsigned NOT NULL DEFAULT '0',
                                       `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                       `read_status` tinyint(1) NOT NULL DEFAULT '0',
                                       `click_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                       `created_at` timestamp NULL DEFAULT NULL,
                                       `updated_at` timestamp NULL DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52335 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_notifications`
--

LOCK TABLES `admin_notifications` WRITE;
/*!40000 ALTER TABLE `admin_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_password_resets`
--

# DROP TABLE IF EXISTS `admin_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_password_resets` (
                                         `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                         `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `token` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `status` tinyint(1) NOT NULL DEFAULT '1',
                                         `created_at` timestamp NULL DEFAULT NULL,
                                         `updated_at` timestamp NULL DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_password_resets`
--

LOCK TABLES `admin_password_resets` WRITE;
/*!40000 ALTER TABLE `admin_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

# DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
                          `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                          `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `email_verified_at` timestamp NULL DEFAULT NULL,
                          `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                          `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `created_at` timestamp NULL DEFAULT NULL,
                          `updated_at` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `email` (`email`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Super Admin','david@mcultivo.com','auction_admin','2022-07-22 11:51:27','62209217f0ef11646301719.jpg','$2y$10$fEZd4r6O.brfxolXUNueEOI0/juRBwimkk4D.E.EREu4KDfH/g1My',NULL,'2022-08-03 22:11:49'),(2,'Admin','jamie@mcultivo.com','jamie_admin','2022-07-23 13:00:29','62209217f0ef11646301719.jpg','$2y$10$cf0MkDgg/YBFmFa9584S7eE7eJ7UuXrBi0Fz4mQszxGpUyw1QuXFO',NULL,'2023-09-21 12:59:36'),(3,'Melanie','melanie@mcultivo.com','melanie_admin','2022-07-23 13:16:10','62209217f0ef11646301719.jpg','$2y$10$9pn46w75.E5LWrFqdhlTZulsw1x/A4WIclmwcEE/6S2H9MBz40qPq',NULL,'2022-09-01 14:26:42');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advertisements`
--

# DROP TABLE IF EXISTS `advertisements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `advertisements` (
                                  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                  `type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                  `status` int NOT NULL DEFAULT '1',
                                  `size` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                  `redirect_url` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
                                  `impression` int NOT NULL DEFAULT '0',
                                  `click` int NOT NULL DEFAULT '0',
                                  `created_at` timestamp NULL DEFAULT NULL,
                                  `updated_at` timestamp NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisements`
--

LOCK TABLES `advertisements` WRITE;
/*!40000 ALTER TABLE `advertisements` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertisements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auto_bid_settings`
--

# DROP TABLE IF EXISTS `auto_bid_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auto_bid_settings` (
                                     `id` bigint NOT NULL AUTO_INCREMENT,
                                     `product_id` int NOT NULL,
                                     `user_id` int NOT NULL,
                                     `step` decimal(28,2) NOT NULL DEFAULT '0.00',
                                     `max_value` decimal(28,2) NOT NULL,
                                     `status` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT 'active',
                                     `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                     `updated_at` timestamp NULL DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2521 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auto_bid_settings`
--

LOCK TABLES `auto_bid_settings` WRITE;
/*!40000 ALTER TABLE `auto_bid_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `auto_bid_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bids`
--

# DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bids` (
                        `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                        `product_id` int unsigned NOT NULL DEFAULT '0',
                        `user_id` int unsigned NOT NULL DEFAULT '0',
                        `amount` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                        `prev_amount` decimal(28,8) DEFAULT NULL,
                        `user_updated` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                        `User_Bids_Count` int NOT NULL DEFAULT '1',
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7045 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 CREATE TRIGGER `bids_before_insert` BEFORE INSERT ON `bids` FOR EACH ROW BEGIN
    DECLARE last_Bid DECIMAL (28,2);
    set last_Bid = (SELECT IFNULL(MAX(amount),(SELECT price FROM products WHERE id = NEW.product_id )) FROM bids WHERE product_id = NEW.product_id);

    INSERT INTO bids_history
    (product_id, user_id, new_bid, previous_bid, user_previous_bid)
    values
        (NEW.product_id, NEW.user_id, NEW.amount, last_bid, 0);
END */;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 CREATE TRIGGER `bids_before_update` BEFORE UPDATE ON `bids` FOR EACH ROW BEGIN
    DECLARE last_Bid DECIMAL (28,2);
    DECLARE user_last_Bid DECIMAL (28,2);

    IF (NEW.prev_amount = 1) THEN
        SET NEW.User_Bids_Count = OLD.User_Bids_Count +1;

        set last_Bid = (SELECT IFNULL(MAX(amount),0) FROM bids WHERE product_id = NEW.product_id);
        set user_last_Bid = (SELECT IFNULL(MAX(amount),0) FROM bids WHERE product_id = NEW.product_id AND user_id=NEW.user_id);
        INSERT INTO bids_history
        (product_id, user_id, new_bid, previous_bid, user_previous_bid)
        VALUES
            (NEW.product_id, NEW.user_id, NEW.amount, last_bid, user_last_bid);
    ELSE
        IF (NEW.prev_amount = -1) THEN
            SET NEW.User_Bids_Count = OLD.User_Bids_Count -1;
        END IF;
    END IF;
END */;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `bids_history`
--

# DROP TABLE IF EXISTS `bids_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bids_history` (
                                `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                `user_id` int NOT NULL,
                                `product_id` int NOT NULL,
                                `new_bid` decimal(28,2) NOT NULL DEFAULT '0.00',
                                `previous_bid` decimal(28,2) NOT NULL DEFAULT '0.00',
                                `user_previous_bid` decimal(28,2) NOT NULL DEFAULT '0.00',
                                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                `updated_at` timestamp NULL DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12213464 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids_history`
--

LOCK TABLES `bids_history` WRITE;
/*!40000 ALTER TABLE `bids_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `bids_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `bids_history_all_data`
--

# DROP TABLE IF EXISTS `bids_history_all_data`;
/*!50001 DROP VIEW IF EXISTS `bids_history_all_data`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `bids_history_all_data` AS SELECT
                                                    1 AS `ID`,
                                                    1 AS `user_id`,
                                                    1 AS `product_id`,
                                                    1 AS `event_id`,
                                                    1 AS `firstname`,
                                                    1 AS `lastname`,
                                                    1 AS `company_name`,
                                                    1 AS `billing_country`,
                                                    1 AS `billing_state`,
                                                    1 AS `billing_city`,
                                                    1 AS `billing_address_1`,
                                                    1 AS `event_name`,
                                                    1 AS `product_name`,
                                                    1 AS `new_bid`,
                                                    1 AS `previous_bid`,
                                                    1 AS `user_previous_bid`,
                                                    1 AS `created_at`,
                                                    1 AS `updated_at`,
                                                    1 AS `RANK`,
                                                    1 AS `Name`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `budgets`
--

# DROP TABLE IF EXISTS `budgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `budgets` (
                           `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                           `user_id` bigint unsigned NOT NULL,
                           `event_id` int NOT NULL,
                           `budget` double(8,2) unsigned NOT NULL,
                           `created_at` timestamp NULL DEFAULT NULL,
                           `updated_at` timestamp NULL DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `budgets_user_id_event_id_unique` (`user_id`,`event_id`),
                           KEY `budgets_user_id_index` (`user_id`),
                           KEY `budgets_event_id_index` (`event_id`),
                           CONSTRAINT `budgets_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
                           CONSTRAINT `budgets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budgets`
--

LOCK TABLES `budgets` WRITE;
/*!40000 ALTER TABLE `budgets` DISABLE KEYS */;
/*!40000 ALTER TABLE `budgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

# DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
                              `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                              `position` int DEFAULT NULL,
                              `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                              `icon` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: Inactive, 1: Active',
                              `created_at` timestamp NULL DEFAULT NULL,
                              `updated_at` timestamp NULL DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

# DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
                             `id` int NOT NULL AUTO_INCREMENT,
                             `Name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT '',
                             `Code` varchar(3) NOT NULL DEFAULT '',
                             `dial_code` varchar(20) DEFAULT NULL,
                             `Default_Region` varchar(100) DEFAULT '',
                             `Default_Region_ID` int DEFAULT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `Name` (`Name`),
                             UNIQUE KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan','AFG',NULL,'Southern Asia',5),(2,'Albania','ALB',NULL,'Southern Europe',4),(3,'Algeria','DZA',NULL,'Northern Africa',12),(4,'Andorra','AND',NULL,'Southern Europe',4),(5,'Angola','AGO',NULL,'Middle Africa',13),(6,'Antigua and Barbuda','ATG',NULL,'Caribbean',21),(7,'Argentina','ARG',NULL,'South America',8),(8,'Armenia','ARM',NULL,'Western Asia',2),(9,'Australia','AUS',NULL,'Australia and New Zealand',22),(10,'Austria','AUT',NULL,'Western Europe',1),(11,'Azerbaijan','AZE',NULL,'Western Asia',2),(12,'Bahamas','BHS',NULL,'Caribbean',21),(13,'Bahrain','BHR',NULL,'Western Asia',2),(14,'Bangladesh','BGD',NULL,'Southern Asia',5),(15,'Barbados','BRB',NULL,'Caribbean',21),(16,'Belarus','BLR',NULL,'Eastern Europe',16),(17,'Belgium','BEL',NULL,'Western Europe',1),(18,'Belize','BLZ',NULL,'Central America',20),(19,'Benin','BEN',NULL,'Western Africa',3),(20,'Bhutan','BTN',NULL,'Southern Asia',5),(21,'Bolivia','BOL',NULL,'South America',8),(22,'Bosnia and Herzegovina','BIH',NULL,'Southern Europe',4),(23,'Botswana','BWA',NULL,'Southern Africa',6),(24,'Brazil','BRA',NULL,'South America',8),(25,'Brunei','BRN',NULL,'South-eastern Asia',7),(26,'Bulgaria','BGR',NULL,'Eastern Europe',16),(27,'Burkina Faso','BFA',NULL,'Western Africa',3),(28,'Burundi','BDI',NULL,'Eastern Africa',18),(29,'Cote d\'Ivoire','CIV',NULL,'Western Africa',3),(30,'Cabo Verde','CPV',NULL,'Western Africa',3),(31,'Cambodia','KHM',NULL,'South-eastern Asia',7),(32,'Cameroon','CMR',NULL,'Middle Africa',13),(33,'Canada','CAN',NULL,'Northern America',11),(34,'Central African Republic','CAF',NULL,'Middle Africa',13),(35,'Chad','TCD',NULL,'Middle Africa',13),(36,'Chile','CHL',NULL,'South America',8),(37,'China','CHN',NULL,'Eastern Asia',17),(38,'Colombia','COL',NULL,'South America',8),(39,'Comoros','COM',NULL,'Eastern Africa',18),(40,'Congo (Congo-Brazzaville)','COG',NULL,'Middle Africa',13),(41,'Costa Rica','CRI',NULL,'Central America',20),(42,'Croatia','HRV',NULL,'Southern Europe',4),(43,'Cuba','CUB',NULL,'Caribbean',21),(44,'Cyprus','CYP',NULL,'Western Asia',2),(45,'Czechia (Czech Republic)','CZE',NULL,'Eastern Europe',16),(46,'Democratic Republic of the Congo','COD',NULL,'Middle Africa',13),(47,'Denmark','DNK',NULL,'Northern Europe',10),(48,'Djibouti','DJI',NULL,'Eastern Africa',18),(49,'Dominica','DMA',NULL,'Caribbean',21),(50,'Dominican Republic','DOM',NULL,'Caribbean',21),(51,'Ecuador','ECU',NULL,'South America',8),(52,'Egypt','EGY',NULL,'Northern Africa',12),(53,'El Salvador','SLV',NULL,'Central America',20),(54,'Equatorial Guinea','GNQ',NULL,'Middle Africa',13),(55,'Eritrea','ERI',NULL,'Eastern Africa',18),(56,'Estonia','EST',NULL,'Northern Europe',10),(57,'Eswatini (fmr. Swaziland)','SWZ',NULL,'Southern Africa',6),(58,'Ethiopia','ETH',NULL,'Eastern Africa',18),(59,'Fiji','FJI',NULL,'Melanesia',15),(60,'Finland','FIN',NULL,'Northern Europe',10),(61,'France','FRA',NULL,'Western Europe',1),(62,'Gabon','GAB',NULL,'Middle Africa',13),(63,'Gambia','GMB',NULL,'Western Africa',3),(64,'Georgia','GEO',NULL,'Western Asia',2),(65,'Germany','DEU',NULL,'Western Europe',1),(66,'Ghana','GHA',NULL,'Western Africa',3),(67,'Greece','GRC',NULL,'Southern Europe',4),(68,'Grenada','GRD',NULL,'Caribbean',21),(69,'Guatemala','GTM',NULL,'Central America',20),(70,'Guinea','GIN',NULL,'Western Africa',3),(71,'Guinea-Bissau','GNB',NULL,'Western Africa',3),(72,'Guyana','GUY',NULL,'South America',8),(73,'Haiti','HTI',NULL,'Caribbean',21),(74,'Holy See','VAT',NULL,'Southern Europe',4),(75,'Honduras','HND',NULL,'Central America',20),(76,'Hungary','HUN',NULL,'Eastern Europe',16),(77,'Iceland','ISL',NULL,'Northern Europe',10),(78,'India','IND',NULL,'Southern Asia',5),(79,'Indonesia','IDN',NULL,'South-eastern Asia',7),(80,'Iran','IRN',NULL,'Southern Asia',5),(81,'Iraq','IRQ',NULL,'Western Asia',2),(82,'Ireland','IRL',NULL,'Northern Europe',10),(83,'Israel','ISR',NULL,'Western Asia',2),(84,'Italy','ITA',NULL,'Southern Europe',4),(85,'Jamaica','JAM',NULL,'Caribbean',21),(86,'Japan','JPN',NULL,'Eastern Asia',17),(87,'Jordan','JOR',NULL,'Western Asia',2),(88,'Kazakhstan','KAZ',NULL,'Central Asia',19),(89,'Kenya','KEN',NULL,'Eastern Africa',18),(90,'Kiribati','KIR',NULL,'Micronesia',14),(91,'Kuwait','KWT',NULL,'Western Asia',2),(92,'Kyrgyzstan','KGZ',NULL,'Central Asia',19),(93,'Laos','LAO',NULL,'South-eastern Asia',7),(94,'Latvia','LVA',NULL,'Northern Europe',10),(95,'Lebanon','LBN',NULL,'Western Asia',2),(96,'Lesotho','LSO',NULL,'Southern Africa',6),(97,'Liberia','LBR',NULL,'Western Africa',3),(98,'Libya','LBY',NULL,'Northern Africa',12),(99,'Liechtenstein','LIE',NULL,'Western Europe',1),(100,'Lithuania','LTU',NULL,'Northern Europe',10),(101,'Luxembourg','LUX',NULL,'Western Europe',1),(102,'Madagascar','MDG',NULL,'Eastern Africa',18),(103,'Malawi','MWI',NULL,'Eastern Africa',18),(104,'Malaysia','MYS',NULL,'South-eastern Asia',7),(105,'Maldives','MDV',NULL,'Southern Asia',5),(106,'Mali','MLI',NULL,'Western Africa',3),(107,'Malta','MLT',NULL,'Southern Europe',4),(108,'Marshall Islands','MHL',NULL,'Micronesia',14),(109,'Mauritania','MRT',NULL,'Western Africa',3),(110,'Mauritius','MUS',NULL,'Eastern Africa',18),(111,'Mexico','MEX',NULL,'Central America',20),(112,'Micronesia','FSM',NULL,'Micronesia',14),(113,'Moldova','MDA',NULL,'Eastern Europe',16),(114,'Monaco','MCO',NULL,'Western Europe',1),(115,'Mongolia','MNG',NULL,'Eastern Asia',17),(116,'Montenegro','MNE',NULL,'Southern Europe',4),(117,'Morocco','MAR',NULL,'Northern Africa',12),(118,'Mozambique','MOZ',NULL,'Eastern Africa',18),(119,'Myanmar (formerly Burma)','MMR',NULL,'South-eastern Asia',7),(120,'Namibia','NAM',NULL,'Southern Africa',6),(121,'Nauru','NRU',NULL,'Micronesia',14),(122,'Nepal','NPL',NULL,'Southern Asia',5),(123,'Netherlands','NLD',NULL,'Western Europe',1),(124,'New Zealand','NZL',NULL,'Australia and New Zealand',22),(125,'Nicaragua','NIC',NULL,'Central America',20),(126,'Niger','NER',NULL,'Western Africa',3),(127,'Nigeria','NGA',NULL,'Western Africa',3),(128,'North Korea','KOR',NULL,'Eastern Asia',17),(129,'North Macedonia','MKD',NULL,'Southern Europe',4),(130,'Norway','NOR',NULL,'Northern Europe',10),(131,'Oman','OMN',NULL,'Western Asia',2),(132,'Pakistan','PAK',NULL,'Southern Asia',5),(133,'Palau','PLW',NULL,'Micronesia',14),(134,'Palestine State','PSE',NULL,'Western Asia',2),(135,'Panama','PAN',NULL,'Central America',20),(136,'Papua New Guinea','PNG',NULL,'Melanesia',15),(137,'Paraguay','PRY',NULL,'South America',8),(138,'Peru','PER',NULL,'South America',8),(139,'Philippines','PHL',NULL,'South-eastern Asia',7),(140,'Poland','POL',NULL,'Eastern Europe',16),(141,'Portugal','PRT',NULL,'Southern Europe',4),(142,'Qatar','QAT',NULL,'Western Asia',2),(143,'Romania','ROU',NULL,'Eastern Europe',16),(144,'Russia','RUS',NULL,'Eastern Europe',16),(145,'Rwanda','RWA',NULL,'Eastern Africa',18),(146,'Saint Kitts and Nevis','KNA',NULL,'Caribbean',21),(147,'Saint Lucia','LCA',NULL,'Caribbean',21),(148,'Saint Vincent and the Grenadines','VCT',NULL,'Caribbean',21),(149,'Samoa','WSM',NULL,'Polynesia',9),(150,'San Marino','SMR',NULL,'Southern Europe',4),(151,'Sao Tome and Principe','STP',NULL,'Middle Africa',13),(152,'Saudi Arabia','SAU',NULL,'Western Asia',2),(153,'Senegal','SEN',NULL,'Western Africa',3),(154,'Serbia','SRB',NULL,'Southern Europe',4),(155,'Seychelles','SYC',NULL,'Eastern Africa',18),(156,'Sierra Leone','SLE',NULL,'Western Africa',3),(157,'Singapore','SGP',NULL,'South-eastern Asia',7),(158,'Slovakia','SVK',NULL,'Eastern Europe',16),(159,'Slovenia','SVN',NULL,'Southern Europe',4),(160,'Solomon Islands','SLB',NULL,'Melanesia',15),(161,'Somalia','SOM',NULL,'Eastern Africa',18),(162,'South Africa','ZAF',NULL,'Southern Africa',6),(163,'South Korea','PRK',NULL,'Eastern Asia',17),(164,'South Sudan','SSD',NULL,'Eastern Africa',18),(165,'Spain','ESP',NULL,'Southern Europe',4),(166,'Sri Lanka','LKA',NULL,'Southern Asia',5),(167,'Sudan','SDN',NULL,'Northern Africa',12),(168,'Suriname','SUR',NULL,'South America',8),(169,'Sweden','SWE',NULL,'Northern Europe',10),(170,'Switzerland','CHE',NULL,'Western Europe',1),(171,'Syria','SYR',NULL,'Western Asia',2),(172,'Tajikistan','TJK',NULL,'Central Asia',19),(173,'Tanzania','TZA',NULL,'Eastern Africa',18),(174,'Thailand','THA',NULL,'South-eastern Asia',7),(175,'Timor-Leste','TLS',NULL,'South-eastern Asia',7),(176,'Togo','TGO',NULL,'Western Africa',3),(177,'Tonga','TON',NULL,'Polynesia',9),(178,'Trinidad and Tobago','TTO',NULL,'Caribbean',21),(179,'Tunisia','TUN',NULL,'Northern Africa',12),(180,'Turkey','TUR',NULL,'Western Asia',2),(181,'Turkmenistan','TKM',NULL,'Central Asia',19),(182,'Tuvalu','TUV',NULL,'Polynesia',9),(183,'Uganda','UGA',NULL,'Eastern Africa',18),(184,'Ukraine','UKR',NULL,'Eastern Europe',16),(185,'United Arab Emirates','ARE',NULL,'Western Asia',2),(186,'United Kingdom','GBR',NULL,'Northern Europe',10),(187,'United States of America','USA',NULL,'Northern America',11),(188,'Uruguay','URY',NULL,'South America',8),(189,'Uzbekistan','UZB',NULL,'Central Asia',19),(190,'Vanuatu','VUT',NULL,'Melanesia',15),(191,'Venezuela','VEN',NULL,'South America',8),(192,'Vietnam','VNM',NULL,'South-eastern Asia',7),(193,'Yemen','YEM',NULL,'Western Asia',2),(194,'Zambia','ZMB',NULL,'Eastern Africa',18),(195,'Zimbabwe','ZWE',NULL,'Eastern Africa',18),(196,'Taiwan','TWN',NULL,'Eastern Asia',17),(197,'Hong Kong','HKG',NULL,'Eastern Asia',17);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `default_regions`
--

# DROP TABLE IF EXISTS `default_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `default_regions` (
                                   `id` int NOT NULL AUTO_INCREMENT,
                                   `name` varchar(100) NOT NULL DEFAULT '0',
                                   `default` tinyint(1) NOT NULL DEFAULT '0',
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `default_regions`
--

LOCK TABLES `default_regions` WRITE;
/*!40000 ALTER TABLE `default_regions` DISABLE KEYS */;
INSERT INTO `default_regions` VALUES (1,'Western Europe',0),(2,'Western Asia',0),(3,'Western Africa',0),(4,'Southern Europe',0),(5,'Southern Asia',0),(6,'Southern Africa',0),(7,'South-eastern Asia',0),(8,'South America',0),(9,'Polynesia',0),(10,'Northern Europe',0),(11,'Northern America',0),(12,'Northern Africa',0),(13,'Middle Africa',0),(14,'Micronesia',0),(15,'Melanesia',0),(16,'Eastern Europe',0),(17,'Eastern Asia',0),(18,'Eastern Africa',0),(19,'Central Asia',0),(20,'Central America',0),(21,'Caribbean',0),(22,'Australia and New Zealand',0),(24,'Rest of the World',1);
/*!40000 ALTER TABLE `default_regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deposits`
--

# DROP TABLE IF EXISTS `deposits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `deposits` (
                            `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                            `user_id` int unsigned NOT NULL,
                            `method_code` int unsigned NOT NULL,
                            `amount` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                            `method_currency` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `charge` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                            `rate` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                            `final_amo` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                            `detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `btc_amo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `btc_wallet` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `trx` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `try` int NOT NULL DEFAULT '0',
                            `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=>success, 2=>pending, 3=>cancel',
                            `from_api` tinyint(1) NOT NULL DEFAULT '0',
                            `admin_feedback` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deposits`
--

LOCK TABLES `deposits` WRITE;
/*!40000 ALTER TABLE `deposits` DISABLE KEYS */;
/*!40000 ALTER TABLE `deposits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_logs`
--

# DROP TABLE IF EXISTS `email_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `email_logs` (
                              `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                              `user_id` int unsigned NOT NULL,
                              `merchant_id` int unsigned NOT NULL DEFAULT '0',
                              `mail_sender` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `email_from` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `email_to` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                              `created_at` timestamp NULL DEFAULT NULL,
                              `updated_at` timestamp NULL DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4810 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_logs`
--

LOCK TABLES `email_logs` WRITE;
/*!40000 ALTER TABLE `email_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_sms_templates`
--

# DROP TABLE IF EXISTS `email_sms_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `email_sms_templates` (
                                       `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                       `act` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `subj` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `email_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                       `cc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                       `sms_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                       `shortcodes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `email_status` tinyint(1) NOT NULL DEFAULT '1',
                                       `sms_status` tinyint(1) NOT NULL DEFAULT '1',
                                       `created_at` timestamp NULL DEFAULT NULL,
                                       `updated_at` timestamp NULL DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_sms_templates`
--

LOCK TABLES `email_sms_templates` WRITE;
/*!40000 ALTER TABLE `email_sms_templates` DISABLE KEYS */;
INSERT INTO `email_sms_templates` VALUES (1,'PASS_RESET_CODE','Password Reset','Password Reset','<div>We have received a request to reset the password for your account on <b>{{time}} .<br></b></div><div>Requested From IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}} </b>.</div><div><br></div><br><div><div><div>Your account recovery code is:&nbsp;&nbsp; <font size=\"6\"><b>{{code}}</b></font></div><div><br></div></div></div><div><br></div><div><font size=\"4\" color=\"#CC0000\">If you do not wish to reset your password, please disregard this message.&nbsp;</font><br></div><br>',NULL,'Your account recovery code is: {{code}}',' {\"code\":\"Password Reset Code\",\"ip\":\"IP of User\",\"browser\":\"Browser of User\",\"operating_system\":\"Operating System of User\",\"time\":\"Request Time\"}',1,1,'2019-09-25 01:04:05','2021-01-06 01:49:06'),(2,'PASS_RESET_DONE','Password Reset Confirmation','You have Reset your password','<div><p>\r\n    You have successfully reset your password.</p><p>You changed from&nbsp; IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}}&nbsp;</b> on <b>{{time}}</b></p><p><b><br></b></p><p><font color=\"#FF0000\"><b>If you did not make this change, please contact with M-Cultivo support at support@mcultivo.com.</b></font><br></p></div>',NULL,'Your password has been changed successfully','{\"ip\":\"IP of User\",\"browser\":\"Browser of User\",\"operating_system\":\"Operating System of User\",\"time\":\"Request Time\"}',1,1,'2019-09-25 01:04:05','2022-06-29 20:59:03'),(3,'EVER_CODE','Email Verification','Please verify your email address','<div><br></div><div>Thank you for joining M-Cultivo Auctions.&nbsp;<br></div><div>Please use below code to verify your email address.<br></div><div><br></div><div>Your email verification code is:<font size=\"6\"><b> {{code}}</b></font></div>',NULL,'Your email verification code is: {{code}}','{\"code\":\"Verification code\"}',1,1,'2019-09-25 01:04:05','2022-06-29 20:59:46'),(4,'SVER_CODE','SMS Verification ','Please verify your phone','Your phone verification code is: {{code}}',NULL,'Your phone verification code is: {{code}}','{\"code\":\"Verification code\"}',0,1,'2019-09-25 01:04:05','2020-03-08 02:28:52'),(5,'2FA_ENABLE','Google Two Factor - Enable','Google Two Factor Authentication is now  Enabled for Your Account','<div>You just enabled Google Two Factor Authentication for Your Account.</div><div><br></div><div>Enabled at <b>{{time}} </b>From IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}} </b>.</div>',NULL,'Your verification code is: {{code}}','{\"ip\":\"IP of User\",\"browser\":\"Browser of User\",\"operating_system\":\"Operating System of User\",\"time\":\"Request Time\"}',1,1,'2019-09-25 01:04:05','2020-03-08 02:42:59'),(6,'2FA_DISABLE','Google Two Factor Disable','Google Two Factor Authentication is now  Disabled for Your Account','<div>You just disabled Google Two Factor Authentication for your account.</div><div><br></div><div>Disabled at <b>{{time}} </b>From IP: <b>{{ip}}</b> using <b>{{browser}}</b> on <b>{{operating_system}} </b>.</div>',NULL,'Google two factor verification is disabled','{\"ip\":\"IP of User\",\"browser\":\"Browser of User\",\"operating_system\":\"Operating System of User\",\"time\":\"Request Time\"}',1,1,'2019-09-25 01:04:05','2022-06-29 21:01:28'),(16,'ADMIN_SUPPORT_REPLY','Support Ticket Reply ','Reply Support Ticket','<div><p><span style=\"font-size: 11pt;\" data-mce-style=\"font-size: 11pt;\"><strong>A member from our support team has replied to the following ticket:</strong></span></p><p><b><span style=\"font-size: 11pt;\" data-mce-style=\"font-size: 11pt;\"><strong><br></strong></span></b></p><p><b>[Ticket#{{ticket_id}}] {{ticket_subject}}<br><br>Click here to reply:&nbsp; {{link}}</b></p><p>----------------------------------------------</p><p>Here is the reply : <br></p><p> {{reply}}<br></p></div><div><br></div>',NULL,'{{subject}}\r\n\r\n{{reply}}\r\n\r\n\r\nClick here to reply:  {{link}}','{\"ticket_id\":\"Support Ticket ID\", \"ticket_subject\":\"Subject Of Support Ticket\", \"reply\":\"Reply from Staff/Admin\",\"link\":\"Ticket URL For relpy\"}',0,1,'2020-06-08 20:00:00','2022-06-29 21:06:55'),(206,'DEPOSIT_COMPLETE','Automated Deposit - Successful','Deposit Completed Successfully','<div>Your deposit of <b>{{amount}} {{currency}}</b> is via&nbsp; <b>{{method_name}} </b>has been completed Successfully.<b><br></b></div><div><b><br></b></div><div><b>Details of your Deposit :<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color=\"#000000\">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>Payable : {{method_amount}} {{method_currency}} <br></div><div>Paid via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><font size=\"5\"><b><br></b></font></div><div><font size=\"5\">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br><br></div>',NULL,'{{amount}} {{currrency}} Deposit successfully by {{gateway_name}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By user\",\"charge\":\"Gateway Charge\",\"currency\":\"Site Currency\",\"rate\":\"Conversion Rate\",\"method_name\":\"Deposit Method Name\",\"method_currency\":\"Deposit Method Currency\",\"method_amount\":\"Deposit Method Amount After Conversion\", \"post_balance\":\"Users Balance After this operation\"}',0,1,'2020-06-24 20:00:00','2022-07-13 08:31:06'),(207,'DEPOSIT_REQUEST','Manual Deposit - User Requested','Deposit Request Submitted Successfully','<div>Your deposit request of <b>{{amount}} {{currency}}</b>&nbsp;via&nbsp; <b>{{method_name}} </b>was<b>&nbsp;</b>submitted successfully<b> .<br></b></div><div><b><br></b></div><div><b>Details of your Deposit:<br></b></div><div><br></div><div>Amou<font color=\"#000000\">nt : {{amount}} {{currency}}</font></div><div><font color=\"#000000\">Charge: {{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>Payable : {{method_amount}} {{method_currency}} <br></div><div>Pay via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div><div><br></div>',NULL,'{{amount}} Deposit requested by {{method}}. Charge: {{charge}} . Trx: {{trx}}\r\n','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By user\",\"charge\":\"Gateway Charge\",\"currency\":\"Site Currency\",\"rate\":\"Conversion Rate\",\"method_name\":\"Deposit Method Name\",\"method_currency\":\"Deposit Method Currency\",\"method_amount\":\"Deposit Method Amount After Conversion\"}',0,1,'2020-05-31 20:00:00','2022-07-13 08:27:44'),(208,'DEPOSIT_APPROVE','Manual Deposit - Admin Approved','Your Deposit is Approved','<div>Your deposit request of <b>{{amount}} {{currency}}</b> is via&nbsp; <b>{{method_name}} </b>is Approved .<b><br></b></div><div><b><br></b></div><div><b>Details of your Deposit :<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color=\"#FF0000\">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>Payable : {{method_amount}} {{method_currency}} <br></div><div>Paid via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><font size=\"5\"><b><br></b></font></div><div><font size=\"5\">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br></div>',NULL,'Admin Approve Your {{amount}} {{gateway_currency}} payment request by {{gateway_name}} transaction : {{transaction}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By user\",\"charge\":\"Gateway Charge\",\"currency\":\"Site Currency\",\"rate\":\"Conversion Rate\",\"method_name\":\"Deposit Method Name\",\"method_currency\":\"Deposit Method Currency\",\"method_amount\":\"Deposit Method Amount After Conversion\", \"post_balance\":\"Users Balance After this operation\"}',0,1,'2020-06-16 20:00:00','2022-07-13 08:27:48'),(209,'DEPOSIT_REJECT','Manual Deposit - Admin Rejected','Your Deposit Request was Rejected','<div>Your deposit request of <b>{{amount}} {{currency}}</b> is via&nbsp; <b>{{method_name}} has been rejected</b>.<b><br></b></div><br><div>Transaction Number was : {{trx}}</div><div><br></div><div>if you have any query, feel free to contact us.<br></div><br><div><br><br></div>\r\n\r\n\r\n\r\n{{rejection_message}}',NULL,'Admin Rejected Your {{amount}} {{gateway_currency}} payment request by {{gateway_name}}\r\n\r\n{{rejection_message}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By user\",\"charge\":\"Gateway Charge\",\"currency\":\"Site Currency\",\"rate\":\"Conversion Rate\",\"method_name\":\"Deposit Method Name\",\"method_currency\":\"Deposit Method Currency\",\"method_amount\":\"Deposit Method Amount After Conversion\",\"rejection_message\":\"Rejection message\"}',0,1,'2020-06-09 20:00:00','2022-07-13 08:27:52'),(210,'WITHDRAW_REQUEST','Withdraw  - User Requested','Withdraw Request Submitted Successfully','<div>Your withdraw request of <b>{{amount}} {{currency}}</b>&nbsp; via&nbsp; <b>{{method_name}} </b>has been submitted Successfully.<b><br></b></div><div><b><br></b></div><div><b>Details of your withdraw:<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color=\"#FF0000\">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>You will get: {{method_amount}} {{method_currency}} <br></div><div>Via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><font size=\"4\" color=\"#FF0000\"><b><br></b></font></div><div><font size=\"4\" color=\"#FF0000\"><b>This may take {{delay}} to process the payment.</b></font><br></div><div><font size=\"5\"><b><br></b></font></div><div><font size=\"5\"><b><br></b></font></div><div><font size=\"5\">Your current Balance is <b>{{post_balance}} {{currency}}</b></font></div><div><br></div><div><br><br><br><br></div>',NULL,'{{amount}} {{currency}} withdraw requested by {{method_name}}. You will get {{method_amount}} {{method_currency}} in {{delay}}. Trx: {{trx}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By user\",\"charge\":\"Gateway Charge\",\"currency\":\"Site Currency\",\"rate\":\"Conversion Rate\",\"method_name\":\"Deposit Method Name\",\"method_currency\":\"Deposit Method Currency\",\"method_amount\":\"Deposit Method Amount After Conversion\", \"post_balance\":\"Users Balance After this operation\", \"delay\":\"Delay time for processing\"}',0,1,'2020-06-07 20:00:00','2022-07-13 08:27:56'),(211,'WITHDRAW_REJECT','Withdraw - Admin Rejected','Withdraw Request has been Rejected and your money is refunded to your account','<div>Your withdraw request of <b>{{amount}} {{currency}}</b>&nbsp; via&nbsp; <b>{{method_name}} </b>has been Rejected.<b><br></b></div><div><b><br></b></div><div><b>Details of your withdraw:<br></b></div><div><br></div><div>Amount : {{amount}} {{currency}}</div><div>Charge: <font color=\"#FF0000\">{{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>You should get: {{method_amount}} {{method_currency}} <br></div><div>Via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div><div><br></div><div>----</div><div><font size=\"3\"><br></font></div><div><font size=\"3\"> {{amount}} {{currency}} has been <b>refunded </b>to your account and your current Balance is <b>{{post_balance}}</b><b> {{currency}}</b></font></div><div><br></div><div>-----</div><div><br></div><div><font size=\"4\">Details of Rejection :</font></div><div><font size=\"4\"><b>{{admin_details}}</b></font></div><div><br></div><div><br><br><br><br><br><br></div>',NULL,'Admin Rejected Your {{amount}} {{currency}} withdraw request. Your Main Balance {{main_balance}}  {{method}} , Transaction {{transaction}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By user\",\"charge\":\"Gateway Charge\",\"currency\":\"Site Currency\",\"rate\":\"Conversion Rate\",\"method_name\":\"Deposit Method Name\",\"method_currency\":\"Deposit Method Currency\",\"method_amount\":\"Deposit Method Amount After Conversion\", \"post_balance\":\"Users Balance After this operation\", \"admin_details\":\"Details Provided By Admin\"}',0,1,'2020-06-09 20:00:00','2022-07-13 08:28:00'),(212,'WITHDRAW_APPROVE','Withdraw - Admin  Approved','Withdraw request has been processed and your money is sent','<div>Your withdraw request of <b>{{amount}} {{currency}}</b>&nbsp; via&nbsp; <b>{{method_name}} </b>has been Processed Successfully.<b><br></b></div><div><b><br></b></div><div><b>Details of your withdraw:<br></b></div><div><br></div><div><font color=\"#000000\">Amount : {{amount}} {{currency}}</font></div><div><font color=\"#000000\">Charge: {{charge}} {{currency}}</font></div><div><br></div><div>Conversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}</div><div>You will get: {{method_amount}} {{method_currency}} <br></div><div>Via :&nbsp; {{method_name}}</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div><div>-----</div><div><br></div><div><font size=\"4\">Details of Processed Payment :</font></div><div><font size=\"4\"><b>{{admin_details}}</b></font></div><div><br></div><div><br><br><br><br><br></div>',NULL,'Admin Approve Your {{amount}} {{currency}} withdraw request by {{method}}. Transaction {{transaction}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By user\",\"charge\":\"Gateway Charge\",\"currency\":\"Site Currency\",\"rate\":\"Conversion Rate\",\"method_name\":\"Deposit Method Name\",\"method_currency\":\"Deposit Method Currency\",\"method_amount\":\"Deposit Method Amount After Conversion\", \"admin_details\":\"Details Provided By Admin\"}',0,1,'2020-06-10 20:00:00','2022-07-13 08:28:04'),(215,'BAL_ADD','Balance Add by Admin','Your Account has been Credited','<div>{{amount}} {{currency}} has been added to your account .</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div>Your Current Balance is : <font size=\"3\"><b>{{post_balance}}&nbsp; {{currency}}&nbsp;</b></font>',NULL,'{{amount}} {{currency}} credited in your account. Your Current Balance {{remaining_balance}} {{currency}} . Transaction: #{{trx}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By Admin\",\"currency\":\"Site Currency\", \"post_balance\":\"Users Balance After this operation\"}',0,1,'2019-09-14 21:14:22','2022-07-13 08:28:08'),(216,'BAL_SUB','Balance Subtracted by Admin','Your Account has been Debited','<div>{{amount}} {{currency}} has been subtracted from your account .</div><div><br></div><div>Transaction Number : {{trx}}</div><div><br></div>Your Current Balance is : <font size=\"3\"><b>{{post_balance}}&nbsp; {{currency}}</b></font>',NULL,'{{amount}} {{currency}} debited from your account. Your Current Balance {{remaining_balance}} {{currency}} . Transaction: #{{trx}}','{\"trx\":\"Transaction Number\",\"amount\":\"Request Amount By Admin\",\"currency\":\"Site Currency\", \"post_balance\":\"Users Balance After this operation\"}',0,1,'2019-09-14 21:14:22','2022-07-13 08:28:12'),(217,'BID_COMPLETE','Product Bid - Successful','Your Product has been Bid on Successfully','<div>Your product has been bided by <b>{{amount}} {{currency}}</b> successfully.<br></div>\r\n<div><b><br></b></div>\r\n<div><b>Details of your Bid :<br></b></div>\r\n<div><br></div>\r\n<div>Product: {{product}}</div>\r\n<div>Product price : {{product_price}} {{currency}}</div>\r\n<div>Bid price : {{amount}} {{currency}}</div>\r\n\r\n<div><br></div>\r\n\r\n<div>Transaction Number : {{trx}}</div>\r\n<div>\r\n    <font size=\"5\"><b><br></b></font>\r\n</div>\r\n<div>\r\n    <font size=\"5\">Your current Balance is <b>{{ post_balance }} {{ currency }}</b></font>\r\n</div>\r\n<div><br></div>\r\n<div><br><br><br></div>',NULL,'{{amount}} {{currrency}} Bid successfully','{\r\n    \"trx\": \"Transaction Number\",\r\n    \"amount\": \"Request Amount By user\",\r\n    \"currency\": \"Site Currency\",\r\n    \"product\": \"Your Product Name\",\r\n    \"product_price\": \"Product Price\",\r\n    \"post_balance\": \"Users Balance After this operation\"\r\n}',0,1,'2020-06-24 20:00:00','2022-06-29 21:06:01'),(218,'BID_WINNER','Bid Winner','Congratulations! You have secured a Indonesia National Winners 2023/2024 Auction Lot','<p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\"><span style=\"background-color: transparent; color: rgb(33, 37, 41); font-size: 12pt; white-space: pre-wrap;\">Congratulations. You are the winning bidder and owner of {{product}}.</span><br></p><p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\">&nbsp;</p><p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\"><span style=\"color: rgb(33, 37, 41); font-size: 12pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Details of your Winning Bid :</span></p><p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\">&nbsp;</p><p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\"><span style=\"color: rgb(33, 37, 41); font-size: 12pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">Product: {{product}}</span></p><p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\"><span style=\"color: rgb(33, 37, 41); background-color: transparent; font-size: 12pt; white-space: pre-wrap;\">Bid price : {{amount}} {{currency}}</span></p><p dir=\"ltr\" style=\"margin-top: 0pt; margin-bottom: 0pt; line-height: 1.38;\"><span style=\"color: rgb(33, 37, 41); background-color: transparent; font-size: 12pt; white-space: pre-wrap;\"><br></span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">This bid does not include the vacuum packing fees and shipping costs. The Indonesia NW coffees are\r\npackaged in 30kg boxes containing two 15kg vacuum-packed bags. If you are the winning bidder, and\r\nyou bid for a group of roasters, you are responsible for notifying ACE of the names of the companies in\r\nyour group. You are also the responsible party for shipment and payment unless otherwise approved by\r\nthe exporter.</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nPLEASE FILL IN THE FOLLOWING FORM WITH YOUR SHIPPING &amp;amp; LOGISTIC DETAILS. IN CASE YOU ARE\r\nSHARING YOUR LOT WITH OTHER BUYERS WHO HAVE DIFFERENT SHIPPING INSTRUCTIONS FROM THE\r\nMAIN BUYER, PLEASE SHARE THE LINK WITH THEM. ALL BUYING PARTIES MUST FILL.\r\nhttps://forms.gle/VDGtjH3oTjNnh8WK8</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nHere is your winning lot digital sticker for your creative departments to be able to download and print in\r\nadvance of receiving the lots. Follow the link below to download the sticker.\r\nhttps://drive.google.com/file/d/1rNTH7APoXCeC4hrg40DTaYePKsPT8odS/view?usp=sharing</span><br></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nThe winning lot sticker will be accessible solely through the link above. If you are in a buying group, you\r\ncan share the link with anyone else who is deemed a part of the buying group.</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nBidders are responsible for contacting: Dede Elin from PT SINGA GARUDA MAS, Indonesia (within 7\r\ndays) and payment (within 30 days) as per the bidding agreement. NW lots for Indonesia are shipped\r\nterms FOB net weight, and applicable fees- airfreight is available at an extra cost and can be coordinated\r\nby the exporter. Lots will be shipped as per the instructions and at the cost of the successful bidder. Any\r\nchanges that result in extra fees will be paid by the buyer. Lots will be shipped as per the instructions\r\nand at the cost of the successful bidder. Any changes that result in extra fees will be paid by the buyer.</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">Successful bidders will be charged $.20 (twenty cents) per pound mandatory fee for vacuum packing. If\r\nyou are bidding for a group, it is your responsibility to inform ACE of the buyers on the lot and to send\r\nprompt payment and shipping instructions</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nThe nominated exporter is the successful bidder’s contractual counterpart. The nominated exporter for\r\nIndonesia is:\r\nTHE NOMINATED EXPORTER\r\n1. Exporter Name: PT SINGA GARUDA MAS\r\n2. Exporter Address: Office 8, #18-A, SCBD Lot 8, Jl. Jend. Sudirman Kav. 52-53, Jakarta Selatan\r\n12190, Indonesia\r\n3. Exporter Contact Name: Dede Elin\r\n4. Exporter Phone numbers: +6597517501\r\n5. Exporter Email Address: kenny@catur.coffee, avilla@catur.coffee, erica@catur.coffee,</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">6. Copy to: gabriela@cupofexcellence.org, gary@cupofexcellence.org, kathia@cupofexcellence.org\r\n7. Box Dimensions: 55x36x23 cm</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nPrompt shipping, and payment within 30 days of the auction date is critical for the quality of your coffee\r\nand the farmers being rewarded. Airfreight is available.</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nBelow are the verified bank details for payment. If you get any invoice that does not contain this exact\r\nbank information do not pay it until both ACE and the exporter have verified the accuracy of the\r\npayment information. To facilitate follow up, please remember to send a copy of your bank transaction\r\ndocument to the exporter as soon as you make payment.</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nBANK DETAILS:\r\nBENEFICIARY BANK: BCA\r\nADDRESS: Landmark Pluit, Jl. Pluit Selatan Raya, Blok A8, RT.8/RW.10, Pluit, Penjaringan,\r\nNorth Jakarta City, Jakarta 14450\r\nSWIFT CODE: CENAIDJA\r\nACCOUNT NUMBER: 168 913 8134\r\nFINAL BENEFICIARY ACCOUNT NAME: SINGA GARUDA MAS PT\r\nFOR FINAL CREDIT TO ACCOUNT NUMBER: 168 913 8134</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nIf you would like any other companies listed on the auction results page of the website, please contact\r\nkathia@cupofexcellence.org. Winning buyers’ names are posted one hour after the auction closes.</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\nThank you so very much for your support. The prices you have been willing to pay for these\r\nextraordinary NW award winning coffees will provide a huge incentive for further quality development.</span></p><p dir=\"ltr\" style=\"margin: 0pt 0px; line-height: 1.38;\"><span style=\"font-size: 16px; white-space-collapse: preserve;\">\r\n- The Alliance for Coffee Excellence team</span><br></p>',NULL,'You have won  the {{product}}','{\r\n    \"product\": \"Your Product Name\"\r\n}',1,1,'2020-06-24 20:00:00','2024-04-02 13:34:02'),(219,'INVITATION','Invitation to join a Bid Group','Invitation to join a Bidding Group','<div>\r\n<p>Dear {{name}}</p><p>You have been invited to join a bidding group in the event {{event}}.</p><p>The leader of the bidding group is {{leader}}</p><br>\r\nClick Here to open your dashboard:<br>\r\n{{link}}\r\n</div>',NULL,'You have invited to join a bid group','{\r \"name\":\"reciever name\",\r \"event\":\"event name\",\r \"leader\":\"group leader\",\r \"link\":\"link to dashboard\"\r }',1,1,'2022-06-18 18:14:17','2022-06-27 16:10:17'),(220,'Delegate_Group_Leader','Delegate Bidding Group Leader','Your have been delegated as Bidding Group Leader','<div>\r\n<p>Dear {{name}}</p><p>You have been delegated as Bidding Group Leader in the event {{event}} by {{delegator}}</p><p><br></p><p>You can approve or reject the delegation from the link:</p><p>{{link}}</p>\r\n</div>',NULL,'You have assigned as Group Leader','{\"link\":\"link to dashboard\",\"name\":\"reciever name\",\"event\":\"event name\",\"delegator\":\"sender name\"}',1,1,'2022-06-18 21:14:17','2022-06-27 16:09:14'),(221,'Group_Leader','Bidding Group Created','Bidding Group Created','<div>\n<p>Dear {{name}}</p><p>Bidding Group Created in event {{event}}</p>\n</div>',NULL,'Bidding Group Created','{\"name\":\"Bidding group leader\",\"event\":\"event name\"}',1,1,'2022-06-27 14:32:00','2022-06-27 14:32:24'),(222,'Bidder_Approval','Bidder Approval','Your Bidder Approval has been Granted','<p></p><p><font size=\"3\">You have been approved as a bidder for the auction.&nbsp; <a href=\"https://mcultivo.com/login/ace_member\" title=\"\" target=\"_blank\">Login here.</a>&nbsp;<br></font><span style=\"font-size: medium;\">Learn more about the M-Cultivo auction portal before the bidding begins by watching our&nbsp;</span><span style=\"background-color: rgb(255, 255, 255);\"><a href=\"https://mcultivo.tawk.help/category/cup-of-excellence-help-videos\" title=\"\" target=\"\">instructional videos</a><font size=\"3\">.</font></span></p><font size=\"3\"><br><br></font><p></p>',NULL,'<div><span id=\"docs-internal-guid-a5ac3384-7fff-3f01-96b2-b5d597566f01\"><p dir=\"ltr\" style=\"line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;\"><span style=\"font-size: 12pt; color: rgb(33, 37, 41); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;\">You have been approved as a bidder for {Event Name}. Login here {hyperlink to login page}.</span></p><p dir=\"ltr\" style=\"line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;\">&nbsp;</p><p dir=\"ltr\" style=\"line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;\"><br></p></span></div><div><br></div>\r\n<div><br></div>\r\n<div><br><br><br></div>','{\"event\": \"Your Event Name\",\"hyperlink to login page\" :\"login link\" }',1,1,NULL,'2024-01-22 14:22:18'),(223,'Request_Access_Notification','Request Access Notification','Request Access Notification','<div><br></div><div>the user {{user_name}} requests access to the event {{event_name}}</div>',NULL,NULL,'{\"event_name\":\"Event Name\",\"user_name\":\"User Name\"}',1,1,NULL,NULL),(224,'Clock_Notification','Clock Notification','Clock Notification','<div><br></div><div>the clock for the event {{event_name}} has run out</div>',NULL,NULL,'{\"event_name\":\"Event Name\"}',1,1,NULL,NULL);
/*!40000 ALTER TABLE `email_sms_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_permission_group`
--

# DROP TABLE IF EXISTS `event_permission_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event_permission_group` (
                                          `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                          `group_id` bigint unsigned NOT NULL,
                                          `event_id` bigint unsigned NOT NULL,
                                          `created_at` timestamp NULL DEFAULT NULL,
                                          `updated_at` timestamp NULL DEFAULT NULL,
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_permission_group`
--

LOCK TABLES `event_permission_group` WRITE;
/*!40000 ALTER TABLE `event_permission_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_permission_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_permission_user`
--

# DROP TABLE IF EXISTS `event_permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `event_permission_user` (
                                         `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                         `permission_id` bigint unsigned NOT NULL,
                                         `event_id` bigint unsigned NOT NULL,
                                         `created_at` timestamp NULL DEFAULT NULL,
                                         `updated_at` timestamp NULL DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_permission_user`
--

LOCK TABLES `event_permission_user` WRITE;
/*!40000 ALTER TABLE `event_permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

# DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `status` varchar(190) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'active' COMMENT 'active, ended',
                          `bid_status` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'open',
                          `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `practice` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'practice=1',
                          `sname` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                          `description` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `image` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `logo` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `start_date` datetime NOT NULL,
                          `end_date` datetime NOT NULL,
                          `max_end_date` datetime DEFAULT NULL,
                          `less_bidding_time` int NOT NULL,
                          `deposit` decimal(28,2) DEFAULT NULL,
                          `event_type` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'm_cultivo_event',
                          `agreement` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `EventClockStartOn` int DEFAULT NULL,
                          `start_status` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL DEFAULT 'pending',
                          `category_id` int DEFAULT NULL,
                          `start_counter` int NOT NULL DEFAULT '0',
                          `max_bidding_value` decimal(28,2) NOT NULL,
                          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          `updated_at` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exchange_rates`
--

# DROP TABLE IF EXISTS `exchange_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exchange_rates` (
                                  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                  `Currency_Code` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `Base_Currency` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USD',
                                  `Exchange_Rate` decimal(8,2) NOT NULL,
                                  `Exchange_Date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `created_at` timestamp NULL DEFAULT NULL,
                                  `updated_at` timestamp NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exchange_rates`
--

LOCK TABLES `exchange_rates` WRITE;
/*!40000 ALTER TABLE `exchange_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `exchange_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extensions`
--

# DROP TABLE IF EXISTS `extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extensions` (
                              `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                              `act` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                              `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                              `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                              `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `script` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                              `shortcode` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'object',
                              `support` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'help section',
                              `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>enable, 2=>disable',
                              `deleted_at` datetime DEFAULT NULL,
                              `created_at` timestamp NULL DEFAULT NULL,
                              `updated_at` timestamp NULL DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extensions`
--

LOCK TABLES `extensions` WRITE;
/*!40000 ALTER TABLE `extensions` DISABLE KEYS */;
INSERT INTO `extensions` VALUES (1,'tawk-chat','Tawk.to','Key location is shown bellow','tawky_big.png','<script>\r\n                        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n                        (function(){\r\n                        var s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];\r\n                        s1.async=true;\r\n                        s1.src=\"https://embed.tawk.to/{{app_key}}\";\r\n                        s1.charset=\"UTF-8\";\r\n                        s1.setAttribute(\"crossorigin\",\"*\");\r\n                        s0.parentNode.insertBefore(s1,s0);\r\n                        })();\r\n                    </script>','{\"app_key\":{\"title\":\"App Key\",\"value\":\"6181a7386bb0760a4940d715\\/1fjh7ad74\"}}','twak.png',0,NULL,'2019-10-19 01:16:05','2023-11-03 14:51:46'),(2,'google-recaptcha2','Google Recaptcha 2','Key location is shown bellow','recaptcha3.png','\r\n<script src=\"https://www.google.com/recaptcha/api.js\"></script>\r\n<div class=\"g-recaptcha\" data-sitekey=\"{{sitekey}}\" data-callback=\"verifyCaptcha\"></div>\r\n<div id=\"g-recaptcha-error\"></div>','{\"sitekey\":{\"title\":\"Site Key\",\"value\":\"6Lfpm3cUAAAAAGIjbEJKhJNKS4X1Gns9ANjh8MfH\"}}','recaptcha.png',0,NULL,'2019-10-19 01:16:05','2022-03-07 12:22:07'),(3,'custom-captcha','Custom Captcha','Just Put Any Random String','customcaptcha.png',NULL,'{\"random_key\":{\"title\":\"Random String\",\"value\":\"SecureString\"}}','na',0,NULL,'2019-10-19 01:16:05','2022-03-07 13:41:18'),(4,'google-analytics','Google Analytics','Key location is shown bellow','google_analytics.png','<script async src=\"https://www.googletagmanager.com/gtag/js?id={{app_key}}\"></script>\r\n                <script>\r\n                  window.dataLayer = window.dataLayer || [];\r\n                  function gtag(){dataLayer.push(arguments);}\r\n                  gtag(\"js\", new Date());\r\n                \r\n                  gtag(\"config\", \"{{app_key}}\");\r\n                </script>','{\"app_key\":{\"title\":\"App Key\",\"value\":\"G-12SLHXFLSR\"}}','ganalytics.png',1,NULL,NULL,'2022-10-26 13:20:42'),(5,'fb-comment','Facebook Comment ','Key location is shown bellow','Facebook.png','<div id=\"fb-root\"></div><script async defer crossorigin=\"anonymous\" src=\"https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId={{app_key}}&autoLogAppEvents=1\"></script>','{\"app_key\":{\"title\":\"App Key\",\"value\":\"----\"}}','fb_com.PNG',0,NULL,NULL,'2022-03-05 12:48:01'),(7,'hotjar','HotJar','Key location is shown bellow','hotjar.png','<script>(function(h,o,t,j,a,r){h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};h._hjSettings={hjid:{{app_key}},hjsv:6};a=o.getElementsByTagName(\'head\')[0];r=o.createElement(\'script\');r.async=1;r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;a.appendChild(r);})(window,document,\'https://static.hotjar.com/c/hotjar-\',\'.js?sv=\');</script>','{\"app_key\":{\"title\":\"App Key\",\"value\":\"3180225\"}}','HotJar-Support.png',1,NULL,NULL,'2022-10-27 14:50:41'),(8,'exchange_rate','Exchange Rate','Key location is shown bellow','exchange.png','','{\"app_key\":{\"title\":\"App Key\",\"value\":\"w7A6PQL39TN36WBUSEjTmHhfzufN18hP\"}}','exchange.png',1,NULL,NULL,'2023-04-23 10:06:13'),(9,'pusher','Pusher','Key location is shown bellow','pusher.png',NULL,'{\"PUSHER_APP_ID\":{\"title\":\"PUSHER_APP_ID\",\"value\":\"1459178\"},\"PUSHER_APP_KEY\":{\"title\":\"PUSHER_APP_KEY\",\"value\":\"64b2a46f7a0c3ff4f6fb\"},\"PUSHER_APP_SECRET\":{\"title\":\"PUSHER_APP_SECRET\",\"value\":\"78db106679beec522725\"},\"PUSHER_APP_CLUSTER\":{\"title\":\"PUSHER_APP_CLUSTER\",\"value\":\"mt1\"}}','pusher_support.png',1,NULL,NULL,'2024-03-12 11:30:18');
/*!40000 ALTER TABLE `extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees`
--

# DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fees` (
                        `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                        `event_id` int NOT NULL,
                        `country_id` int NOT NULL,
                        `fee_value` double(8,2) NOT NULL,
                        `payment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=783 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees`
--

LOCK TABLES `fees` WRITE;
/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frontends`
--

# DROP TABLE IF EXISTS `frontends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `frontends` (
                             `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                             `data_keys` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                             `data_values` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                             `created_at` timestamp NULL DEFAULT NULL,
                             `updated_at` timestamp NULL DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frontends`
--

LOCK TABLES `frontends` WRITE;
/*!40000 ALTER TABLE `frontends` DISABLE KEYS */;
INSERT INTO `frontends` VALUES (1,'seo.data','{\"seo_image\":\"1\",\"keywords\":[\"bid\",\"auction\",\"bidding\",\"bidding platform\",\"auction bidding\",\"product bidding\",\"e-commerce\",\"coffee auction\"],\"description\":\"M-Cultivo provides digital tools to make the coffee supply chain more equitable.\",\"social_title\":\"M-Cultivo - Cultivating a new generation of coffee producers\",\"social_description\":\"M-Cultivo provides coffee producers technology services to digitalize their supply chains, offering them better opportunities to compete more fully in the global marketplace. The primary ways we partner with coffee producers are through supply chain management technologies tailored specifically to coffee production, and access-to-market solutions that provide commerce enablement like our Marketplaces.\",\"image\":\"660c8be8198c61712098280.jpg\"}','2020-07-05 01:42:52','2024-04-02 22:51:20'),(24,'about.content','{\"has_image\":\"1\",\"heading\":\"About Us\",\"subheading\":\"Cultivating a new generation of coffee producers\",\"description\":\"<div><h4>What We Do<\\/h4>M-Cultivo provides coffee producers technology services to digitalize their supply chains, offering them better opportunities to compete more fully in the global marketplace. The primary ways we partner with coffee producers are through supply chain management technologies tailored specifically to coffee production, and access-to-market solutions that provide commerce enablement like our Marketplaces.\\u00a0<br \\/><br \\/><h4>M-Cultivo History<\\/h4><div>Founded in 2020, our mission to cultivate better livelihoods for coffee farmers is born out of a deep love for the industry and a desire to see everyone in it flourish. We are coffee professionals and compassionate problem solvers with decades of experience in coffee supply chains, technology, and impact evaluation. We think the best way to improve coffee is to create a supply chain that actually works for producers.\\u00a0<br \\/><br \\/><\\/div><div>For decades, coffee producers have been excluded and disconnected from fully competing in the global coffee market. Limited pricing information forces farmers to accept a low price mandated by a select few buyers. Millions of farmers are stuck in this cycle that barely keeps them in business one more day.<br \\/><br \\/><\\/div><div>Our solutions have been built alongside coffee producers through hundreds of conversations and interviews. The feedback from these producers has been our guide to ensure we are building a solution that meets real needs. M-Cultivo is not just a technology company. We are fostering a global community of producers strengthened by the information and relationships they have access to through our platform.<\\/div><\\/div><div><br \\/><\\/div><div><h4>Partnership with ACE\\/COE<\\/h4>We\'ve partnered with the <a href=\\\"http:\\/\\/https\\/\\/allianceforcoffeeexcellence.org\\/\\\" title=\\\"\\\">Alliance for Coffee Excellence<\\/a> to host their prestigious Cup of Excellence auctions, and to expand their auction programming beyond their Cup of Excellence, National Winner\'s, and ACE Private Collection auctions. We\'re excited to expand our partnership with ACE to offer marketplaces for coffees vetted through the National Juries in the Cup of Excellence process. In addition to our partnership with ACE and COE, we\\u2019ll be offering private auctions for producers, as well as an ongoing offer list of coffees available to buy now.\\u00a0<\\/div><div><br \\/><\\/div><div><h4>Impact<\\/h4><h6>B-Corp Certification<\\/h6><\\/div><div>In order to provide accountability within our organization and to our stakeholders, we have chosen to become a B Corporation. As a B Corporation, we are legally required to consider the impact our decisions have on our employees, customers, suppliers, community, and environment. We chose to pursue this certification because it provides us with a framework to measure and assess our impact in the communities we serve.<\\/div>\",\"video_url\":\"https:\\/\\/www.youtube.com\\/watch?v=lG8635ImG0Q\",\"about_image\":\"62e44ae324f941659128547.jpg\"}','2020-10-28 01:51:20','2022-07-30 02:36:09'),(25,'blog.content','{\"heading\":\"Recent Posts\",\"subheading\":\"Praesentium ipsam modi nostrum, quibusdam voluptas minus qui quas dicta consequuntur placeat animi cumque\"}','2020-10-28 01:51:34','2022-02-26 06:54:57'),(27,'contact_us.content','{\"title\":\"We\'re here to help\",\"short_details\":\"Please contact us through our live chat or through the details below\",\"email_address\":\"support@mcultivo.com\",\"contact_details\":\"Atlanta, Georgia, USA\",\"contact_number\":\"+1 (678) 404-9338\"}','2020-10-28 01:59:19','2022-06-29 13:26:46'),(28,'counter.content','{\"heading\":\"Latest News\",\"subheading\":\"Register New Account\"}','2020-10-28 02:04:02','2020-10-28 02:04:02'),(31,'social_icon.element','{\"title\":\"Facebook\",\"social_icon\":\"<i class=\\\"las la-expand\\\"><\\/i>\",\"url\":\"https:\\/\\/www.google.com\\/\"}','2020-11-12 05:07:30','2021-05-12 07:56:59'),(33,'feature.content','{\"heading\":\"asdf\",\"subheading\":\"asdf\"}','2021-01-04 00:40:54','2021-01-04 00:40:55'),(35,'service.element','{\"trx_type\":\"withdraw\",\"service_icon\":\"<i class=\\\"las la-highlighter\\\"><\\/i>\",\"title\":\"asdfasdf\",\"description\":\"asdfasdfasdfasdf\"}','2021-03-06 02:12:10','2021-03-06 02:12:10'),(36,'service.content','{\"trx_type\":\"withdraw\",\"heading\":\"asdf fffff\",\"subheading\":\"asdf asdfasdf\"}','2021-03-06 02:27:34','2021-03-06 03:19:39'),(39,'banner.content','{\"has_image\":\"1\",\"heading\":\"Discover and purchase the best coffees in the world\",\"subheading\":\"M-Cultivo hosts auctions and marketplaces for producers and organizations looking for streamlined, innovative supply chain solutions\",\"button\":\"Learn More\",\"button_url\":\"about-us\",\"link\":\"Register as a Farmer\",\"link_url\":\"merchant\\/register\",\"background_image\":\"6245c5c698cb81648739782.jpg\"}','2021-05-02 08:09:30','2022-06-29 13:42:44'),(41,'cookie.data','{\"link\":\"#\",\"description\":\"<font color=\\\"#000\\\" face=\\\"Exo, sans-serif\\\"><span style=\\\"font-size: 18px;\\\">We may use cookies or any other tracking technologies when you visit our website, including any other media form, mobile website, or mobile application related or connected to help customize the Site and improve your experience.<\\/span><\\/font><br>\",\"status\":1}','2020-07-05 01:42:52','2022-03-03 11:26:19'),(47,'categories.content','{\"heading\":\"Auction Categories\",\"subheading\":\"........\"}','2022-02-01 09:25:09','2022-04-07 19:42:59'),(48,'categories.element','{\"heading\":\"Sports\",\"icon\":\"<i class=\\\"las la-chess-knight\\\"><\\/i>\"}','2022-02-01 09:26:03','2022-02-01 09:26:03'),(49,'categories.element','{\"heading\":\"Electronics\",\"icon\":\"<i class=\\\"las la-mobile-alt\\\"><\\/i>\"}','2022-02-01 09:26:17','2022-02-01 09:26:17'),(50,'categories.element','{\"heading\":\"Watches\",\"icon\":\"<i class=\\\"las la-hourglass-start\\\"><\\/i>\"}','2022-02-01 09:26:26','2022-02-01 09:26:26'),(51,'categories.element','{\"heading\":\"Vehicles, Marine\",\"icon\":\"<i class=\\\"las la-car-side\\\"><\\/i>\"}','2022-02-01 09:26:50','2022-02-01 09:26:50'),(52,'categories.element','{\"heading\":\"Real Estate\",\"icon\":\"<i class=\\\"las la-place-of-worship\\\"><\\/i>\"}','2022-02-01 09:27:10','2022-02-01 09:27:10'),(53,'categories.element','{\"heading\":\"Jewelry\",\"icon\":\"<i class=\\\"las la-flask\\\"><\\/i>\"}','2022-02-01 09:27:33','2022-02-01 09:27:33'),(54,'categories.element','{\"heading\":\"Industrial Machinery\",\"icon\":\"<i class=\\\"las la-industry\\\"><\\/i>\"}','2022-02-01 09:27:40','2022-02-01 09:27:40'),(55,'categories.element','{\"heading\":\"Farm &amp; Agriculture\",\"icon\":\"<i class=\\\"las la-home\\\"><\\/i>\"}','2022-02-01 09:28:18','2022-02-01 09:28:18'),(56,'categories.element','{\"heading\":\"Dolls, Bears &amp; Toys\",\"icon\":\"<i class=\\\"las la-universal-access\\\"><\\/i>\"}','2022-02-01 09:28:26','2022-02-01 09:28:26'),(57,'categories.element','{\"heading\":\"Decorative Art\",\"icon\":\"<i class=\\\"las la-holly-berry\\\"><\\/i>\"}','2022-02-01 09:28:45','2022-02-01 09:28:53'),(58,'winner.content','{\"heading\":\"Our Latest Winners\",\"subheading\":\"Praesentium ipsam modi nostrum, quibusdam voluptas minus qui quas dicta consequuntur placeat animi cumque\",\"has_image\":\"1\",\"image\":\"61f8fe1524b391643707925.png\"}','2022-02-01 10:01:48','2022-02-01 10:02:13'),(59,'how_to_bid.content','{\"heading\":\"How To Bid\",\"subheading\":\"Praesentium ipsam modi nostrum, quibusdam voluptas minus qui quas dicta consequuntur placeat animi cumque\"}','2022-02-01 10:12:30','2022-02-01 10:12:30'),(60,'how_to_bid.element','{\"heading\":\"Win Your Bid\",\"icon\":\"<i class=\\\"las la-trophy\\\"><\\/i>\"}','2022-02-01 10:14:42','2022-02-01 10:14:42'),(61,'how_to_bid.element','{\"heading\":\"Make Bid Product\",\"icon\":\"<i class=\\\"las la-gavel\\\"><\\/i>\"}','2022-02-01 10:14:55','2022-02-01 10:14:55'),(62,'how_to_bid.element','{\"heading\":\"Choose Products\",\"icon\":\"<i class=\\\"las la-hand-pointer\\\"><\\/i>\"}','2022-02-01 10:15:07','2022-02-01 10:15:07'),(63,'testimonial.content','{\"heading\":\"Auction Winners Say\",\"subheading\":\"Praesentium ipsam modi nostrum, quibusdam voluptas minus qui quas dicta consequuntur placeat animi cumque\"}','2022-02-01 10:19:29','2022-02-01 10:19:29'),(64,'testimonial.element','{\"name\":\"Mabrur Rashid Banna\",\"designation\":\"Businessman\",\"star\":\"4\",\"description\":\"Fugiat expedita eius quas consectetur culpa. Neque veniam et molestias laborum non corporis aperiam optio culpa. Fuga ipsum harum aliquam quod nisi nostrum nam quis, ipsam excepturi qui dignissimos quidem\",\"has_image\":\"1\",\"user_image\":\"61f903ddab6a31643709405.jpg\"}','2022-02-01 10:26:45','2022-02-22 14:22:33'),(65,'testimonial.element','{\"name\":\"Mabrur Rashid Banna\",\"designation\":\"Sportsman\",\"star\":\"5\",\"description\":\"Fugiat expedita eius quas consectetur culpa. Neque veniam et molestias laborum non corporis aperiam optio culpa. Fuga ipsum harum aliquam quod nisi nostrum nam quis, ipsam excepturi qui dignissimos quidem.\",\"has_image\":\"1\",\"user_image\":\"61f9040db8c6b1643709453.jpg\"}','2022-02-01 10:27:33','2022-02-22 14:22:15'),(66,'testimonial.element','{\"name\":\"John Doe\",\"designation\":\"Businessman\",\"star\":\"5\",\"description\":\"Fugiat expedita eius quas consectetur culpa. Neque veniam et molestias laborum non corporis aperiam optio culpa. Fuga ipsum harum aliquam quod nisi nostrum nam quis, ipsam excepturi qui dignissimos quidem.\",\"has_image\":\"1\",\"user_image\":\"61f904417d5061643709505.jpg\"}','2022-02-01 10:28:25','2022-02-22 14:22:21'),(67,'testimonial.element','{\"name\":\"Mr David Roy\",\"designation\":\"Businessman\",\"star\":\"4\",\"description\":\"Fugiat expedita eius quas consectetur culpa. Neque veniam et molestias laborum non corporis aperiam optio culpa. Fuga ipsum harum aliquam quod nisi nostrum nam quis, ipsam excepturi qui dignissimos quidem.\",\"has_image\":\"1\",\"user_image\":\"61f9046e9dc841643709550.jpg\"}','2022-02-01 10:29:10','2022-02-22 14:22:28'),(68,'sponsors.element','{\"has_image\":\"1\",\"image\":\"621c6e3f062971646030399.png\"}','2022-02-01 10:34:38','2022-02-28 07:39:59'),(69,'sponsors.element','{\"has_image\":\"1\",\"image\":\"621c6e6d55ced1646030445.png\"}','2022-02-01 10:34:43','2022-02-28 07:40:45'),(70,'sponsors.element','{\"has_image\":\"1\",\"image\":\"621c6e73ab3711646030451.png\"}','2022-02-01 10:34:48','2022-02-28 07:40:51'),(71,'sponsors.element','{\"has_image\":\"1\",\"image\":\"621c6e7a527ef1646030458.png\"}','2022-02-01 10:34:54','2022-02-28 07:40:58'),(72,'sponsors.element','{\"has_image\":\"1\",\"image\":\"621c6e7fcb3651646030463.png\"}','2022-02-01 10:34:58','2022-02-28 07:41:03'),(73,'sponsors.element','{\"has_image\":\"1\",\"image\":\"621c6e85bb9d11646030469.png\"}','2022-02-01 10:35:03','2022-02-28 07:41:09'),(74,'auth.content','{\"has_image\":\"1\",\"background_image\":\"61f90fbb32a711643712443.png\"}','2022-02-01 11:17:23','2022-02-01 11:17:23'),(75,'header.content','{\"email\":\"support@mcultivo.com\",\"mobile\":\"+1 (678) 404-9338\"}','2022-02-20 08:55:56','2022-03-31 23:37:35'),(76,'faq.content','{\"heading\":\"Frequently Asked Questions\",\"subheading\":\"........\"}','2022-02-20 09:45:20','2022-03-31 23:35:00'),(77,'faq.element','{\"question\":\"Who can buy from the M-Cultivo Marketplace?\",\"answer\":\"<div class=\\\"faq__title\\\" style=\\\"padding:20px 45px 20px 20px;margin-right:20px;color:rgb(164,189,206);font-family:Nunito, sans-serif;\\\"><h5 class=\\\"title\\\" style=\\\"line-height:1.2;font-size:20px;color:rgb(193,81,204);font-family:Jost, sans-serif;\\\"><\\/h5><font color=\\\"#FFFFCC\\\">first : The marketplace is open to roasters in the United States, Canada, and the United Kingdom through our import partners Anthem Coffee Importers and Falcon Coffee. We plan to expand to other countries in the coming months. If you are outside of these markets, please still register on the marketplace. We will take note of your location and seek out import relationships to serve your area. We can also keep you updated as we expand and list new coffees.<br \\/><br \\/><br \\/>Second : Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/font><br \\/><\\/div>\"}','2022-02-20 09:45:45','2022-04-01 14:18:02'),(78,'faq.element','{\"question\":\"How does the marketplace work?\",\"answer\":\"<p style=\\\"margin-top:-12px;margin-bottom:20px;background-color:rgb(0,19,41);\\\"><span class=\\\"text--base\\\" style=\\\"color:rgb(193,81,204);font-family:Jost, sans-serif;font-size:16px;font-weight:bolder;\\\">First :\\u00a0<\\/span><font color=\\\"#a4bdce\\\" face=\\\"Nunito, sans-serif\\\"><span style=\\\"font-size:16px;\\\">The marketplace is a pilot project in which roasters in select markets can purchase coffees directly from producers online through a pre-negotiated supply chain. It\\u2019s a collaborative effort between M-Cultivo, the Alliance for Coffee Excellence, Cup of Excellence, the Brazilian Specialty Coffee Association, Sancoffee, Anthem Coffee Imports, and Falcon Coffees.<\\/span><\\/font><\\/p><p style=\\\"margin-top:-12px;margin-bottom:20px;background-color:rgb(0,19,41);\\\"><font color=\\\"#a4bdce\\\" face=\\\"Nunito, sans-serif\\\"><span style=\\\"font-size:16px;\\\">The Brazil Select coffees have been cupped by the Cup of Excellence Brazil 2021 National Jury and all scored 86 points or higher by that jury. All the coffees are currently in Brazil, awaiting sale and shipment.<\\/span><\\/font><\\/p><p style=\\\"margin-top:-12px;margin-bottom:20px;background-color:rgb(0,19,41);\\\"><font color=\\\"#a4bdce\\\" face=\\\"Nunito, sans-serif\\\"><span style=\\\"font-size:16px;\\\">Buyers can order samples of coffees or purchase without tasting the coffees. Buyers purchase the coffees and pay an estimated shipping charge that is inclusive of international freight, import fees, approval samples, and domestic freight. In other words, the price you pay is the cost of moving the coffee from origin to your door.<\\/span><\\/font><\\/p><p style=\\\"margin-top:-12px;margin-bottom:20px;background-color:rgb(0,19,41);\\\"><font color=\\\"#a4bdce\\\" face=\\\"Nunito, sans-serif\\\"><span style=\\\"font-size:16px;\\\">Coffees sold are shipped when the minimum order for consolidation agreed upon by the importers is met. Once the coffees are ready for shipment, an approval sample is sent to the buyer who has 5 days to either approve or reject the coffee. If not approval or rejection is given to M-Cultivo, the coffee will be counted as approved.<\\/span><\\/font><\\/p><p style=\\\"margin-top:-12px;margin-bottom:20px;background-color:rgb(0,19,41);\\\"><font color=\\\"#a4bdce\\\" face=\\\"Nunito, sans-serif\\\"><span style=\\\"font-size:16px;\\\">Freight charges are then fixed and the buyer may receive a partial refund for any difference in the actual freight cost and what was paid on the Marketplace. M-Cultivo will keep buyers updated as their coffee moves from the country of origin to their door.<\\/span><\\/font><\\/p><p style=\\\"margin-top:-12px;margin-bottom:20px;background-color:rgb(0,19,41);\\\"><font color=\\\"#a4bdce\\\" face=\\\"Nunito, sans-serif\\\"><span style=\\\"font-size:16px;\\\">After that, just enjoy your coffee!<\\/span><\\/font><\\/p><p style=\\\"margin-top:-12px;color:rgb(164,189,206);font-family:Nunito, sans-serif;font-size:16px;background-color:rgb(0,19,41);margin-bottom:-7px;\\\"><span class=\\\"text--base\\\" style=\\\"font-weight:bolder;font-family:Jost, sans-serif;color:rgb(193,81,204);\\\">Second :\\u00a0<\\/span><span style=\\\"font-size:1rem;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/span><\\/p>\"}','2022-02-20 09:47:20','2022-04-01 06:56:13'),(79,'faq.element','{\"question\":\"Are all coffees on the marketplace from the same origin?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Yes, right now all the coffees are from Brazil. In the future, we\'ll add more origins to the marketplace. We just have to wait for the coffee to be harvested and processed in other countries. Stay tuned!<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/p>\"}','2022-02-20 09:47:33','2022-04-07 20:05:13'),(80,'faq.element','{\"question\":\"How much of the FOB price will the farmer get paid?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">The farmer will get paid the FOB price minus any services necessary to move his or her coffee from farm to over the rails of the ship. The \'free on board\' price can include an export fee, broker fee, and any partnership fees needed to promote and sell the coffee.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">For example, in the Brazil select program the advertised $4.20\\/lb FOB price, farmers will take home approximately $3.36\\/lb. This per pound price accounts for fees for service from M-Cultivo, Alliance for Coffee Excellence, the Brazil Specialty Coffee Association, and the exporter.<br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Each supply chain and farmer program is unique, but please ask us on any particular coffee you are curious about. We aim to provide as much transparency as possible.<br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/p>\"}','2022-02-20 10:05:19','2022-04-07 20:05:55'),(82,'faq.element','{\"question\":\"Where is my coffee?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Have you purchased your coffee yet? If not, your coffee is still in the country of origin.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">If so...All coffees start off in the country of origin and must be consolidated before shipment. Consolidation can take up to 30 days and helps us reduce the cost to the end buyer. Shipping costs are particularly high right now because of the on-going shipping crisis.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">After coffees have been consolidated, we will export the coffee and begin moving it to the destination country. Depending on whether you selected air freight or ocean freight this process can take less than 4 weeks or up to 16 weeks. The final step is palletizing the coffee and shipping it in LTL (\'less than truckload\') or\\u00a0 FTL (\'full truck load\') directly to the address you provide at checkout. This domestic delivery is not included in shipping rates on the M-Cultivo Marketplace and will be quoted to each buyer by the importer upon the coffee\'s arrival to the importer\'s warehouse.\\u00a0<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">We will keep you updated on timelines as your coffee moves. In the meantime, please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/p>\"}','2022-02-20 10:07:53','2022-04-07 20:07:09'),(89,'counter.element','{\"title\":\"Registered Buyers\",\"counter_digit\":\"3000\",\"counter_icon\":\"<i class=\\\"fas fa-money-check-alt\\\"><\\/i>\"}','2022-02-20 12:49:16','2022-04-07 18:11:20'),(90,'counter.element','{\"title\":\"Coffees Auctioned\",\"counter_digit\":\"700\",\"counter_icon\":\"<i class=\\\"fas fa-coffee\\\"><\\/i>\"}','2022-02-20 12:50:01','2022-04-07 18:11:51'),(91,'counter.element','{\"title\":\"Farmers Participating\",\"counter_digit\":\"10,000\",\"counter_icon\":\"<i class=\\\"fas fa-user-check\\\"><\\/i>\"}','2022-02-20 12:53:52','2022-04-07 18:15:12'),(92,'counter.element','{\"title\":\"Avg. Lot Price\",\"counter_digit\":\"$20\\/lb\",\"counter_icon\":\"<i class=\\\"fas fa-money-bill-alt\\\"><\\/i>\"}','2022-02-20 12:54:19','2022-04-07 18:15:49'),(93,'quick_banner.element','{\"has_image\":\"1\",\"button\":\"Bid Now\",\"url\":\"category\\/4\\/industrial-machinery\",\"image\":\"6214e510d32891645536528.jpg\"}','2022-02-22 13:58:48','2022-03-06 08:13:06'),(94,'quick_banner.element','{\"has_image\":\"1\",\"button\":\"Bid Now\",\"url\":\"category\\/2\\/dolls-bears-and-toys\",\"image\":\"6214e5251a1661645536549.jpg\"}','2022-02-22 13:59:09','2022-03-06 08:13:20'),(95,'quick_banner.element','{\"has_image\":\"1\",\"button\":\"Bid Now\",\"url\":\"category\\/3\\/farm-and-agriculture\",\"image\":\"6214e52e0f82b1645536558.jpg\"}','2022-02-22 13:59:18','2022-03-06 08:14:07'),(96,'login.content','{\"has_image\":\"1\",\"background_image\":\"62172609a21711645684233.png\"}','2022-02-24 07:30:33','2022-02-24 07:30:34'),(97,'register.content','{\"has_image\":\"1\",\"background_image\":\"6217262fbae031645684271.png\"}','2022-02-24 07:31:11','2022-02-24 07:31:12'),(98,'2fa_verify.content','{\"has_image\":\"1\",\"background_image\":\"6217279c829dc1645684636.png\"}','2022-02-24 07:37:16','2022-02-24 07:37:16'),(99,'code_verify.content','{\"has_image\":\"1\",\"background_image\":\"621727a336e2e1645684643.png\"}','2022-02-24 07:37:23','2022-02-24 07:37:23'),(100,'email_verify.content','{\"has_image\":\"1\",\"background_image\":\"621727ab9dce11645684651.png\"}','2022-02-24 07:37:31','2022-02-24 07:37:32'),(101,'reset_password.content','{\"has_image\":\"1\",\"background_image\":\"621727b1dfa8d1645684657.png\"}','2022-02-24 07:37:37','2022-02-24 07:37:38'),(102,'reset_password_email.content','{\"has_image\":\"1\",\"background_image\":\"621727b82a4d51645684664.png\"}','2022-02-24 07:37:44','2022-02-24 07:37:44'),(103,'sms_verify.content','{\"has_image\":\"1\",\"background_image\":\"621727bd571881645684669.png\"}','2022-02-24 07:37:49','2022-02-24 07:37:49'),(108,'quick_banner.element','{\"has_image\":\"1\",\"button\":\"Bid Now\",\"url\":\"category\\/10\\/sports\",\"image\":\"622065a4733a41646290340.jpg\"}','2022-03-03 07:51:48','2022-03-06 08:14:20'),(109,'live_auction.content','{\"heading\":\"Featured Auctions\",\"subheading\":\".......................................................................................\"}','2022-03-03 08:04:04','2022-06-29 09:45:19'),(110,'recently_expired.content','{\"heading\":\"Recently Expired Auctions\",\"subheading\":\"Praesentium ipsam modi nostrum, quibusdam voluptas minus qui quas dicta consequuntur placeat animi cumque\"}','2022-03-03 08:04:05','2022-03-03 08:04:05'),(111,'upcoming_auction.content','{\"heading\":\"Upcoming Auctions\",\"subheading\":\"Praesentium ipsam modi nostrum, quibusdam voluptas minus qui quas dicta consequuntur placeat animi cumque\"}','2022-03-03 08:04:07','2022-03-03 08:04:07'),(112,'breadcrumb.content','{\"has_image\":\"1\",\"background_image\":\"62bc656b332571656513899.jpg\"}','2022-03-06 12:56:07','2022-06-29 14:44:59'),(114,'faq.element','{\"question\":\"What makes up the shipping and freight charges?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">The shipping rates charged are estimates and include international freight, import and fees, an approval sample for each lot, and a payment processing fee. Actual charges may differ at the time of shipping. Any differences in logistics costs we find will be refunded to you, the buyer. Each buyer will be responsible for additional domestic freight charges directly with their importer.\\u00a0<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">The more coffee you buy the less expensive the shipping rates will be. Shipping a single sack from the country of origin to your door will be expensive. Buyers start to get better shipping rates at 5 bags.\\u00a0If you\\u2019re interested in connecting with other roasters in your city to coordinate last-mile shipment to save on domestic freight, please reach out to support@mcultivo.com.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">We aim to serve farmers and customers. Part of that process is managing the volatility of shipping rates throughout the supply chain in a streamlined process.\\u00a0Shipping rates are particularly unpredictable and high at the moment due to the ongoing freight crisis. Thank you for your patience and understanding.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information. We are happy to provide more detail. Our commitment is full transparency on every order.<\\/p>\"}','2022-04-07 20:07:59','2022-04-07 20:07:59'),(115,'faq.element','{\"question\":\"When can I expect my order to arrive?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">For all sample orders, we will ship the samples as soon as possible with 2-day transit times. Please evaluate the samples for purchase as soon as possible after receipt. The coffees you sample are limited in quantity and will remain open for sale during your decision process. Expediting your evaluation process will increase your chances of being able to purchase the coffee sampled.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">For all lot orders, you will receive shipping updates from us as your coffee progress through the supply chain. Ocean Freight is estimated to take 12 to 16 weeks for delivery from the time of order. Air Freight is estimated to take 4 to 6 weeks for delivery from the time of order. Keep in mind air freight is only available in pallet (10 bag) increments.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/p>\"}','2022-04-07 20:08:23','2022-04-07 20:08:23'),(116,'faq.element','{\"question\":\"What marketing materials do I get with my order?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Buyers will receive photos, farm(er) information, coffee information, farmer contact details, and any relevant logos. For example, the Brazil Select program buyers will receive a Brazil Select logo.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">We will put our buyers in touch with the farmer as soon as the lot is approved and ready for shipment. Our farming partners will be able to support with any additional information and any more coffee needed. As always, we are here to assist as needed so please reach out if there is anything else you may need.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/p>\"}','2022-04-07 20:08:38','2022-04-07 20:08:38'),(117,'faq.element','{\"question\":\"How does quality control work on the marketplace?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">The coffees on the marketplace are graded by the national jury of cuppers through the Cup of Excellence competition process. The cuppers have be trained and calibrated by the Alliance for Coffee Excellence. The high quality of the coffees listed has earned these farmers a spot on the marketplace.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">The process the coffees go through is the following: Each coffee has goes through several rigorous rounds of cupping via the Cup of Excellence competition. Buyers are welcome to purchase samples to cup prior to purchasing a coffee. An approval sample will be shipped to the buyer for each lot purchased for the buyer to approve the sample prior to shipment. The buyer must approve or reject the approval sample within 5 days of receipt or the coffee will be considered approved and shipped.<br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">M-Cultivo, our partnering importers, and exporters have also committed to cup your coffees to provide an extra layer of assurance. Though, the ultimate responsibility lies on the buyer once the coffee is shipped.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/p>\"}','2022-04-07 20:08:54','2022-04-07 20:08:54'),(118,'faq.element','{\"question\":\"Is my coffee order guaranteed to ship?\",\"answer\":\"<p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Unfortunately your coffee is not guaranteed to ship. Though, it is highly likely your order will ship. The coffee supply chain can be unpredictable. The availability of freight options within the price paid, a timely shipment, and the quality of the coffee are all difficult to guarantee.\\u00a0<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Our export and import partners have agreed to support the marketplace. They will be as flexible as possible to help ensure your coffee is shipped in a timely cost effective manner.\\u00a0If your coffees do not ship, you will receive a full refund minus the cost of any samples.<\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\"><br style=\\\"margin:0px;font-size:inherit;\\\" \\/><\\/p><p style=\\\"margin-right:0px;margin-left:0px;font-size:18px;color:rgb(0,0,0);font-family:arial;word-spacing:1px;\\\">Please reach out via our chatbot or support@mcultivo.com if you need additional information.<\\/p>\"}','2022-04-07 20:09:10','2022-04-07 20:09:10'),(120,'policy_pages.element','{\"title\":\"Terms and Conditions\",\"details\":\"<span><\\/span><p dir=\\\"ltr\\\" style=\\\"line-height:1.38;margin-top:0pt;margin-bottom:0pt;\\\"><\\/p><div><div><i>Aug 1, 2023<\\/i><\\/div><div><br \\/><\\/div><div><h6><\\/h6><p class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><b>Welcome to the M-Cultivo Marketplace and\\nAuction Platform!<\\/b><\\/span><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">These Terms and Conditions (these \\u201cTerms\\u201d) constitute a legally\\nbinding agreement between you, whether individually or on behalf of an entity\\n(\\u201cyou\\u201d) and the Cultivo Group, including Cultivo Group, LLC, M Cultivo, Inc.,\\nM. Cultivo Limited and their respective subsidiaries and affiliates\\n(collectively, \\u201cM-Cultivo\\u201d, the \\u201cCompany\\u201d, \\u201cwe,\\u201d \\u201cus\\u201d or \\u201cour\\u201d), concerning\\nyour access to and use of \\u00a0<a href=\\\"http:\\/\\/www.mucultivo.com\\/\\\">www.mcultivo.com<\\/a> and any affiliated\\nwebsites (collectively, the \\u201dSite\\u201d), regardless of whether you access the Site\\nvia a personal computer, smartphone, tablet, mobile application or via any\\nother medium. <\\/span><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">1.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">General Use of the Site.<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0 <\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">a.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">You agree that by accessing the Site, you have\\nread, understood, and agree to be bound by these Terms. If you do not agree to\\nthese Terms, then you are expressly prohibited from using the Site and you must\\ndiscontinue use immediately.\\u00a0 We reserve the right, in our sole\\ndiscretion, to make changes or modifications to these Terms and\\/or to the Site\\nat any time and for any reason.\\u00a0 You\\nwaive any right to receive specific notice of any\\u00a0 change or modification.\\u00a0It is your\\nresponsibility to periodically review these Terms to stay informed of updates.\\nBy continuing to use the Site, you will be deemed to have been made aware of\\nand to have accepted, the modifications or changes.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 72pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">b.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">The Site consists of two primary components: (a)\\na marketplace, where coffee producers or sellers (each a \\u201cSeller\\u201d) and coffee\\nbuyers (each a \\u201cBuyer\\u201d) can virtually \\u201cmeet\\u201d (the \\u201cMarketplace\\u201d) to potentially\\nenter into transactions to sell coffee beans and related products (the \\u201cProducts\\u201d);\\nand (b) an auction platform, where auctions occur to purchase coffee beans and\\nrelated products (the \\u201cAuction Platform\\u201d).\\u00a0\\nThese Terms generally apply to all Marketplace transaction (each, a\\n\\u201cMarketplace Transaction\\u201d) and Auction Platform transactions (each, an \\u201cAuction\\nTransaction\\u201d).\\u00a0 Section 20 sets forth\\nadditional terms and conditions that apply to Auction Transactions.\\u00a0 If you are using the Site in your capacity as\\nan employee or agent of a company or any other legal entity, you represent and\\nwarrant that you are authorized to legally bind such entity. \\u00a0If you do not have this authority, you must\\nnot accept these Terms and may not use the Site.\\u00a0 Purchases made through the Marketplace and\\nthe Auction Platform are non-refundable.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">2.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\n<\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Use of\\nthe Site in Individual Countries.<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0 The information provided within the Site is\\nnot intended for distribution to, or use by, any person or entity in any\\njurisdiction or country where such distribution or use would be contrary to law\\nor regulation or which would subject us to any registration requirement.\\u00a0\\u00a0\\u00a0 <\\/span><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><span style=\\\"color:#000000;\\\">The Marketplace and the\\nAuction Platform may not be used in connection with transactions between Buyers\\nand Sellers where the consummation of such transaction would be prohibited by\\napplicable law.\\u00a0 \\u00a0<\\/span><\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:18pt;text-align:justify;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><a><\\/a><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">3.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Creating an Account<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.\\u00a0 You can browse the\\nSite without registering, but in order to become a Buyer, you must create an\\naccount (each, an \\u201cAccount\\u201d). To create an Account, you must be 18 years of age\\nor older.\\u00a0 Account registration will\\nrequire that you provide us with certain personal information such as your\\nname, mailing address, telephone number and email.\\u00a0 When you provide that registration\\ninformation, please make sure that it is accurate and complete. Do not\\nimpersonate anyone else. We reserve the right to close or cancel an Account at\\nany time for any reason.\\u00a0 You are\\nresponsible for all the activity on your Account.\\u00a0 Keep your username and password confidential\\nand do not share with third parties. If you learn that someone used your\\nAccount without permission, report it promptly to<\\/span><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><span style=\\\"color:#000000;\\\"> <\\/span><a href=\\\"mailto:support@mcultivo.com\\\">support@mcultivo.com<\\/a><span style=\\\"color:#000000;\\\">.\\u00a0<\\/span><\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:18pt;text-align:justify;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">4.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\n<\\/span><\\/span><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><u><span style=\\\"color:#000000;\\\">Security<\\/span><\\/u><span style=\\\"color:#000000;\\\">.\\u00a0 We will maintain\\ncommercially reasonable security measures in providing the Site. We reserve the\\nright to suspend access to the Site in the event of a suspected or actual\\nsecurity breach.\\u00a0 However, no security\\nsystem is foolproof.\\u00a0 However, we are not\\nliable for any damages incurred in connection with any unauthorized access\\nresulting from the actions or omissions of you or any third-party.\\u00a0 The Site may be hosted by a third-party cloud\\nhosting service provider (the \\u201cHosting Provider\\u201d).\\u00a0 M-Cultivo is not responsible for the acts or\\nomissions of the Hosting Provider.\\u00a0 It is\\nyour responsibility to safeguard your credentials.\\u00a0 If any unauthorized user bids on your behalf\\nas a result of unauthorized access, such bid is your responsibility.\\u00a0 Multi-factor authentication is available and its\\nuse is encouraged.<\\/span><\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:18pt;text-align:justify;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">5.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Things Not to Do<\\/span><\\/u><b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.<\\/span><\\/b><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><font color=\\\"#000000\\\">\\u00a0 Below is a\\nrepresentative, but non-exhaustive, list of things you should not do with\\nrespect to the Site, the Marketplace or the Auction Platform:<br \\/><\\/font><span style=\\\"font-size:0.875rem;\\\">Do not lie.<\\/span><br \\/><\\/span><\\/p><ol style=\\\"margin-top:0cm;\\\" start=\\\"5\\\" type=\\\"1\\\"><li><ul><li>Do not create an Account\\n      using any false information.<br \\/>Do not submit payment\\n      information that is false, inaccurate, deceptive, or fraudulent. <br \\/>Do not interfere with the\\n      working or functionality of the Site, the Marketplace or the Auction\\n      Platform.<br \\/>Do not bypass any security\\n      measures.<br \\/>Do not damage or obtain\\n      unauthorized access to any system, data, password, or other information,\\n      whether it belongs to M-Cultivo or another person or entity.<br \\/>Do not take any action that\\n      imposes an unreasonable load on Site infrastructure or on our third-party\\n      providers and partners.<br \\/>Do not use any kind of\\n      software or device, whether it is manual or automated, to \\u201ccrawl\\u201d,\\n      \\u201cscrape\\u201d or \\u201cspider\\u201d any part of the Site.<br \\/>Do not reverse engineer any\\n      aspect of the Site, the Marketplace or the Auction Platform.<\\/li><\\/ul><ol style=\\\"margin-top:0cm;\\\" start=\\\"1\\\" type=\\\"a\\\"><li class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;line-height:normal;background-size:initial;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><b style=\\\"color:rgb(0,0,0);\\\">\\n      <br \\/>\\n      <\\/b><\\/span><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><\\/span><p><\\/p><\\/li>\\n <\\/ol><\\/li>\\n<\\/ol>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">6.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\n<\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><u>How the Marketplace and Auction Platform Work<\\/u><b>.<\\/b><\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">a.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">The Marketplace and the Auction Platform each\\nrespectively provide a sales platform for Buyers to purchase Products directly\\nfrom Sellers through a pre-negotiated supply chain (the \\u201cSupply Chain\\u201d). This\\nSupply Chain is contingent upon the minimum agreed upon amount of Products\\nbeing sold in any given market as determined by the importer and\\nexporter.\\u00a0When a Buyer purchases Products through the Marketplace or the\\nAuction Platform, the Buyer is obligated to pay for, and receive, the Products\\npurchased and pay any applicable delivery charges.\\u00a0 When a Seller consents to have their Products\\nlisted on the Marketplace or within the Auction Platform, the Seller is\\nagreeing to hold the exact Products and quantity until those Products are\\npurchased.\\u00a0 After purchase, the Products\\nwill be delivered to the agreed-upon exporter for transportation. Products will\\nbe released back to the Seller if they have not sold during the agreed-upon\\nduration of a Marketplace Transaction or an Auction Transaction.\\u00a0 If a Seller fails to fulfill its obligations,\\na refund will be issued to the Buyer whenever possible.\\u00a0<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 54pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">b.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">A Buyer has the option to: (i) have us manage\\nthe supply chain for a management fee; or (ii) manage from the exporter (FOB)\\nforward.\\u00a0 Exporter fees are\\npre-negotiated for each event, but importer and other supply chain fees are\\nnot.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 54pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">c.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">The Seller agrees to: (i) deliver the Products\\nto the exporter\\u2019s designated warehouse once sold; (ii) pay transport charges to\\nthe exporter to the extent applicable; and (iii) \\u00a0deliver the quality as advertised on the Site.\\u00a0<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">d.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\n<\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">M-Cultivo passes all orders to\\nthe exporter and importer for fulfillment. However, it is the responsibility of\\nthe exporter and importer, not us, to fulfill the orders and handle the\\nlogistics of shipping the Products to you.\\u00a0\\nM-Cultivo will provide shipping quotes upon request.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/u><\\/p><p><u><span>\\u00a0<\\/span><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">7.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Fees.<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"> \\u00a0We charge a percentage fee for each Marketplace\\nTransaction.\\u00a0You are responsible for paying additional fees or taxes\\nassociated with your use of the Marketplace.\\u00a0\\u00a0 Fees for Auction Platform Transactions\\nvary.\\u00a0 You expressly authorize us and our\\nthird-party payment processor, such as Stripe (each, a \\u201cPayment Processor\\u201d) to\\ncharge you the applicable fee.\\u00a0 Fees are\\nnot collected by us, but rather by the Payment Processor.\\u00a0 We may ask you to supply additional\\ninformation relevant to the fee, including your credit card number, the\\nexpiration date of your credit card and your email and postal addresses for\\nbilling and notification (such information, collectively referred to as the\\n\\u201cPayment Information\\u201d). You represent and warrant that you have the legal right\\nto use all payment method(s) represented by any such Payment Information.\\u00a0 By providing us with Payment Information, you\\nare agreeing to the Payment Processor\\u2019s terms of service.<u><\\/u><\\/span><\\/p><p><u><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;line-height:normal;border:none;\\\"><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/u><\\/p><p><u><span>\\u00a0<\\/span><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">8.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Other Websites<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.\\u00a0 The Marketplace or\\nthe Auction Platform may contain links to third-party websites that we do not\\nown, operate or control. When you access third-party websites, you do so at\\nyour own risk. We do not control or endorse those websites.\\u00a0<u><\\/u><\\/span><\\/p><p><u><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;\\\"><u><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/u><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">9.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Intellectual Property Rights<\\/span><\\/u><b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.\\u00a0 <\\/span><\\/b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">The Site, the Marketplace and the Auction Platform and all of their\\nrespective enhancements, upgrades, modifications, customizations, derivative\\nworks, selections, algorithms, compilations, aggregations, source code and\\/or\\nobject code, and copies thereof, and all information, methods, processes and\\nall intellectual property contained therein as well as any audio, video, text,\\nphotos or graphics (collectively, the \\u201cSite IP\\u201d) are and will remain the\\nproperty of M-Cultivo.\\u00a0 M-Cultivo has and\\nwill retain exclusive right and title to, and has all patent, copyright,\\ntrademark, trade secret and all other intellectual property rights in and to\\nthe Site IP.\\u00a0 Nothing in this Agreement\\nwill be construed as transferring any aspects of such rights to you with the\\nexception of your limited right to use and access the Site.\\u00a0 M-Cultivo shall have the right to register\\npatents, trademarks and copyrights related to the Site with any governmental\\nauthority anywhere in the world.\\u00a0 The\\nSite IP is provided on an \\u201das-is\\u201d basis without representation or warranty.\\nExcept as expressly provided in these Terms, no part of the Site IP may be\\ncopied, reproduced, aggregated, republished, uploaded, posted, publicly\\ndisplayed, encoded, translated, transmitted, distributed, sold, licensed, or\\notherwise exploited for any commercial purpose whatsoever, without our express\\nprior written permission.\\u00a0 Provided that\\nyou are eligible to use the Site and comply with these Terms, you are granted a\\nlimited license to access and use the Site.<u><\\/u><\\/span><\\/p><p><u><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/u><\\/p><p><u><span>\\u00a0<\\/span><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">10.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0 <\\/span><\\/span><b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/b><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Site Modifications and Interruptions.<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0 We\\nreserve the right to change, modify, or remove the contents of the Site at any\\ntime or for any reason at our sole discretion and without notice. However, we\\nhave no obligation to update any information on our Site. We also reserve the\\nright to discontinue all or part of the Site without notice at any\\ntime.\\u00a0We will not be liable to you or any third party for any\\nmodification, price change, suspension, or discontinuance of the Site.\\u00a0 We cannot guarantee the Site will be\\navailable at all times. We may experience hardware, software, or other problems\\nor need to perform maintenance related to the Site, resulting in interruptions,\\ndelays, or errors.\\u00a0 You agree that we\\nhave no liability whatsoever for any loss, damage, or inconvenience caused by\\nyour inability to access or use the Site during any downtime or discontinuance\\nof the Site.\\u00a0 Nothing in these Terms will\\nbe construed to obligate us to maintain and support the Site or to supply any\\ncorrections, updates, or releases in connection therewith.<u><\\/u><\\/span><\\/p><p><u><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/u><\\/p><p><u><span>\\u00a0<\\/span><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">11.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Term and Termination.<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0 These\\nTerms shall remain in full force and effect while you use the Site, the\\nMarketplace or the Auction Platform. WITHOUT LIMITING ANY OTHER PROVISION OF\\nTHESE TERMS AND CONDITIONS, WE RESERVE THE RIGHT TO, IN OUR SOLE DISCRETION AND\\nWITHOUT NOTICE OR LIABILITY, DENY ACCESS TO AND USE OF THE SITE (INCLUDING\\nBLOCKING CERTAIN IP ADDRESSES), TO ANY PERSON FOR ANY REASON OR FOR NO REASON,\\nINCLUDING WITHOUT LIMITATION FOR BREACH OF ANY REPRESENTATION, WARRANTY, OR\\nCOVENANT CONTAINED IN THESE TERMS OR FAILURE TO COMPLY WITH APPLICABLE LAW OR\\nREGULATION. WE MAY TERMINATE YOUR USE OR PARTICIPATION IN THE SITE OR DELETE\\nYOUR ACCOUNT AND ANY CONTENT OR INFORMATION AT ANY TIME, WITHOUT WARNING, IN\\nOUR SOLE DISCRETION.\\u00a0If we terminate or suspend your Account for any\\nreason, you are prohibited from registering and creating a new Account under\\nyour name, a fake or borrowed name, or the name of any third party, even if you\\nmay be acting on behalf of the third party.\\u00a0In addition to terminating or\\nsuspending your Account, we reserve the right to take\\u00a0appropriate legal\\naction, including without limitation pursuing civil, criminal, and injunctive\\nredress.<u><\\/u><\\/span><\\/p><p><u><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/u><\\/p><p><u><span>\\u00a0<\\/span><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">12.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\n<\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Governing\\nLaw.<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0 These Terms and your use of the Site are\\ngoverned by and construed in accordance with the laws of the State of Georgia\\n(USA) applicable to agreements made and to be entirely performed within the\\nState of Georgia, without regard to its conflict of law principles.\\u00a0 Any dispute related to these Terms or your\\nuse of the Site shall be submitted solely to the state or federal courts\\nlocated in Fulton County, Georgia.\\u00a0 You\\nand us submit exclusively to such jurisdiction and venue.\\u00a0 Judgments may be enforced in any court in the\\nworld having jurisdiction over such matters.\\u00a0\\nYOU AND US WAIVE ANY RIGHT TO A TRIAL BY JURY FOR A DISPUTE RELATED TO\\nTHESE TERMS OR YOUR USE OF THE SITE.\\u00a0 TO\\nTHE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, YOU AND US AGREE THAT NEITHER\\nPARTY SHALL BRING ANY LAWSUIT, ACTION, PROCEEDING OR CLAIM OF ANY NATURE\\nPERTAINING TO THESE TERMS OR YOUR USE OF THE SITE AGAINST THE OTHER PARTY AS A\\nPLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS, COLLECTIVE, OR REPRESENTATIVE\\nPROCEEDING.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:18pt;text-align:justify;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">13.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Indemnification.<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0 You agree to\\ndefend, indemnify, and hold us harmless, including our subsidiaries,\\naffiliates, and all of our respective officers, agents, partners, and\\nemployees, from and against any loss, damage, liability, claim, or demand,\\nincluding reasonable attorneys\\u2019 fees and expenses, made by any third party due\\nto or arising out of: (a) use of the Site; (b) a breach by you of these Terms;\\n(c) any breach of your representations and warranties set forth in these Terms;\\n(d) your violation of the rights of a third party, including but not limited to\\nintellectual property rights; or (e) any overt harmful act toward any other\\nuser of the Site.\\u00a0 Notwithstanding the\\nforegoing, we reserve the right, at your expense, to assume the exclusive\\ndefense and control of any matter for which you are required to indemnify us,\\nand you agree to cooperate, at your expense, with our defense of such claims.\\nWe will use reasonable efforts to notify you of any such claim, action, or\\nproceeding which is subject to this indemnification upon becoming aware of\\nit.\\u00a0<u><\\/u><\\/span><\\/p><p><u><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">14.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Limitation of Liability<\\/span><\\/u><b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.\\u00a0 <\\/span><\\/b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">TO THE FULLEST EXTENT PERMITTED BY LAW, IN NO EVENT WILL M-CULTIVO, ITS\\nPARENTS, SUBSIDIARIES, AFFILIATES AND EACH OF THEIR RESPECTIVE OFFICERS,\\nDIRECTORS, EMPLOYEES, PARTNERS, SUPPLIERS, AGENTS, REPRESENTATIVES, SUCCESSORS\\nOR ASSIGNS (COLLECTIVELY THE \\u201cCOMPANY PARTIES\\u201d) BE LIABLE FOR ANY INDIRECT,\\nINCIDENTAL, PUNITIVE, CONSEQUENTIAL, SPECIAL, OR EXEMPLARY DAMAGES OF ANY KIND,\\nINCLUDING BUT NOT LIMITED TO DAMAGES: (A) RESULTING FROM YOUR ACCESS TO, USE\\nOF, OR INABILITY TO ACCESS OR USE THE SITE; (C) FOR ANY LOST PROFITS, DATA\\nLOSS, OR COST OF PROCUREMENT OR SUBSTITUTE GOODS OR SERVICES; OR (C) FOR ANY\\nCONDUCT OF CONTENT OF ANY THIRD PARTY EVEN IF A COMPANY PARTY HAS BEEN ADVISED\\nAS TO THE FORESEEABILITY OF THE SAME. IN NO EVENT SHALL THE COMPANY\\u2019S PARTIES\\u2019\\nLIABILITY FOR DIRECT DAMAGES BE IN EXCESS OF (IN THE AGGREGATE) TWO HUNDRED AND\\nFIFTY U.S. DOLLARS ($250).<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">15.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Disclaimer Of Warranties<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.\\u00a0 We do\\nnot guarantee, represent or warrant that your use of the Site, the Marketplace\\nor the Auction Platform will be uninterrupted, timely, secure or\\nerror-free.\\u00a0 You expressly agree that\\nyour use of, or inability to use, the Site is at your sole risk. THE SITE IS\\nPROVIDED \\u201cAS IS\\u201d AND \\u201cAS AVAILABLE\\u201d, WITHOUT ANY REPRESENTATIONS OR WARRANTIES,\\nEITHER EXPRESS OR IMPLIED.\\u00a0 WE DISCLAIM\\nALL IMPLIED WARRANTIES OR CONDITIONS, INCLUDING, WITHOUT LIMITATION, THOSE OF\\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, TITLE, AND NON-INFRINGEMENT.\\n<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;border:none;\\\"><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\"><\\/span><\\/u><\\/p><p><u><span>\\u00a0<\\/span><\\/u><\\/p><u><\\/u>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-left:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">16.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Use of Personal Data<\\/span><\\/u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.<b>\\u00a0 \\u201c<\\/b>Personal Data\\u201d will be requested from\\nyou as part of your use of the Site.\\u00a0\\u00a0\\nTypically, this Personal Data will include your name, email address,\\ntelephone, and internet protocol address, but it could include items such as\\ngender, ethnicity, other biographical or demographic information, and metadata,\\nsuch as location, time and device information.\\u00a0\\nPersonal Data will be handled and processed in accordance with our\\nPrivacy Policy [Appendix A], which you agree to be bound by.\\u00a0 You grant us permission to use the Personal\\nData to contact you with respect to the Site and Site-related inquiries,\\nsurveys, customer care, technical support and\\/or to provide you with marketing\\nmaterials or information of interest from the Company or third parties. <\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-left:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">17.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\n<\\/span><\\/span><u><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">Use of Anonymized Data<\\/span><\\/u><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">.\\u00a0 For purposes of these\\nTerms, \\u201cAnonymized Data\\u201d means your Personal Data once your name, mailing\\naddress (other than zip code), email address, telephone number and internet\\nprotocol address have been removed.\\u00a0 You\\ngrant the Company without charge or other compensation, the perpetual,\\nirrevocable, non-exclusive right, with or without your knowledge, throughout\\nthe universe, in any medium now known or hereinafter created, to use the\\nAnonymized Data, in connection with the development, marketing, promotion and\\nsale of our products and services, including, but not limited to, the\\nimprovement of the Site.\\u00a0 Moreover, we\\nhave the unrestricted right to license or sell the Anonymized Data to third\\nparties for any purpose.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-left:0cm;text-align:justify;text-indent:18pt;line-height:normal;border:none;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">18.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\n<\\/span><\\/span><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><u>Contacting You.<\\/u>\\u00a0 By using the Site, you expressly authorize\\nthe Company Parties and service providers (collectively, the \\\"Messaging\\nParties\\\") to contact you using automated telephone dialing systems,\\nartificial or prerecorded voice messaging, text messaging, and automated email\\nsystems in order to provide any and all relevant information as well as to\\nmarket products and services to you.\\u00a0 You\\nauthorize the Messaging Parties to make such contacts using any telephone\\nnumbers (including wireless, landline, mobile, VOIP numbers and hereinafter\\ndeveloped technology and regardless of whether such number is on the Do Not\\nCall Registry) or email addresses you supply to us.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:18pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">19.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0 <\\/span><\\/span><u><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Miscellaneous<\\/span><\\/u><b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">.\\u00a0\\n<\\/span><\\/b><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">These Terms are the\\nentire agreement between you and M-Cultivo with respect to the Site. They\\nsupersede all other communications and proposals, whether oral, written, or\\nelectronic, between you and M-Cultivo with respect to the Site and govern our\\nfuture relationship. If there is any conflict between the provisions of any\\nother contracts on the Site and these Terms, the provisions set forth in these\\nTerms shall govern.\\u00a0 For the avoidance of\\ndoubt, in the event of a conflict between the provisions of these Terms and the\\nprovisions of any other agreement between a Buyer and a Seller, the provision\\nof these Terms shall control.\\u00a0 If any\\nprovision of these Terms is found to be invalid under the law, that provision\\nwill be limited or eliminated to the minimum extent necessary so that the Terms\\notherwise will remain in full force and effect and enforceable. The failure of\\neither you or M-Cultivo to exercise any right provided for in these Terms in\\nany way will not be deemed a waiver of any other rights.\\u00a0 These Terms are personal to you. You cannot\\nassign them, transfer them, or sublicense them. M-Cultivo has the right to\\nassign, transfer, or delegate any of its rights and obligations under these\\nTerms without your consent.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 18pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<ol style=\\\"margin-top:0cm;\\\" start=\\\"20\\\" type=\\\"1\\\">\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#000000;margin-bottom:0cm;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><u><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">Additional Terms for the Auction Platform.<\\/span><\\/u><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\"><\\/span><p><\\/p><\\/li>\\n<\\/ol>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">a.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">The Auction Platform provides a mechanism for\\nSellers to sell and Buyers to buy Products.\\u00a0\\nAlthough we are facilitating the Auction Transaction process, we are not\\na party to the contract for sale and purchase entered into between Buyer and\\nSeller. We are not in control of the information made available by\\nSellers.\\u00a0 We do not provide any quality\\ncontrol or quality assurance services.\\u00a0\\nWe do not and cannot make any representations, warranties or guarantees\\nin connection with the Seller or the Products.\\u00a0\\nWe are not an agent or representative of the Seller.\\u00a0 Any individual Auction Transaction may be\\nterminated or discontinued at any time.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm 0cm 0cm 54pt;text-align:justify;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">b.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">The current Auction Transaction process, which\\nis subject to change, typically works as follows: (i) each potential Buyer signs\\nin to the Auction Platform; (ii) the potential Buyer requests access to a\\nspecific Auction Transaction and accepts the rules of that specific Auction\\nTransaction (rules may vary for different Auction Transactions); (iii) the\\nSeller has final approval of which potential Buyers may access the Auction\\nTransaction; and (iv) at the conclusion of the Auction Transaction, each\\nsuccessful Buyer receives an email with payment instructions.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">c.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">Most, albeit not all, of the Auction\\nTransactions that take place will be \\u201cTimed Auctions\\u201d.\\u00a0 That is, they will start and end at specified\\ntimes.\\u00a0 Timed Auction bids are placed over\\na defined time period (\\u201cBidding Period\\u201d).\\u00a0\\nBuyers can view the highest current bid. The highest bid, being the\\ngreatest bid that meets or is in excess of a reserve bid, if any, at the end of\\nthe Bidding Period will be the \\u201csuccessful\\u201d bid. <\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">d.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">To bid, a Buyer must have a registered Account with\\nus.\\u00a0 We reserve the right to ban a Buyer\\nfrom any specific Auction Transaction or from the Auction Platform in general\\nif, in our sole discretion, your conduct poses a risk to our integrity or\\nreputation or we have reason to believe you do not possess the financial or\\nlogistical wherewithal to complete an Auction Transaction. <\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:36pt;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"margin:0cm;text-align:justify;text-indent:54pt;line-height:normal;background-size:initial;border:none;\\\"><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">e.<span style=\\\"font-size:7pt;line-height:normal;font-family:\'Times New Roman\';\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><span lang=\\\"en-us\\\" style=\\\"color:#000000;\\\" xml:lang=\\\"en-us\\\">In addition to these Terms, a Seller may have\\nseparate transaction terms that apply to a specific Auction Transaction.\\u00a0 Each Buyer agrees that in bidding during an\\nAuction Transaction, Buyer accepts the specific terms and conditions of that\\nSeller and any other terms which may be implied by law, whether or not these\\nhave been brought to Buyer\\u2019s attention.\\u00a0\\nBy making a bid or commitment to buy Products, Buyer is committing to\\nbuy the item from the Seller if Buyer\\u2019s bid is the highest. In such\\ncircumstances, Buyer and Seller are entering into a legally binding contract\\nand Buyer is obligated to purchase the Products.\\u00a0 Buyer shall look solely to the Seller to\\nresolve all questions and disputes regarding Products that are purchased.<\\/span><\\/p><p><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align:justify;line-height:normal;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">\\u00a0<\\/span><\\/p>\\n\\n<p class=\\\"MsoNormal\\\" style=\\\"text-align:justify;line-height:normal;\\\"><span lang=\\\"en-us\\\" xml:lang=\\\"en-us\\\">Last Updated: August 1, 2023.<\\/span><\\/p><p><\\/p><\\/div><div><br \\/><h6><b>Appendix A<\\/b><\\/h6><br \\/><div><font color=\\\"#212529\\\"><i>M Cultivo data privacy statement:<\\/i><br \\/><br \\/><\\/font><\\/div><div><font color=\\\"#212529\\\">M cultivo is committed to managing the data of our users in a responsible and ethical manner,\\u00a0<\\/font><span style=\\\"color:rgb(33,37,41);font-size:1rem;\\\">respecting the rights of those users and the responsibilities of M cultivo as data holders.<\\/span><\\/div><div><font color=\\\"#212529\\\"><br \\/><\\/font><\\/div><div><font color=\\\"#212529\\\">We will identify the benefit to the users of collecting, processing and storing their data as well\\u00a0<\\/font><span style=\\\"color:rgb(33,37,41);font-size:1rem;\\\">as the benefit to the company.<\\/span><\\/div><div><font color=\\\"#212529\\\"><br \\/><\\/font><\\/div><div><font color=\\\"#212529\\\">We recognize the right of users to consent to the uses we make of the data we hold on them.<br \\/><br \\/><\\/font><\\/div><div><font color=\\\"#212529\\\">We recognize there are specific categories\\u2019 of personal data that are sensitive, for example\\u00a0<\\/font><span style=\\\"color:rgb(33,37,41);font-size:1rem;\\\">racial or ethnic origin, political opinions, religious or philosophical beliefs, or trade union\\u00a0<\\/span><span style=\\\"color:rgb(33,37,41);font-size:1rem;\\\">membership. Such data will only be collected if there is a clear benefit to the user, that their\\u00a0<\\/span><span style=\\\"color:rgb(33,37,41);font-size:1rem;\\\">prior consent is obtained and that the security of that data can be guaranteed.<\\/span><\\/div><div><font color=\\\"#212529\\\"><br \\/><\\/font><\\/div><div><font color=\\\"#212529\\\">We will not use data processing techniques that are intrusive, and which may pose a risk to the\\u00a0<\\/font><span style=\\\"color:rgb(33,37,41);font-size:1rem;\\\">rights and freedoms of the users or which are open to misuse.<\\/span><\\/div><div><font color=\\\"#212529\\\"><br \\/><\\/font><\\/div><div><font color=\\\"#212529\\\">We will establish a robust internal process to assess ethical compliance with our policies, to\\u00a0<\\/font><span style=\\\"color:rgb(33,37,41);font-size:1rem;\\\">manage risk to our ethical approach and encourage a process of internal challenge.<\\/span><\\/div><div><font color=\\\"#212529\\\"><br \\/><\\/font><\\/div><div><font color=\\\"#212529\\\">We will regularly audit our approach to ethics and the rights of our users.<\\/font><\\/div><\\/div><\\/div>\"}','2022-07-30 13:52:30','2023-08-02 15:56:19'),(128,'blog.element','{\"has_image\":[\"1\"],\"title\":\"Understanding the EU Regulation on Coffee Traceability: What Coffee Producers Need to Know\",\"description_nic\":\"<p><br \\/>In the evolving landscape of global trade, the European Union (EU) has recently introduced new regulations regarding the traceability of coffee. These regulations, known as the EUDR, are set to take effect on December 29, 2024. Coffee supply chain compliance with the EUDR is required in order to access the European market, making it essential for supply chain actors to remain abreast of regulation updates and changes as they are announced.<br \\/><br \\/>M-Cultivo, committed to helping producers grow their businesses, breaks down the intricate details of the EU regulation to help you remain compliant and competitive. We\\u2019ve done the research with the aim of breaking the new regulations down into a few digestible points below.<br \\/><b><br \\/>The Essence of the EU Coffee Traceability Regulation:<\\/b><br \\/><br \\/><\\/p><p>The main objective behind the EU\'s decision to regulate coffee traceability is to ensure that coffee products entering its markets are responsibly sourced and free from sourcing from deforested land. By understanding the origin of each coffee, consumers can make informed choices, and regulators can better enforce equitable trade practices.<\\/p><p><br \\/><b>Traceability Requirements:<\\/b><br \\/><br \\/><\\/p><p><b>Documentation:<\\/b>\\u00a0Farmers and aggregators must maintain comprehensive records of the entire supply chain journey of their coffee beans from the moment they are harvested, referencing the plots with geolocation using tools, up to their entry into the EU.<br \\/><\\/p><p><b>Digital Tools:<\\/b>\\u00a0Modern technology has been recommended for tracking to provide real-time updates and ensure accuracy. Solutions like digital databases have gained prominence.<br \\/><\\/p><p><b>Third-party Verification:<\\/b>\\u00a0To foster trust, the EU suggests third-party organizations to verify the traceability data provided by farmers.<br \\/><br \\/><b>Implications for Coffee Producers:<\\/b><br \\/><br \\/><\\/p><p><b>Operational Changes:<\\/b>\\u00a0Implementing traceability mechanisms may require changes in harvesting, storing, and shipping methods to ensure data accuracy.<\\/p><p>Financial Implications:\\u00a0While initial setup costs for traceability systems might be significant, they promise long-term benefits in the form of sustained market access, potential premium pricing, and enhanced consumer trust.<br \\/><br \\/><b>Why Compliance Matters- The Benefits Beyond Market Access:<br \\/><\\/b><br \\/><\\/p><p>Understanding and complying with the EU\'s coffee traceability regulation isn\'t just about ensuring producers\\u2019 products find their way to European shelves. It\'s about elevating the entire coffee industry. Here are some benefits producers stand to gain:<br \\/><br \\/><\\/p><p><b>Enhanced Credibility:\\u00a0<\\/b>Compliance boosts your brand\'s image. When retailers and consumers see that a producer adheres to stringent traceability standards, it instills trust and loyalty.<br \\/><br \\/><\\/p><p><b>Future-Proofing:\\u00a0<\\/b>As more markets worldwide move toward traceability requirements, early adaptation prepares producers for evolving global demands.<\\/p><p><br \\/>The EU\'s coffee traceability regulation is more than just a bureaucratic requirement; it\'s a testament to the evolving nature of trade, prioritizing responsible and sustainable practices, and the ever-increasing power of the consumer\\u2019s voice. M-Cultivo\'s Cultivo Pro platform aims to simplify this transition for coffee producers through providing them with user-friendly tools to support their compliance with the EUDR. As the coffee industry looks to the future, the roles of traceability and transparency will undoubtedly be pivotal in shaping its trajectory.<br \\/><br \\/><\\/p><p>Are you interested in gaining a deeper understanding of the EUDR? <font color=\\\"#006699\\\"><a href=\\\"https:\\/\\/calendar.google.com\\/calendar\\/u\\/0\\/appointments\\/schedules\\/AcZssZ0FaUnT9rQ-FjQDNXBe1eucOkL0zD_0xKn_WhtoKZbIjjs-4uHfrxgu3wVNlUA15fMOHd3nSL-P\\\" title=\\\"\\\"><font color=\\\"#006699\\\">Book a call<\\/font><\\/a>,<\\/font> and we will be happy to support you throughout this process. You can also review the requirements document <a href=\\\"https:\\/\\/environment.ec.europa.eu\\/topics\\/forests\\/deforestation\\/regulation-deforestation-free-products_en\\\" title=\\\"\\\"><font color=\\\"#006699\\\">here<\\/font><\\/a>, and stay tuned for more on how M-Cultivo can assist coffee producers in complying with regulations.<br \\/><br \\/><\\/p><p>References:<\\/p><p dir=\\\"ltr\\\" style=\\\"margin-top:0pt;margin-bottom:0pt;line-height:1.38;\\\"><\\/p><p><font color=\\\"#006699\\\"><a href=\\\"https:\\/\\/environment.ec.europa.eu\\/topics\\/forests\\/deforestation\\/regulation-deforestation-free-products_en\\\" style=\\\"text-decoration:inherit;\\\" class=\\\"notion-link-token notion-focusable-token notion-enable-hover\\\"><span style=\\\"border-bottom:0.05em solid rgba(55,53,47,0.4);border-top-color:rgba(55,53,47,0.4);border-right-color:rgba(55,53,47,0.4);border-left-color:rgba(55,53,47,0.4);\\\" class=\\\"link-annotation-unknown-block-id--1760106187\\\">https:\\/\\/environment.ec.europa.eu\\/topics\\/forests\\/deforestation\\/regulation-deforestation-free-products_en<\\/span><\\/a>\\n<br \\/><br \\/><a href=\\\"https:\\/\\/perfectdailygrind.com\\/2023\\/05\\/how-can-coffee-comply-with-new-eu-deforestation-regulations\\/\\\" style=\\\"text-decoration:inherit;\\\" class=\\\"notion-link-token notion-focusable-token notion-enable-hover\\\"><span style=\\\"border-bottom:0.05em solid;border-color:rgba(55,53,47,0.4);\\\" class=\\\"link-annotation-unknown-block-id--1986743828\\\">https:\\/\\/perfectdailygrind.com\\/2023\\/05\\/how-can-coffee-comply-with-new-eu-deforestation-regulations\\/<\\/span><\\/a>\\n<br \\/><br \\/><a href=\\\"https:\\/\\/www.forest-trends.org\\/wp-content\\/uploads\\/2021\\/02\\/10-things-to-know-about-coffee-production.pdf\\\" style=\\\"text-decoration:inherit;\\\" class=\\\"notion-link-token notion-focusable-token notion-enable-hover\\\"><span style=\\\"border-bottom:0.05em solid rgba(55,53,47,0.4);border-top-color:rgba(55,53,47,0.4);border-right-color:rgba(55,53,47,0.4);border-left-color:rgba(55,53,47,0.4);\\\" class=\\\"link-annotation-unknown-block-id-1615330923\\\">https:\\/\\/www.forest-trends.org\\/wp-content\\/uploads\\/2021\\/02\\/10-things-to-know-about-coffee-production.pdf<\\/span><\\/a><\\/font> <br \\/><br \\/><\\/p><p><br \\/><\\/p><p style=\\\"margin-top:0px;margin-bottom:0px;color:rgb(91,110,136);font-size:0.875rem;line-height:1.7;font-weight:400;\\\"><\\/p><blockquote style=\\\"margin:0px 0px 0px 40px;border:none;padding:0px;\\\"><\\/blockquote>\",\"blog_image\":\"659f43c16a33b1704936385.jpg\"}','2024-01-11 01:26:25','2024-01-12 18:32:42'),(129,'blog.element','{\"has_image\":[\"1\"],\"title\":\"How M-Cultivo Can Support Coffee Farmers and Industry Compliance With The New EU Regulations\",\"description_nic\":\"<font color=\\\"#000000\\\"><font size=\\\"2\\\">As the global demand for coffee continues to rise, so does the need for sustainable practices within the industry. The European Union (EU) has recognized the significance of deforestation in coffee production and implemented new regulations aimed at ensuring traceability and combating deforestation. Coffee entering the EU is expected to comply with these new regulations, known as the EUDR, by December 2024.  In this blog post, we will explore how M-Cultivo\'s CultivoPro, a comprehensive software solution, assists stakeholders in the coffee industry - from the farmer level through to importers and beyond - in complying with requirements while promoting sustainable practices.<br \\/><br \\/><b>Understanding the EUDR:<br \\/><br \\/><\\/b>The regulations on preventing deforestation have established strict product traceability standards for coffee products entering the European Union. All parties involved in the coffee supply chain are required to provide evidence of sustainable sourcing and prove their supply chains are free from deforestation. <a href=\\\"https:\\/\\/mcultivo.com\\/blog\\/128\\/understanding-the-eu-regulation-on-coffee-traceability-what-coffee-producers-need-to-know\\\" title=\\\"\\\"><span class=\\\"discussion-id-ca958e1f-880e-4d88-ad33-2db7cefea719 notion-enable-hover\\\">You can learn more about the EUDR here<\\/span>.<\\/a>\\u00a0\\u00a0<br \\/><\\/font><br \\/><br \\/><\\/font><h2><\\/h2><h3><font color=\\\"#000000\\\">Understanding How CultivoPro Supports Coffee Supply Chain Compliance with the EUDR:<\\/font><\\/h3><font color=\\\"#000000\\\"><br \\/><\\/font><p><font color=\\\"#000000\\\" size=\\\"2\\\"><b>Compliance with Regulations:\\n<\\/b><br \\/>CultivoPro integrates geolocation references using polygons, which in agricultural area mapping are shapes that delineate and represent specific farm plots or land uses on a map. With this integration, CultivoPro allows all users to accurately track the origin of coffee beans. This supports compliance with the stringent traceability requirements within the EUDR.<br \\/><br \\/><\\/font><\\/p>\\n<p><font color=\\\"#000000\\\" size=\\\"2\\\"><b>Technology Access and Support:<\\/b><br \\/>\\u00a0CultivoPro is an accessible, user-friendly software solution designed with the needs of coffee farmers in mind. Training and ongoing support are offered to help users integrate CultivoPro into their multifaceted operations, enabling them to efficiently track their coffee and gain greater visibility into and understanding of their practices related to coffee reception, processing, inventory, and relationship records. With data readily available and easily understood, producers can make faster, well-informed decisions to enhance operational efficiencies and reduce costs.<br \\/><br \\/><\\/font><\\/p>\\n<p><font color=\\\"#000000\\\" size=\\\"2\\\"><b>Attractive to Coffee Buyers:\\n<\\/b><br \\/>By adhering to the EUDR, farmers and aggregators enhance their market reputation and access to both existing and new opportunities for business in the EU. CultivoPro provides buyers with a streamlined way to easily access and check the product traceability data provided by farmers and aggregators, simplifying their verification process. As buyers identify early adopters of compliance with the EUDR, they are poised to avoid supply chain interruptions and work closely with their suppliers to meet their sourcing needs and goals.<br \\/><br \\/><\\/font><\\/p>\\n<p><font color=\\\"#000000\\\" size=\\\"2\\\">M-Cultivo supports a sustainable future for coffee production. Compliance with the EUDR and demonstrating a lack of deforestation in coffee supply chains pose significant challenges to both coffee suppliers and buyers across the supply chain. CultivoPro provides the coffee industry with a robust tool for managing product traceability designed specifically for use within the coffee supply chain. By adhering to the EUDR and encouraging sustainable practices, CultivoPro users promote the safeguarding of forests and preservation of biodiversity while playing a crucial role in addressing climate change. With a focus tailored to the needs of coffee farmers and aggregators, CultivoPro provides a comprehensive solution extending beyond mere compliance in turn fostering a more responsible coffee industry.<br \\/><br \\/><\\/font><\\/p>\\n<p><font size=\\\"2\\\"><font color=\\\"#000000\\\">Interested in learning more about how M-Cultivo can support your coffee business? You can schedule a demo of CultivoPro with one of our team members <\\/font><a href=\\\"https:\\/\\/calendar.google.com\\/calendar\\/u\\/0\\/appointments\\/schedules\\/AcZssZ0FaUnT9rQ-FjQDNXBe1eucOkL0zD_0xKn_WhtoKZbIjjs-4uHfrxgu3wVNlUA15fMOHd3nSL-P\\\" title=\\\"\\\"><font color=\\\"#0066cc\\\">here<\\/font><\\/a><font color=\\\"#000000\\\">.\\u00a0<\\/font><br \\/><br \\/><\\/font><\\/p>\\n<p><font size=\\\"2\\\"><font color=\\\"#000000\\\">References:\\n<\\/font><br \\/><font color=\\\"#0066cc\\\"><a href=\\\"https:\\/\\/environment.ec.europa.eu\\/topics\\/forests\\/deforestation\\/regulation-deforestation-free-products_en\\\">https:\\/\\/environment.ec.europa.eu\\/topics\\/forests\\/deforestation\\/regulation-deforestation-free-products_en<\\/a>\\n<br \\/><br \\/><a href=\\\"https:\\/\\/perfectdailygrind.com\\/2023\\/05\\/how-can-coffee-comply-with-new-eu-deforestation-regulations\\/\\\">https:\\/\\/perfectdailygrind.com\\/2023\\/05\\/how-can-coffee-comply-with-new-eu-deforestation-regulations\\/<\\/a>\\n<br \\/><br \\/><a href=\\\"https:\\/\\/www.forest-trends.org\\/wp-content\\/uploads\\/2021\\/02\\/10-things-to-know-about-coffee-production.pdf\\\">https:\\/\\/www.forest-trends.org\\/wp-content\\/uploads\\/2021\\/02\\/10-things-to-know-about-coffee-production.pdf<\\/a><\\/font><\\/font><\\/p>\",\"blog_image\":\"659f5769e74571704941417.jpg\"}','2024-01-11 02:50:17','2024-01-15 13:43:38');
/*!40000 ALTER TABLE `frontends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gateway_currencies`
--

# DROP TABLE IF EXISTS `gateway_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gateway_currencies` (
                                      `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                      `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `currency` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `symbol` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `method_code` int DEFAULT NULL,
                                      `gateway_alias` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `min_amount` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                      `max_amount` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                      `percent_charge` decimal(5,2) NOT NULL DEFAULT '0.00',
                                      `fixed_charge` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                      `rate` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                      `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      `gateway_parameter` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                      `created_at` timestamp NULL DEFAULT NULL,
                                      `updated_at` timestamp NULL DEFAULT NULL,
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gateway_currencies`
--

LOCK TABLES `gateway_currencies` WRITE;
/*!40000 ALTER TABLE `gateway_currencies` DISABLE KEYS */;
INSERT INTO `gateway_currencies` VALUES (1,'US International Wire','USD','',1000,'us_international_wire',100.00000000,1000000.00000000,5.00,0.00000000,1.00000000,'62b47f088e8961655996168.png','[]','2022-06-23 17:56:08','2022-06-23 17:56:08');
/*!40000 ALTER TABLE `gateway_currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gateways`
--

# DROP TABLE IF EXISTS `gateways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gateways` (
                            `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                            `code` int DEFAULT NULL,
                            `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `alias` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NULL',
                            `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=>enable, 2=>disable',
                            `gateway_parameters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `supported_currencies` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `crypto` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: fiat currency, 1: crypto currency',
                            `extra` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `input_form` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gateways`
--

LOCK TABLES `gateways` WRITE;
/*!40000 ALTER TABLE `gateways` DISABLE KEYS */;
/*!40000 ALTER TABLE `gateways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `general_settings`
--

# DROP TABLE IF EXISTS `general_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `general_settings` (
                                    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                    `sitename` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `cur_text` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'currency text',
                                    `cur_sym` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'currency symbol',
                                    `email_from` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `email_template` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                    `sms_api` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `base_color` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `mail_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'email configuration',
                                    `sms_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                    `merchant_profile` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                    `ev` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'email verification, 0 - dont check, 1 - check',
                                    `en` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'email notification, 0 - dont send, 1 - send',
                                    `sv` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'sms verication, 0 - dont check, 1 - check',
                                    `sn` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'sms notification, 0 - dont send, 1 - send',
                                    `force_ssl` tinyint(1) NOT NULL DEFAULT '0',
                                    `secure_password` tinyint(1) NOT NULL DEFAULT '0',
                                    `agree` tinyint(1) NOT NULL DEFAULT '0',
                                    `registration` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Off	, 1: On',
                                    `active_template` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `sys_version` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                    `created_at` timestamp NULL DEFAULT NULL,
                                    `updated_at` timestamp NULL DEFAULT NULL,
                                    `stop_live_in_not_auth_users` tinyint(1) NOT NULL DEFAULT '0',
                                    `stop_cart` tinyint(1) NOT NULL DEFAULT '0',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general_settings`
--

LOCK TABLES `general_settings` WRITE;
/*!40000 ALTER TABLE `general_settings` DISABLE KEYS */;
INSERT INTO `general_settings` VALUES (1,'M-Cultivo','USD','$','logistics@cupofexcellence.org','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" bgcolor=\"#414a51\" align=\"center\">\r\n    <tbody><tr>\r\n      <td height=\"50\"><br></td>\r\n    </tr>\r\n    <tr>\r\n      <td style=\"text-align:center;vertical-align:top;font-size:0;\" align=\"center\">\r\n        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\r\n          <tbody><tr>\r\n            <td width=\"600\" align=\"center\">\r\n              <!--header-->\r\n              <table class=\"table-inner\" width=\"95%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\r\n                <tbody><tr>\r\n                  <td style=\"border-top-left-radius:6px; border-top-right-radius:6px;text-align:center;vertical-align:top;font-size:0;\" bgcolor=\"#0087ff\" align=\"center\">\r\n                    <table width=\"90%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\r\n                      <tbody><tr>\r\n                        <td height=\"20\"><br></td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td style=\"font-family: \'Open sans\', Arial, sans-serif; color:#FFFFFF; font-size:16px; font-weight: bold;\" align=\"center\">This is a System Generated Email</td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td height=\"20\"><br></td>\r\n                      </tr>\r\n                    </tbody></table>\r\n                  </td>\r\n                </tr>\r\n              </tbody></table>\r\n              <!--end header-->\r\n              <table class=\"table-inner\" width=\"95%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                <tbody><tr>\r\n                  <td style=\"text-align:center;vertical-align:top;font-size:0;\" bgcolor=\"#FFFFFF\" align=\"center\">\r\n                    <table width=\"90%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\r\n                      <tbody><tr>\r\n                        <td height=\"35\"><br></td>\r\n                      </tr>\r\n                      <!--logo-->\r\n                      <tr>\r\n                        <td style=\"vertical-align:top;font-size:0;\" align=\"center\"></td>\r\n                      </tr>\r\n                      <!--end logo-->\r\n                      <tr>\r\n                        <td height=\"40\">=<br><br></td>\r\n                      </tr>\r\n                      <!--headline-->\r\n                      <tr>\r\n                        <td style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 22px; color: rgb(65, 74, 81);\" align=\"center\"><img src=\"https://i.imgur.com/FpodSYq.png\" width=\"410\"><br>Hello {{fullname}} ({{username}})</td>\r\n                      </tr>\r\n                      <!--end headline-->\r\n                      <tr>\r\n                        <td style=\"text-align:center;vertical-align:top;font-size:0;\" align=\"center\">\r\n                          <table width=\"40\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\r\n                            <tbody><tr>\r\n                              <td style=\" border-bottom:3px solid #0087ff;\" height=\"20\"><br></td>\r\n                            </tr>\r\n                          </tbody></table>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td height=\"20\"><br></td>\r\n                      </tr>\r\n                      <!--content-->\r\n                      <tr>\r\n                        <td style=\"font-family: \'Open sans\', Arial, sans-serif; color:#7f8c8d; font-size:16px; line-height: 28px;\" align=\"left\">{{message}}</td>\r\n                      </tr>\r\n                      <!--end content-->\r\n                      <tr>\r\n                        <td height=\"40\"><br></td>\r\n                      </tr>\r\n              \r\n                    </tbody></table>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td style=\"border-bottom-left-radius:6px;border-bottom-right-radius:6px;\" height=\"45\" bgcolor=\"#f4f4f4\" align=\"center\">\r\n                    <table width=\"90%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\r\n                      <tbody><tr>\r\n                        <td height=\"10\"><br></td>\r\n                      </tr>\r\n                      <!--preference-->\r\n                      <tr>\r\n                        <td class=\"preference-link\" style=\"font-family: \'Open sans\', Arial, sans-serif; color:#95a5a6; font-size:14px;\" align=\"center\">\r\n                          © 2022 <a href=\"#\">M-Cultivo Auctions</a>®. All Rights Reserved. \r\n                        </td>\r\n                      </tr>\r\n                      <!--end preference-->\r\n                      <tr>\r\n                        <td height=\"10\"><br></td>\r\n                      </tr>\r\n                    </tbody></table>\r\n                  </td>\r\n                </tr>\r\n              </tbody></table>\r\n            </td>\r\n          </tr>\r\n        </tbody></table>\r\n      </td>\r\n    </tr>\r\n    <tr>\r\n      <td height=\"60\"><br></td>\r\n    </tr>\r\n  </tbody></table>','hi {{name}}, {{message}}','#008e8f','{\"name\":\"smtp\",\"host\":\"smtp.gmail.com\",\"port\":\"465\",\"enc\":\"ssl\",\"username\":\"logistics@cupofexcellence.org\",\"password\":\"dryi gtho twkg mmte\"}','{\"clickatell_api_key\":\"----------------------------\",\"infobip_username\":\"--------------\",\"infobip_password\":\"----------------------\",\"message_bird_api_key\":\"-------------------\",\"account_sid\":\"AC67afdacf2dacff5f163134883db92c24\",\"auth_token\":\"77726b242830fb28f52fb08c648dd7a6\",\"from\":\"+17739011523\",\"apiv2_key\":\"dfsfgdfgh\",\"name\":\"clickatell\"}','{\"name\":\"Our Shop\",\"mobile\":\"88004054545\",\"address\":\"New Yoka\",\"image\":\"6224ad83e6f471646570883.jpg\",\"cover_image\":\"6224ad83f31601646570883.jpg\"}',0,1,0,0,0,1,1,1,'basic',NULL,NULL,'2024-02-02 16:58:55',0,1);
/*!40000 ALTER TABLE `general_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

# DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groups` (
                          `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                          `name` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `leader_id` int DEFAULT NULL,
                          `event_id` int NOT NULL,
                          `description` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `image` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
                          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                          `updated_at` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitations`
--

# DROP TABLE IF EXISTS `invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invitations` (
                               `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                               `user_id` int NOT NULL,
                               `group_id` int NOT NULL,
                               `status` int NOT NULL DEFAULT '-1',
                               `invitation_type` tinyint DEFAULT NULL,
                               `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                               `updated_at` timestamp NULL DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitations`
--

LOCK TABLES `invitations` WRITE;
/*!40000 ALTER TABLE `invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

# DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
                        `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                        `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                        `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                        `attempts` tinyint unsigned NOT NULL,
                        `reserved_at` int unsigned DEFAULT NULL,
                        `available_at` int unsigned NOT NULL,
                        `created_at` int unsigned NOT NULL,
                        PRIMARY KEY (`id`),
                        KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

# DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
                             `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                             `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                             `code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                             `text_align` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: left to right text align, 1: right to left text align',
                             `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: not default language, 1: default language',
                             `created_at` timestamp NULL DEFAULT NULL,
                             `updated_at` timestamp NULL DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English','en',0,1,'2022-03-28 23:14:35','2022-07-29 20:52:53');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lots`
--

# DROP TABLE IF EXISTS `lots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lots` (
                        `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                        `user_id` int NOT NULL,
                        `event_id` int NOT NULL,
                        `product_id` int NOT NULL,
                        `group_id` int NOT NULL,
                        `lot` int NOT NULL,
                        `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        `updated_at` timestamp NULL DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lots`
--

LOCK TABLES `lots` WRITE;
/*!40000 ALTER TABLE `lots` DISABLE KEYS */;
/*!40000 ALTER TABLE `lots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `merchant_password_resets`
--

# DROP TABLE IF EXISTS `merchant_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `merchant_password_resets` (
                                            `email` varchar(40) DEFAULT NULL,
                                            `token` varchar(40) DEFAULT NULL,
                                            `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merchant_password_resets`
--

LOCK TABLES `merchant_password_resets` WRITE;
/*!40000 ALTER TABLE `merchant_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `merchant_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `merchants`
--

# DROP TABLE IF EXISTS `merchants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `merchants` (
                             `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                             `firstname` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `lastname` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                             `mobile` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `email_verified_at` timestamp NULL DEFAULT NULL,
                             `balance` decimal(28,8) unsigned NOT NULL DEFAULT '0.00000000',
                             `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                             `country_code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `cover_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `social_links` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                             `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                             `avg_rating` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
                             `total_rating` int unsigned NOT NULL DEFAULT '0',
                             `review_count` int unsigned NOT NULL DEFAULT '0',
                             `status` tinyint NOT NULL DEFAULT '1',
                             `ev` tinyint NOT NULL DEFAULT '0',
                             `sv` tinyint NOT NULL DEFAULT '0',
                             `ver_code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `ver_code_send_at` datetime NOT NULL,
                             `ts` tinyint NOT NULL DEFAULT '0',
                             `tv` tinyint NOT NULL DEFAULT '1',
                             `tsc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `created_at` timestamp NULL DEFAULT NULL,
                             `updated_at` timestamp NULL DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=815 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merchants`
--

LOCK TABLES `merchants` WRITE;
/*!40000 ALTER TABLE `merchants` DISABLE KEYS */;
/*!40000 ALTER TABLE `merchants` ENABLE KEYS */;
UNLOCK TABLES;

# DROP TABLE IF EXISTS `migrations`;
# LOCK TABLES `migrations` WRITE;
# /*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
# INSERT INTO `migrations` VALUES (1,'2019_12_14_000001_create_personal_access_tokens_table',1),(2,'2022_06_13_205705_permission-group',1),(3,'2022_06_13_205852_user-permission-eve-prod',1),(4,'2022_06_13_213118_user-permission-group',1),(5,'2022_06_15_221043_product-permission-group',1),(6,'2022_06_16_195857_product-permission-user',1),(7,'2022_06_17_140122_event-permission-group',1),(8,'2022_06_17_140336_event-permission-user',1),(9,'2022_06_19_191837_shipping__region',2),(10,'2022_06_19_192256_shipping_ranges',2),(11,'2022_06_25_111229_create_payments_table',3),(12,'2022_06_26_183736_shipping_region_country',4),(13,'2021_03_15_084721_create_admin_notifications_table',5),(14,'2021_05_08_103925_create_sms_gateways_table',5),(15,'2021_05_23_111859_create_email_logs_table',5),(16,'2022_02_15_144408_create_products_table',5),(17,'2022_02_15_153018_create_categories_table',5),(18,'2022_02_17_161112_create_merchants_table',5),(19,'2022_02_19_113234_create_bids_table',5),(20,'2022_02_19_140759_create_reviews_table',5),(21,'2022_02_20_182134_create_winners_table',5),(22,'2022_02_24_145314_create_advertisements_table',5),(23,'2022_04_28_072752_create_events_table',6),(24,'2022_05_06_143757_create_auto_bid_settings_table',7),(25,'2022_06_15_211113_create_user_requests_table',8),(26,'2022_06_15_211523_create_user_events_table',9),(27,'2022_06_17_180147_create_groups_table',10),(28,'2022_06_18_070845_create_invitations_table',11),(29,'2022_06_21_131323_create_lots_table',12),(30,'2022_06_30_205721_product-specification',13),(31,'2022_08_01_132841_add_postion_colum_to_categories_table',14),(32,'2022_08_01_170836_create_bid_histories_table',15),(33,'2022_08_07_190111_add_colums_to_products_table',16),(34,'2022_08_07_194705_add_stop_live_in_not_auth_users_to_general_settings_table',16),(35,'2022_09_01_171231_create_jobs_table',17),(36,'2022_10_26_140642_insert_hotjar_extention_row',17),(37,'2022_09_22_154140_add_cc_to_email_sms_templates_table',18),(38,'2022_09_22_215354_create_user_product_fav_table',18),(39,'2022_09_27_114804_add_description_field_to_categories_table',18),(40,'2022_09_27_121119_add_practice_field_to_events_table',18),(41,'2022_09_28_155946_add_sname_field_to_events_table',18),(42,'2022_09_29_185833_add_user_bid_count_sql',18),(43,'2022_11_02_085412_add_triggers',18),(44,'2022_12_05_161337_create_fees_table',19),(45,'2022_12_23_003507_add_cart_stop_to_general_settings_table',19),(46,'2023_03_10_205204_create_scores_table',19),(47,'2023_03_13_190113_add_row_to_pages_table',19),(48,'2023_03_14_140555_alter_merchants_table',19),(49,'2023_03_15_122837_create_exchange_rates_table',19),(50,'2022_10_31_200643_create_bids_history_all_data_table',18),(51,'2023_03_20_133132_create_budgets_table',20),(52,'2023_03_22_200902_add_hyperlink_short_code',20),(53,'2023_04_02_150423_add_column_to_users_table',20),(54,'2023_04_11_144755_add_payment_method_to_fees_table',20),(55,'2023_04_19_082457_insert_exchange_rate_extention_row',21),(56,'2023_04_20_120730_add_logo_to_events',21),(57,'2023_05_28_064222_add_opt_in_upcoming_events_date_to_users_table',22);
# /*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
# UNLOCK TABLES;

--
-- Table structure for table `orders`
--

# DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
                          `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                          `user_id` bigint unsigned NOT NULL,
                          `product_ids` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `shipping_method` bigint DEFAULT NULL,
                          `payment_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `payment_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `total_price` double(8,2) NOT NULL,
                          `shipping_price` double(8,2) NOT NULL,
                          `currency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `created_at` timestamp NULL DEFAULT NULL,
                          `updated_at` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          KEY `orders_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

# DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
                         `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                         `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `tempname` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'template name',
                         `secs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                         `is_default` tinyint(1) NOT NULL DEFAULT '0',
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

# DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
                                   `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `token` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

# DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
                            `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                            `user_id` bigint unsigned NOT NULL,
                            `payment_type` enum('hold','payment') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `charge_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `amount` double NOT NULL,
                            `currency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `event_id` int DEFAULT NULL,
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            KEY `payments_user_id_index` (`user_id`),
                            KEY `payments_event_id_index` (`event_id`),
                            CONSTRAINT `payments_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
                            CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_group`
--

# DROP TABLE IF EXISTS `permission_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_group` (
                                    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                    `name_group` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                    `status` tinyint NOT NULL DEFAULT '1',
                                    `created_at` timestamp NULL DEFAULT NULL,
                                    `updated_at` timestamp NULL DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_group`
--

LOCK TABLES `permission_group` WRITE;
/*!40000 ALTER TABLE `permission_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
                                          `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                          `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `tokenable_id` bigint unsigned NOT NULL,
                                          `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                          `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                          `last_used_at` timestamp NULL DEFAULT NULL,
                                          `created_at` timestamp NULL DEFAULT NULL,
                                          `updated_at` timestamp NULL DEFAULT NULL,
                                          PRIMARY KEY (`id`),
                                          UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
                                          KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_permission_group`
--

# DROP TABLE IF EXISTS `product_permission_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_permission_group` (
                                            `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                            `group_id` bigint unsigned NOT NULL,
                                            `product_id` bigint unsigned NOT NULL,
                                            `created_at` timestamp NULL DEFAULT NULL,
                                            `updated_at` timestamp NULL DEFAULT NULL,
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_permission_group`
--

LOCK TABLES `product_permission_group` WRITE;
/*!40000 ALTER TABLE `product_permission_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_permission_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_permission_user`
--

# DROP TABLE IF EXISTS `product_permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_permission_user` (
                                           `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                           `permission_id` bigint unsigned NOT NULL,
                                           `product_id` bigint unsigned NOT NULL,
                                           `created_at` timestamp NULL DEFAULT NULL,
                                           `updated_at` timestamp NULL DEFAULT NULL,
                                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_permission_user`
--

LOCK TABLES `product_permission_user` WRITE;
/*!40000 ALTER TABLE `product_permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_specification`
--

# DROP TABLE IF EXISTS `product_specification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_specification` (
                                         `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                         `product_id` bigint unsigned NOT NULL,
                                         `spec_key` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `Value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `is_display` int NOT NULL DEFAULT '1',
                                         `created_at` timestamp NULL DEFAULT NULL,
                                         `updated_at` timestamp NULL DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48319 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_specification`
--

LOCK TABLES `product_specification` WRITE;
/*!40000 ALTER TABLE `product_specification` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_specification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

# DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
                            `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                            `event_id` int unsigned NOT NULL,
                            `merchant_id` int unsigned NOT NULL DEFAULT '0',
                            `admin_id` int unsigned NOT NULL DEFAULT '0',
                            `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `price` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                            `max_auto_bid_price` decimal(10,2) DEFAULT NULL,
                            `max_auto_bid_steps` int unsigned DEFAULT NULL,
                            `total_bid` int unsigned NOT NULL DEFAULT '0',
                            `started_at` datetime DEFAULT NULL,
                            `expired_at` datetime DEFAULT NULL,
                            `avg_rating` decimal(5,2) NOT NULL DEFAULT '0.00',
                            `total_rating` int unsigned NOT NULL DEFAULT '0',
                            `review_count` int unsigned NOT NULL DEFAULT '0',
                            `image` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `short_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `long_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `specification` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Pending, 1: Live',
                            `less_bidding_value` decimal(20,2) NOT NULL DEFAULT '0.00',
                            `FirstBidDone` tinyint DEFAULT '0',
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3795 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

# DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
                           `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                           `rating` int unsigned NOT NULL DEFAULT '0',
                           `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                           `user_id` int unsigned NOT NULL DEFAULT '0',
                           `product_id` int unsigned NOT NULL DEFAULT '0',
                           `merchant_id` int unsigned NOT NULL DEFAULT '0',
                           `created_at` timestamp NULL DEFAULT NULL,
                           `updated_at` timestamp NULL DEFAULT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scores`
--

# DROP TABLE IF EXISTS `scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scores` (
                          `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                          `user_id` bigint unsigned NOT NULL,
                          `product_id` bigint unsigned NOT NULL,
                          `score` double(8,2) unsigned NOT NULL,
                          `created_at` timestamp NULL DEFAULT NULL,
                          `updated_at` timestamp NULL DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `scores_user_id_product_id_unique` (`user_id`,`product_id`),
                          KEY `scores_user_id_index` (`user_id`),
                          KEY `scores_product_id_index` (`product_id`),
                          CONSTRAINT `scores_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
                          CONSTRAINT `scores_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scores`
--

LOCK TABLES `scores` WRITE;
/*!40000 ALTER TABLE `scores` DISABLE KEYS */;
/*!40000 ALTER TABLE `scores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

# DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
                            `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `user_id` bigint unsigned DEFAULT NULL,
                            `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                            `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                            `last_activity` int NOT NULL,
                            PRIMARY KEY (`id`),
                            KEY `sessions_user_id_index` (`user_id`),
                            KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_ranges`
--

# DROP TABLE IF EXISTS `shipping_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipping_ranges` (
                                   `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                   `region_id` bigint unsigned NOT NULL,
                                   `from` decimal(8,2) NOT NULL DEFAULT '0.00',
                                   `up_to` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                                   `cost` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
                                   `created_at` timestamp NULL DEFAULT NULL,
                                   `updated_at` timestamp NULL DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1646 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_ranges`
--

LOCK TABLES `shipping_ranges` WRITE;
/*!40000 ALTER TABLE `shipping_ranges` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_ranges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_region`
--

# DROP TABLE IF EXISTS `shipping_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipping_region` (
                                   `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                   `event_id` bigint unsigned NOT NULL,
                                   `shipping_method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'expedited',
                                   `region_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `created_at` timestamp NULL DEFAULT NULL,
                                   `updated_at` timestamp NULL DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1602 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_region`
--

LOCK TABLES `shipping_region` WRITE;
/*!40000 ALTER TABLE `shipping_region` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_region_country`
--

# DROP TABLE IF EXISTS `shipping_region_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipping_region_country` (
                                           `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                           `region_id` int NOT NULL,
                                           `country` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                           `created_at` timestamp NULL DEFAULT NULL,
                                           `updated_at` timestamp NULL DEFAULT NULL,
                                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_region_country`
--

LOCK TABLES `shipping_region_country` WRITE;
/*!40000 ALTER TABLE `shipping_region_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_region_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_gateways`
--

# DROP TABLE IF EXISTS `sms_gateways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sms_gateways` (
                                `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                `name` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `alias` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `credentials` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                `created_at` timestamp NULL DEFAULT NULL,
                                `updated_at` timestamp NULL DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_gateways`
--

LOCK TABLES `sms_gateways` WRITE;
/*!40000 ALTER TABLE `sms_gateways` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_gateways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support_attachments`
--

# DROP TABLE IF EXISTS `support_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `support_attachments` (
                                       `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                       `support_message_id` int unsigned NOT NULL,
                                       `attachment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `created_at` timestamp NULL DEFAULT NULL,
                                       `updated_at` timestamp NULL DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support_attachments`
--

LOCK TABLES `support_attachments` WRITE;
/*!40000 ALTER TABLE `support_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support_messages`
--

# DROP TABLE IF EXISTS `support_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `support_messages` (
                                    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                    `supportticket_id` int unsigned NOT NULL DEFAULT '0',
                                    `admin_id` int unsigned NOT NULL DEFAULT '0',
                                    `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `created_at` timestamp NULL DEFAULT NULL,
                                    `updated_at` timestamp NULL DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support_messages`
--

LOCK TABLES `support_messages` WRITE;
/*!40000 ALTER TABLE `support_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support_tickets`
--

# DROP TABLE IF EXISTS `support_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `support_tickets` (
                                   `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                   `user_id` int DEFAULT '0',
                                   `merchant_id` int unsigned NOT NULL DEFAULT '0',
                                   `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `ticket` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   `status` tinyint(1) NOT NULL COMMENT '0: Open, 1: Answered, 2: Replied, 3: Closed',
                                   `priority` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Low, 2 = medium, 3 = heigh',
                                   `last_reply` datetime DEFAULT NULL,
                                   `created_at` timestamp NULL DEFAULT NULL,
                                   `updated_at` timestamp NULL DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support_tickets`
--

LOCK TABLES `support_tickets` WRITE;
/*!40000 ALTER TABLE `support_tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

# DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions` (
                                `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                `user_id` int unsigned NOT NULL DEFAULT '0',
                                `merchant_id` int unsigned DEFAULT '0',
                                `amount` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                `charge` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                `post_balance` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                `trx_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `trx` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `details` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                `created_at` timestamp NULL DEFAULT NULL,
                                `updated_at` timestamp NULL DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_events`
--

# DROP TABLE IF EXISTS `user_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_events` (
                               `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                               `user_id` int NOT NULL,
                               `event_id` int NOT NULL,
                               `product_id` int NOT NULL,
                               `created_at` timestamp NULL DEFAULT NULL,
                               `updated_at` timestamp NULL DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68513 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_events`
--

LOCK TABLES `user_events` WRITE;
/*!40000 ALTER TABLE `user_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_logins`
--

# DROP TABLE IF EXISTS `user_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_logins` (
                               `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                               `user_id` int unsigned NOT NULL DEFAULT '0',
                               `merchant_id` int unsigned NOT NULL DEFAULT '0',
                               `user_ip` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `city` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `country` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `country_code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `longitude` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `latitude` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `browser` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `os` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `created_at` timestamp NULL DEFAULT NULL,
                               `updated_at` timestamp NULL DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10229 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_logins`
--

LOCK TABLES `user_logins` WRITE;
/*!40000 ALTER TABLE `user_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permission_eve_prod`
--

# DROP TABLE IF EXISTS `user_permission_eve_prod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_permission_eve_prod` (
                                            `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                            `user_id` int unsigned NOT NULL,
                                            `status` tinyint NOT NULL DEFAULT '1',
                                            `created_at` timestamp NULL DEFAULT NULL,
                                            `updated_at` timestamp NULL DEFAULT NULL,
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permission_eve_prod`
--

LOCK TABLES `user_permission_eve_prod` WRITE;
/*!40000 ALTER TABLE `user_permission_eve_prod` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_permission_eve_prod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permission_group`
--

# DROP TABLE IF EXISTS `user_permission_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_permission_group` (
                                         `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                         `group_id` bigint unsigned NOT NULL,
                                         `user_id` bigint unsigned NOT NULL,
                                         `created_at` timestamp NULL DEFAULT NULL,
                                         `updated_at` timestamp NULL DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permission_group`
--

LOCK TABLES `user_permission_group` WRITE;
/*!40000 ALTER TABLE `user_permission_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_permission_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_product_favs`
--

# DROP TABLE IF EXISTS `user_product_favs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_product_favs` (
                                     `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                     `user_id` bigint unsigned NOT NULL,
                                     `product_id` bigint unsigned NOT NULL,
                                     `created_at` timestamp NULL DEFAULT NULL,
                                     `updated_at` timestamp NULL DEFAULT NULL,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `user_product_favs_user_id_product_id_unique` (`user_id`,`product_id`),
                                     KEY `user_product_favs_product_id_foreign` (`product_id`),
                                     CONSTRAINT `user_product_favs_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
                                     CONSTRAINT `user_product_favs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5084 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_product_favs`
--

LOCK TABLES `user_product_favs` WRITE;
/*!40000 ALTER TABLE `user_product_favs` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_product_favs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_requests`
--

# DROP TABLE IF EXISTS `user_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_requests` (
                                 `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                 `user_id` int unsigned NOT NULL DEFAULT '0',
                                 `event_id` int unsigned NOT NULL DEFAULT '0',
                                 `status` int NOT NULL DEFAULT '-1',
                                 `created_at` timestamp NULL DEFAULT NULL,
                                 `updated_at` timestamp NULL DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3319 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_requests`
--

LOCK TABLES `user_requests` WRITE;
/*!40000 ALTER TABLE `user_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

# DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
                         `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                         `firstname` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `lastname` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                         `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                         `country_code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `mobile` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `balance` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                         `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                         `create_password` tinyint(1) DEFAULT '0',
                         `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'contains full address',
                         `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: banned, 1: active',
                         `ev` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: email unverified, 1: email verified',
                         `sv` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: sms unverified, 1: sms verified',
                         `ver_code` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stores verification code',
                         `ver_code_send_at` datetime DEFAULT NULL COMMENT 'verification send time',
                         `ts` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 2fa off, 1: 2fa on',
                         `tv` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: 2fa unverified, 1: 2fa verified',
                         `tsc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `ace_member` tinyint DEFAULT '0',
                         `remember_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         `shipping_first_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_last_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_company` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_address_1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                         `shipping_address_2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                         `shipping_city` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_state` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_postcode` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_country` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_phone` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_first_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_last_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_company` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_address_1` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_address_2` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_city` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_state` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_postcode` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_country` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_phone` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `company_website` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `describes_business` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `pounds_green_coffee` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `hosting_cupping` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_company_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `ein_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `delivery` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `preferred_receiving_day` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `preferred_receiving_times` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `other_special_delivery` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                         `exclusive_offers` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `shipping_zip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `billing_zip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `company_name` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `opt_in` tinyint DEFAULT '0',
                         `i_contacted_upcoming_auctions` tinyint(1) NOT NULL DEFAULT '0',
                         `opt_in_upcoming_events_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `username` (`username`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=924 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `winners`
--

# DROP TABLE IF EXISTS `winners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `winners` (
                           `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                           `user_id` int unsigned NOT NULL DEFAULT '0',
                           `product_id` int unsigned NOT NULL DEFAULT '0',
                           `bid_id` int unsigned NOT NULL DEFAULT '0',
                           `product_delivered` tinyint unsigned DEFAULT '0' COMMENT '0:Pending, 1:Send',
                           `status` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
                           `order_id` int unsigned DEFAULT NULL,
                           `created_at` timestamp NULL DEFAULT NULL,
                           `updated_at` timestamp NULL DEFAULT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1592 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `winners`
--

LOCK TABLES `winners` WRITE;
/*!40000 ALTER TABLE `winners` DISABLE KEYS */;
/*!40000 ALTER TABLE `winners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `withdraw_methods`
--

# DROP TABLE IF EXISTS `withdraw_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `withdraw_methods` (
                                    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                                    `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `min_limit` decimal(28,8) DEFAULT '0.00000000',
                                    `max_limit` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                                    `delay` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `fixed_charge` decimal(28,8) DEFAULT '0.00000000',
                                    `rate` decimal(28,8) DEFAULT '0.00000000',
                                    `percent_charge` decimal(5,2) DEFAULT NULL,
                                    `currency` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                    `user_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                    `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                                    `status` tinyint(1) NOT NULL DEFAULT '1',
                                    `created_at` timestamp NULL DEFAULT NULL,
                                    `updated_at` timestamp NULL DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdraw_methods`
--

LOCK TABLES `withdraw_methods` WRITE;
/*!40000 ALTER TABLE `withdraw_methods` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdraw_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `withdrawals`
--

# DROP TABLE IF EXISTS `withdrawals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `withdrawals` (
                               `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                               `method_id` int unsigned NOT NULL,
                               `user_id` int unsigned NOT NULL,
                               `merchant_id` int unsigned NOT NULL DEFAULT '0',
                               `amount` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                               `currency` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                               `rate` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                               `charge` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                               `trx` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                               `final_amount` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                               `after_charge` decimal(28,8) NOT NULL DEFAULT '0.00000000',
                               `withdraw_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                               `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=>success, 2=>pending, 3=>cancel,  ',
                               `admin_feedback` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
                               `created_at` timestamp NULL DEFAULT NULL,
                               `updated_at` timestamp NULL DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdrawals`
--

LOCK TABLES `withdrawals` WRITE;
/*!40000 ALTER TABLE `withdrawals` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdrawals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `bids_history_all_data`
--

/*!50001 DROP VIEW IF EXISTS `bids_history_all_data`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
    /*!50001 VIEW `bids_history_all_data` AS select `His`.`id` AS `ID`,`His`.`user_id` AS `user_id`,`His`.`product_id` AS `product_id`,`products`.`event_id` AS `event_id`,`U`.`firstname` AS `firstname`,`U`.`lastname` AS `lastname`,`U`.`company_name` AS `company_name`,`U`.`billing_country` AS `billing_country`,`U`.`billing_state` AS `billing_state`,`U`.`billing_city` AS `billing_city`,`U`.`billing_address_1` AS `billing_address_1`,`events`.`name` AS `event_name`,`products`.`name` AS `product_name`,`His`.`new_bid` AS `new_bid`,`His`.`previous_bid` AS `previous_bid`,`His`.`user_previous_bid` AS `user_previous_bid`,`His`.`created_at` AS `created_at`,`His`.`updated_at` AS `updated_at`,`Spec`.`Value` AS `RANK`,`countries`.`Name` AS `Name` from (((((`bids_history` `His` left join `products` on((`His`.`product_id` = `products`.`id`))) left join `events` on((`products`.`event_id` = `events`.`id`))) left join `users` `U` on((`His`.`user_id` = `U`.`id`))) left join `countries` on((`U`.`billing_country` = `countries`.`id`))) left join `product_specification` `Spec` on(((`products`.`id` = `Spec`.`product_id`) and (upper(`Spec`.`spec_key`) = 'RANK')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-22 14:49:23
