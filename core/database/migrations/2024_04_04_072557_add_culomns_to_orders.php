<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCulomnsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('total_price_sample', 10, 2)->nullable()->default(0);
            $table->decimal('total_price_product', 10, 2)->nullable()->default(0);
            $table->decimal('shipping_price_sample', 10, 2)->nullable()->default(0);
            $table->decimal('shipping_price_product', 10, 2)->nullable()->default(0);
            $table->decimal('paid_amount', 10, 2)->nullable()->default(0);
            $table->string('order_type')->nullable(); //['sample_payment', 'email_only_order', 'full_order ']
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('total_price_sample');
            $table->dropColumn('total_price_product');
            $table->dropColumn('shipping_price_sample');
            $table->dropColumn('shipping_price_product');
            $table->dropColumn('paid_amount');
            $table->dropColumn('order_type');
        });
    }
}
