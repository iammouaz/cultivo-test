<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSampleSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample_sets', function (Blueprint $table) {
            $table->id();
            // event_id is a foreign key that references the id column on the events table
            $table->integer('event_id')->references('id')->on('events');
            $table->string('price');
            $table->string('total_package_weight_Lb');
            $table->string('number_of_samples_per_box');
            $table->string('weight_per_sample_grams');
            $table->string('image');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sample_sets');
    }
}
