<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailAndButtonsToOfferSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_sheets', function (Blueprint $table) {
            $table->string('emails')->nullable()->after('offer_sheet_url');
            $table->boolean('show_add_sample_button')->default(1)->after('emails');
            $table->boolean('show_add_order_button')->default(1)->after('show_add_sample_button');
            $table->boolean('show_make_offer_button')->default(1)->after('show_add_order_button');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_sheets', function (Blueprint $table) {
            $table->dropColumn('emails');
            $table->dropColumn('show_add_sample_button');
            $table->dropColumn('show_add_order_button');
            $table->dropColumn('show_make_offer_button');
        });
    }
}
