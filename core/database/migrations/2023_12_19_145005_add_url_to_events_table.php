<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrlToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_sheets', function (Blueprint $table) {
            $table->id();
            $table->string('status')->default('active')->comment('active, ended');
            $table->string('name');
            $table->string('sname')->nullable();
            $table->text('description');
            $table->string('image');
            $table->decimal('deposit', 28)->nullable();
            $table->integer('category_id')->nullable();
            $table->timestamps();
            $table->string('offer_sheet_url')->nullable();
        });
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('offer_sheet_id')->references('id')->on('offer_sheets');
            $table->foreignId('merchant_id')->references('id')->on('merchants');
            $table->foreignId('admin_id')->references('id')->on('admins');
            $table->string('name')->nullable();
            $table->string('color_class')->nullable();
            $table->dateTime('started_at')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->decimal('avg_rating', 5)->default(0);
            $table->unsignedInteger('total_rating')->default(0);
            $table->unsignedInteger('review_count')->default(0);
            $table->string('image', 40)->nullable();
            $table->text('short_description')->nullable();
            $table->text('long_description')->nullable();
            $table->boolean('status')->default(false)->comment('0: Pending, 1: Live');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
        Schema::dropIfExists('offer_sheets');
    }
}
