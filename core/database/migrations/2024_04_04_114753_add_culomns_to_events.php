<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCulomnsToEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->boolean('show_sample_set_button')->default(0);
            $table->string('sample_set_button_lable')->nullable();
            $table->string('sample_set_external_url')->nullable();
            $table->string('sample_set_cart_config')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn(['show_sample_set_button', 'sample_set_button_lable', 'sample_set_external_url', 'sample_set_cart_config']);
        });
    }
}
