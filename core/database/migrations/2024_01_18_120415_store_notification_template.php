<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StoreNotificationTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_sms_templates')->insert(
            array(
                'act'=>'Request_Access_Notification',
                'name'=>'Request Access Notification',
                'subj'=>'Request Access Notification',
                'email_body'=>'<div><br></div><div>the user {{user_name}} requests access to the event {{event_name}}</div>',
                'sms_body'=>null,
                'cc'=>null,
                'shortcodes'=>'{"event_name":"Event Name","user_name":"User Name"}',
                'email_status'=>1,
                'sms_status'=>1,
            )
        );

        DB::table('email_sms_templates')->insert(
            array(
                'act'=>'Clock_Notification',
                'name'=>'Clock Notification',
                'subj'=>'Clock Notification',
                'email_body'=>'<div><br></div><div>the clock for the event {{event_name}} has run out</div>',
                'sms_body'=>null,
                'cc'=>null,
                'shortcodes'=>'{"event_name":"Event Name"}',
                'email_status'=>1,
                'sms_status'=>1,
                
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
