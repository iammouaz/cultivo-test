<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveOfferUnusedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers',function (Blueprint $table) {
            $table->dropColumn('color_class');
            $table->dropColumn('started_at');
            $table->dropColumn('expired_at');
            $table->boolean('status')->default(1)->comment('0: Pending, 1: Live')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers',function (Blueprint $table) {
            $table->string('color_class',100)->nullable();
            $table->dateTime('started_at')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->boolean('status')->default(0)->comment('0: Pending, 1: Live')->change();
        });
    }
}
