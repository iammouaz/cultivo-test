<?php

use App\Models\Event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FillEventUrlToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
{
    Schema::table('events', function (Blueprint $table) {
        $events = Event::whereNull('event_url')->get();
        
        foreach ($events as $event) {
            $event->event_url = $event->id;
            $event->save();
        }
    });
}


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $events = \App\Models\Event::all();
            foreach ($events as $event) {
                if ($event->event_url === $event->id) {
                    $event->update([
                        'event_url' => null,
                    ]);
                    $event->save();
                }
                
            }
        });
    }
}
