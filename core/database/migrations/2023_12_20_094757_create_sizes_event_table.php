<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSizesEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sizes', function (Blueprint $table) {
            $table->id(); // Auto-incremental primary key
            $table->string('size');
            $table->decimal('weight_LB', 8, 2);
            // offer_sheet_id column for foreign key constraint
            $table->foreignId('offer_sheet_id')->references('id')->on('offer_sheets');

            $table->timestamps(); // Created_at and updated_at columns for tracking changes
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sizes');
    }
}
