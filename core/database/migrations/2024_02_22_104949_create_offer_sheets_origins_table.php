<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferSheetsOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_sheets_origins', function (Blueprint $table) {
            $table->id();
            $table->foreignId('offer_sheet_id')->references('id')->on('offer_sheets');
            $table->foreignId('origin_id')->references('id')->on('origins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_sheets_origins');
    }
}
