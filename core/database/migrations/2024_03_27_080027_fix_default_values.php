<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('event_type')->default(null)->change();
        });
        Schema::table('fees', function (Blueprint $table) {
            $table->string('event_type')->default(null)->change();
        });
        Schema::table('shipping_region', function (Blueprint $table) {
            $table->string('event_type')->default(null)->change();
        });
        \App\Models\Payment::query()->where('event_type','AppModelsEvent')->update(['event_type' => \App\Models\Event::class]);
        \App\Models\Fee::query()->where('event_type','AppModelsEvent')->update(['event_type' => \App\Models\Event::class]);
        \App\Models\ShippingRegion::query()->where('event_type','AppModelsEvent')->update(['event_type' => \App\Models\Event::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //irreversiblle migration
    }
}
