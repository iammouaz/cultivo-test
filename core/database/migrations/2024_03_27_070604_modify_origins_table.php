<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ModifyOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('origins', function (Blueprint $table) {
            $table->integer('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->boolean('is_multiple_origins')->default(0);
            });
        if (DB::table('origins')->where('name', 'Multiple Origins')->doesntExist()) {
            DB::table('origins')->insert([
                'name' => 'Multiple Origins',
                'is_multiple_origins' => 1
            ]);
            $countries = \App\Models\Country::query()->select(['id', 'name'])->get()->toArray();
            $values = [];
            foreach ($countries as $value) {
                $values[] = [
                    'name' => $value['name'],
                    'country_id' => $value['id']
                ];
            }
            DB::table('origins')->insert($values);

//            $var=DB::raw('insert  origins (name, country_id, is_multiple_origins) values ("Multiple Origins", null, 1)');
//            DB::insert($var);
//            $var=DB::raw('insert  origins (country_id,name) select (id,name) from countries');
//            DB::insert($var);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('origins', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropColumn('country_id');
            $table->dropColumn('is_multiple_origins');
        });


    }
}
