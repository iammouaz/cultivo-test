<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCulomnsHeroSectionToEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->boolean('hero_show_action_name')->default(0);
            $table->string('hero_text_color')->nullable();
            $table->string('hero_primary_button_color')->nullable();
            $table->string('hero_image_overlay')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn(['hero_show_action_name', 'hero_text_color', 'hero_primary_button_color', 'hero_image_overlay']);
        });
    }
}
