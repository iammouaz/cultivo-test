<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateAuctionOrderEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::table('email_sms_templates')->insert(
            array(
                'act'=>'Auction_Order_Confirmation',
                'name'=>'Auction Order Confirmation',
                'subj'=>'Congratulations to our winning bidders!',
                'email_body'=>'<div><br></div><div>Congratulations to Our Winning Bidders</div><br><div>Congratulations on being one of our successful bidders at {{event_name}}.Find Payment instructions below:</div><br><div>Name:xxxx<br>ACH Wire routing number:xxxx<br>Account Number:xxxx<br>Account Type:xxxx<br>Bank Address:xxxx</div><br><div>Order Number:{{order_number}}<br>Event Name:{{event_name}}<br>Event Type:{{event_type}}<br>Order Type:{{order_type}}<br>Date of order:{{order_date}}<br>Product:{{product}}<br>Weight:{{weight}}<br>Quntity:{{quantity}}<br>Price:{{price}}:{{currency}}<br></div><br><div>Customer Name:{{customer_name}}<br>Company Name:{{company_name}}<br>Payment Method:{{payment_method}}<br>Payment Status:{{payment_status}}<br></div><br><div>Subtotal(Total Price without any shipping or fees):{{subtotal}}<br>Shipping Total:{{shipping_value}}<br>Grand Total:{{grand_total}}<br></div><br><div>Shipping and Billing information</div><br><div>Shipping & Delivery Details<br>{{shipping_details}}</div><br><div>Billing Details<br>{{billing_details}}</div><br>',
                'sms_body'=>null,
                'cc'=>null,
                'shortcodes'=>'{"order_number":"Number of the order ceated","event_name":"Name of the event","event_type":"Auction/Open Offer/Fixed Price Offer","order_type":"cart with payment processing for samples/Cart with order Emails for all products","order_date":"The date the order was created and sent","product":"Product Purchased","weight":"Product Weight (per unit)","quantity":"Quantity of the product sold","price":"Product\'s price","currency":"Currency","customer_name":"Customer Name","company_name":"Customer\'s Company Name","payment_method":"Credit Card/Bank Transfer/Email Order","payment_status":"Paid/Pending Payment","subtotal":"Total Price without any shipping of fees","shipping_value":"Shipping & Handling Value","grand_total":"Total amount to be paid:Subtotal + all fees (shipping etc)","shipping_details":"Shipping & Delivery Details added of payment checkout","billing_details":"Billing Details added at payment checkout"}',
                'email_status'=>1,
                'sms_status'=>1,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
