<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            // add price_id after product_ids
            $table->integer('price_id')->after('product_ids')->nullable();
            $table->string('customer_first_name')->after('user_id')->nullable();
            $table->string('customer_last_name')->after('customer_first_name')->nullable();
            $table->string('customer_email')->after('customer_last_name')->nullable();
            $table->string('customer_phone')->after('customer_email')->nullable();
            $table->string('customer_company_name')->after('customer_email')->nullable();
            $table->string('customer_company_website')->after('customer_company_name')->nullable();
            $table->boolean('shipping_is_business')->nullable();
            $table->string('shipping_first_name')->nullable();
            $table->string('shipping_last_name')->nullable();
            $table->string('shipping_address1')->nullable();
            $table->string('shipping_address2')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_state')->nullable();
            $table->string('shipping_zip')->nullable();
            $table->string('shipping_country')->nullable();
            $table->string('shipping_phone')->nullable();
            $table->string('shipping_EIN_number')->nullable();
            // add delivery date after shipping address
            $table->boolean('is_lift_gate_delivery')->nullable();
            $table->boolean('is_inside_delivery')->nullable();
            $table->boolean('is_appointment_request')->nullable();
            $table->boolean('is_notify_request')->nullable();
            $table->json('delivery_date')->nullable();
            $table->string('special_delivery_instruction')->nullable();
            // add billing info after shipping address
            $table->string('billing_first_name')->nullable();
            $table->string('billing_last_name')->nullable();
            $table->string('billing_address1')->nullable();
            $table->string('billing_address2')->nullable();
            $table->string('billing_city')->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_zip')->nullable();
            $table->string('billing_country')->nullable();
            $table->string('billing_phone')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {

            $table->dropColumn('price_id');
            $table->dropColumn('customer_first_name');
            $table->dropColumn('customer_last_name');
            $table->dropColumn('customer_email');
            $table->dropColumn('customer_phone');
            $table->dropColumn('customer_company_name');
            $table->dropColumn('customer_company_website');
            $table->dropColumn('shipping_is_business');
            $table->dropColumn('shipping_first_name');
            $table->dropColumn('shipping_last_name');
            $table->dropColumn('shipping_address1');
            $table->dropColumn('shipping_address2');
            $table->dropColumn('shipping_city');
            $table->dropColumn('shipping_state');
            $table->dropColumn('shipping_zip');
            $table->dropColumn('shipping_country');
            $table->dropColumn('shipping_phone');
            $table->dropColumn('shipping_EIN_number');
            $table->dropColumn('is_lift_gate_delivery');
            $table->dropColumn('is_inside_delivery');
            $table->dropColumn('is_appointment_request');
            $table->dropColumn('is_notify_request');
            $table->dropColumn('delivery_date');
            $table->dropColumn('special_delivery_instruction');
            $table->dropColumn('billing_first_name');
            $table->dropColumn('billing_last_name');
            $table->dropColumn('billing_address1');
            $table->dropColumn('billing_address2');
            $table->dropColumn('billing_city');
            $table->dropColumn('billing_state');
            $table->dropColumn('billing_zip');
            $table->dropColumn('billing_country');
            $table->dropColumn('billing_phone');

        });
    }
}
