<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertPusherExtentionRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('extensions')->insert(
            array(
                'act'=>'pusher',
                'name'=>'Pusher',
                'description'=>'Key location is shown bellow',
                'image'=>'hotjar.png',
                'shortcode'=>'{"PUSHER_APP_ID":{"title":"PUSHER_APP_ID","value":"------"},"PUSHER_APP_KEY":{"title":"PUSHER_APP_KEY","value":"------"},"PUSHER_APP_SECRET":{"title":"PUSHER_APP_SECRET","value":"------"},"PUSHER_APP_CLUSTER":{"title":"PUSHER_APP_CLUSTER","value":"------"}}',
                'support'=>'HotJar-Support.png',
                'status'=>'0',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
