<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeUserIdNullableInPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable()->change();
            $table->dropForeign('payments_event_id_foreign');
            $table->dropIndex('payments_event_id_index');
        }   );
        Schema::table('payments', function (Blueprint $table) {
            $table->unsignedBigInteger('event_id')->change();
            $table->string('event_type')->default(\App\Models\Event::class);
            $table->index(['event_id', 'event_type']);
        });
        Schema::table('fees', function (Blueprint $table) {
            $table->string('event_type')->default(\App\Models\Event::class);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('payments_user_id_foreign');
            $table->dropIndex('payments_user_id_index');
        });
        Schema::table('payments', function (Blueprint $table) {

            $table->unsignedBigInteger('user_id')->nullable(false)->change();
            $table->dropIndex('payments_event_id_event_type_index');
            $table->dropColumn('event_type');
            $table->integer('event_id')->nullable()->change();
            $table->foreign('event_id')->references('id')->on('events');
            $table->index('event_id');
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->index('user_id');
        });
        Schema::table('fees', function (Blueprint $table) {
            $table->dropColumn('event_type');
        });
    }
}
