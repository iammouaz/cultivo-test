<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogoToOfferSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_sheets', function (Blueprint $table) {
            $table->string('banner_logo')->nullable()->after('image');
            $table->string('card_logo')->nullable()->after('banner_logo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_sheets', function (Blueprint $table) {
            $table->dropColumn('banner_logo');
            $table->dropColumn('card_logo');
        });
    }
}
