<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteToShippingRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_region', function (Blueprint $table) {
            $table->softDeletes();
            $table->string('event_type')->default(\App\Models\Event::class);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_region', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->dropColumn('event_type');
        });
    }
}
