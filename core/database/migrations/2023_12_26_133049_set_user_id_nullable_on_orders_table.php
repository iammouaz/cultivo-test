<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetUserIdNullableOnOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // set user_id nullable
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // set user_id not nullable
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('user_id')->nullable(false)->change();
        });
    }
}
