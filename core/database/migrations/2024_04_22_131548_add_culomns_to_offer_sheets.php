<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCulomnsToOfferSheets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_sheets', function (Blueprint $table) {
            $table->boolean('hero_show_action_name')->nullable()->default(0);
            $table->string('hero_text_color')->nullable();
            $table->string('hero_primary_button_color')->nullable();
            $table->string('hero_image_overlay')->nullable();
            $table->string('hero_outlined_button_color')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_sheets', function (Blueprint $table) {
            $table->dropColumn('hero_show_action_name');
            $table->dropColumn('hero_text_color');
            $table->dropColumn('hero_primary_button_color');
            $table->dropColumn('hero_image_overlay');
            $table->dropColumn('hero_outlined_button_color');
        });
    }
}
