<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateEventTypeInEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            DB::table('events')
            ->where('event_type','MCultivo Event')
            ->update([
            'event_type' => 'm_cultivo_event',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('events', function (Blueprint $table) {
//            DB::table('events')
//            ->where('event_type','m_cultivo_event')
//            ->update([
//            'event_type' => 'MCultivo Event',
//            ]);
//        });
    }
}
