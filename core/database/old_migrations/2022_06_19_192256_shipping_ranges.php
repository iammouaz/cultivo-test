<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ShippingRanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_ranges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('region_id');
            $table->decimal('from',8,2)->default(0);
            $table->text('up_to',8,2)->default(0);
            $table->text('cost')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_ranges');
    }
}
