<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->string('image');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('less_bidding_time');
            $table->decimal('deposit', 28, 2);
            $table->dateTime('max_end_date');
            $table->longText('agreement');
            $table->integer('EventClockStartOn');// 0  -> All product have a bid  ,  1 -> Manually
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
