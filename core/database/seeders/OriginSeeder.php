<?php

namespace Database\Seeders;

use App\Models\Origin;
use Illuminate\Database\Seeder;

class OriginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Origin::create([
            'name'=>'origin_1',
        ]);
        Origin::create([
            'name'=>'origin_2',
        ]);
        Origin::create([
            'name'=>'origin_3',
        ]);
    }
}
