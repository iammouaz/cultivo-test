<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Origin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.admin',
            'username' => 'admin',
            'password' => '$2y$10$ih6fVSlQAHiIhyUBj3PQYOktb3mvZ71ZTwE03E2031Acyi0uTu7IO',//123456789
            'email_verified_at' => '2022-07-23 13:16:10',
            'created_at' => '2022-07-23 13:16:10',
            'updated_at' => '2022-09-01 14:26:42',
        ]);
    }
}
