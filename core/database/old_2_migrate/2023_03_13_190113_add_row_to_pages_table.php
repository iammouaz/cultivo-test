<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRowToPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $buyers = \App\Models\Page::where('slug','buyers')->first();

        if(!$buyers){
            \Illuminate\Support\Facades\DB::insert("INSERT INTO `pages` (`id`, `name`, `slug`, `tempname`, `secs`, `is_default`, `created_at`, `updated_at`) VALUES (NULL, 'Buyers', 'buyers', 'templates.basic.', '[\"live_auction\"]', '1', '2020-10-22 03:14:53', '2022-06-29 13:24:55')");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
