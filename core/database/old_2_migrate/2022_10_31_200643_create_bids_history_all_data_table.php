<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBidsHistoryAllDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("Create ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW bids_history_all_data AS SELECT His.ID,His.user_id, His.product_id, products.event_id, U.firstname, U.lastname, U.company_name,  U.billing_country, U.billing_state, U.billing_city, U.billing_address_1, events.name AS event_name, products.name AS product_name, His.new_bid, His.previous_bid,  His.user_previous_bid, His.created_at, His.updated_at,Spec.value AS rank FROM bids_history AS His LEFT outer JOIN products ON His.product_id = products.id LEFT outer JOIN events ON products.event_id = events.id LEFT outer JOIN users AS U ON His.user_id = U.id LEFT outer JOIN countries  ON U.billing_country = countries.ID LEFT outer JOIN product_specification AS Spec ON products.id=Spec.product_id and Upper(spec_key)='RANK';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS Bids_history_All_Data');
    }
}
