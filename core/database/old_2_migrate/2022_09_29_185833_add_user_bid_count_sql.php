<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddUserBidCountSql extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE bids ADD COLUMN User_Bids_Count INT NOT NULL DEFAULT '1' AFTER user_updated;");

        DB::statement("DROP TRIGGER IF EXISTS bids_before_update;");
		
        DB::statement("UPDATE bids 
						SET User_Bids_Count = 
							(SELECT COUNT(bids_history.id) FROM bids_history 
								WHERE bids_history.user_id = bids.user_id
								AND bids_history.product_id = bids.product_id
								GROUP BY bids_history.user_id , bids_history.product_id);");

        DB::statement("CREATE TRIGGER bids_before_update   BEFORE UPDATE ON bids   FOR EACH ROW 
		BEGIN
			DECLARE last_Bid DECIMAL (28,2);
			DECLARE user_last_Bid DECIMAL (28,2);
						
			IF (NEW.prev_amount = 1) THEN
				SET NEW.User_Bids_Count = OLD.User_Bids_Count +1;

				set last_Bid = (SELECT IFNULL(MAX(amount),0) FROM bids WHERE product_id = NEW.product_id);
				set user_last_Bid = (SELECT IFNULL(MAX(amount),0) FROM bids WHERE product_id = NEW.product_id AND user_id=NEW.user_id);
				INSERT INTO bids_history
							(product_id, user_id, new_bid, previous_bid, user_previous_bid)
						VALUES 
							(NEW.product_id, NEW.user_id, NEW.amount, last_bid, user_last_bid);
			ELSE 
				IF (NEW.prev_amount = 0) THEN
						SET NEW.User_Bids_Count = OLD.User_Bids_Count -1;
				END IF;
			END IF;
		END");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
