<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertExchangeRateExtentionRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('extensions')->insert(
            array(
                'act'=>'exchange_rate',
                'name'=>'Exchange Rate',
                'description'=>'Key location is shown bellow',
                'image'=>'exchange.png',
                'script'=>'',
                'shortcode'=>'{"app_key":{"title":"App Key","value":"------"}}',
                'support'=>'exchange.png',
                'status'=>'0',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
