<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('max_auto_bid_price', 10, 2)->nullable()->after('price');
            $table->integer('max_auto_bid_steps')->unsigned()->nullable()->after('max_auto_bid_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('max_auto_bid_price');
            $table->dropColumn('max_auto_bid_steps');
        });
    }
}
