<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `merchants` MODIFY `email` varchar(40) NULL');
        DB::statement('ALTER TABLE `merchants` MODIFY `mobile` varchar(40) NULL');
        DB::statement('ALTER TABLE `merchants` MODIFY `country_code`  varchar(40) NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
