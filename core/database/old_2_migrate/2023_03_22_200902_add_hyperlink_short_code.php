<?php

use App\Models\EmailTemplate;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddHyperlinkShortCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $email_template = EmailTemplate::where('act', 'Bidder_Approval')->first();
        if ($email_template) {
            DB::table('email_sms_templates')->where('act', 'Bidder_Approval')->update(
                array(
                    'shortcodes' => '{"event": "Your Event Name","hyperlink to login page" :"login link" }'
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
