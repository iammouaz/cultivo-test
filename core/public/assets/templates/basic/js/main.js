/* Start Constants */
const AJAX_TIMEOUT = 8000000; //set long timeout for ajax requests in case of network problem todo: this must be handled in a better way
/* End Constants */
(function ($) {
    "user strict";
    $(window).on("load", function () {
        try {
            $(".preloader").fadeOut(1000);
            var img = $(".bg_img");
            img.css("background-image", function () {
                var bg = "url(" + $(this).data("background") + ")";
                return bg;
            });
            galleryMasonary();
        } catch (error) {
            Sentry.captureException(error);
        }
    });

    function galleryMasonary() {
        $(".quick-banner-wrapper").isotope({
            itemSelector: ".quick-banner-item",
            horizontalOrder: true,
            masonry: {
                columnWidth: 0,
            },
        });
    }

    // Scroll To Top
    var scrollTop = $(".scrollToTop");
    $(window).on("scroll", function () {
        if ($(this).scrollTop() < 500) {
            scrollTop.removeClass("active");
        } else {
            scrollTop.addClass("active");
        }
    });
    //Click event to scroll to top
    scrollTop.on("click", function () {
        $("html, body").animate(
            {
                scrollTop: 0,
            },
            500
        );
        return false;
    });

    //Header Bar (Hamburger icon button)
    $(".header-bar").on("click", function () {
        $(this).toggleClass("active");
        $(".menu-area, .dashboard__sidebar").toggleClass("active");
    });

    $(".menu-close").on("click", function () {
        $(".menu-area, .header-bar").removeClass("active");
    });

    $(".overlay").on("click", function () {
        $(
            ".dashboard-menu, .header-bar, .dashboard__sidebar, .menu-area"
        ).removeClass("active");
    });

    $(".close-sidebar").on("click", function () {
        $(".dashboard-menu, .header-bar, .dashboard__sidebar").removeClass(
            "active"
        );
    });

    $(".faq__wrapper .faq__title").on("click", function (e) {
        try {
            var element = $(this).parent(".faq__item");
            if (element.hasClass("open")) {
                element.removeClass("open");
                element.find(".faq__content").removeClass("open");
                element.find(".faq__content").slideUp(200, "swing");
            } else {
                element.addClass("open");
                element.children(".faq__content").slideDown(200, "swing");
                element
                    .siblings(".faq__item")
                    .children(".faq__content")
                    .slideUp(200, "swing");
                element.siblings(".faq__item").removeClass("open");
                element
                    .siblings(".faq__item")
                    .find(".faq__title")
                    .removeClass("open");
                element
                    .siblings(".faq__item")
                    .find(".faq__content")
                    .slideUp(200, "swing");
            }
        } catch (error) {
            Sentry.captureException(error);
        }
    });
})(jQuery);
function showEventRegisterModal(eventId, eventType = null, bidderUrl = null) {
    try {
        if (eventId) {
            const formElem = $("#auctionRegisterForm");
            const routeUrl = formElem.attr("data-action");
            formElem.attr("action", routeUrl + "/" + eventId);
            const linkElem = $("#termsAndConditionsLink");
            const termsAndConditionsUrl = linkElem.attr("data-href");
            linkElem.attr("href", termsAndConditionsUrl + "/" + eventId);
            if (eventType) {
                $("#auctionRegisterForm #agree_ace_modal").attr(
                    "style",
                    "display: none !important"
                );
                $("#auctionRegisterForm #agree_mc_modal").attr(
                    "style",
                    "display: none !important"
                );
                if (eventType == "ace_event") {
                    //agree_ace_modal
                    $("#auctionRegisterForm #agree_ace_modal").attr(
                        "style",
                        "display: block !important"
                    );
                    $(
                        "#auctionRegisterForm #agree_ace_modal #bidder_agreement_a_tag"
                    ).attr("href", bidderUrl);
                } else {
                    //agree_mc_modal
                    $("#agree_mc_modal").attr(
                        "style",
                        "display: block !important"
                    );
                }
            }
            $("#auctionRegisterModal").modal("show");
        }
    } catch (error) {
        Sentry.captureException(error);
    }
}
// deal with Cookies
function setCookie(name, value, daysToLive) {
    try {
        const date = new Date();
        date.setTime(date.getTime() + daysToLive * 24 * 60 * 60 * 1000);
        let expires = "expires=" + date.toUTCString();
        document.cookie = `${name}=${value}; ${expires}; path=/`;
    } catch (error) {
        Sentry.captureException(error);
    }
}
function getCookie(name) {
    try {
        const cookieDecoded = decodeURIComponent(document.cookie);
        const cookiesArray = cookieDecoded.split("; ");
        let result = "";
        cookiesArray.forEach((cookie) => {
            if (cookie?.trim()?.indexOf(name) == 0) {
                result = cookie.substring(name.length + 1);
            }
        });
        return result;
    } catch (error) {
        Sentry.captureException(error);
    }
}
function deleteCookie(name) {
    setCookie(name, null, null);
}
function showFatalErrorOverlay(error = null) {
    error && console.error(error);
    const fatalErrorOverlayElement = document.querySelector(
        ".fatal-error-overlay"
    );
    fatalErrorOverlayElement.classList.remove("d-none");
    error && Sentry.captureException(error);
}
function ajaxErrorHandler(jqXHR, errorThrown) {
    var status = jqXHR.status;
    if (status === 0) {
        iziToast.info({
            title: "Network Problem",
            message: "Please check your network connection and try again",
            position: "topRight",
        });
    } else if (status >= 400 && status < 500) {
        var error = jqXHR.responseJSON.error;
        var errors = jqXHR.responseJSON.errors;
        if (error) {
            iziToast.error({
                title: "Failed",
                message: error,
                position: "topRight",
            });
        } else if (errors) {
            var messages = Object.values(errors)?.shift() || [];
            for (var message of messages) {
                iziToast.error({
                    title: "Failed",
                    message: message,
                    position: "topRight",
                });
            }
        } else {
            var message = jqXHR.responseJSON?.message || errorThrown;
            iziToast.error({
                title: "Failed",
                message: message,
                position: "topRight",
            });
        }
    } else {
        showFatalErrorOverlay();
        Sentry.captureException(jqXHR);
    }
}

function setResponsiveImage(
    largeImage,
    mediumImage,
    smallImage,
    imageElement,
    isImg = false
) {
    // Get the screen width
    const screenWidth =
        window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;

    // Define the breakpoints for different screen sizes
    const largeScreenBreakpoint = 1200;
    const mediumScreenBreakpoint = 768;

    // Choose the appropriate image URL based on the screen size
    let imageUrl;
    if (screenWidth >= largeScreenBreakpoint) {
        imageUrl = largeImage;
    } else if (screenWidth >= mediumScreenBreakpoint) {
        imageUrl = mediumImage;
    } else {
        imageUrl = smallImage;
    }

    // Set the image source
    if (isImg) {
        imageElement.src = imageUrl;
    } else {
        if(imageElement?.style) {
            imageElement.style.backgroundImage = `url(${imageUrl})`;
        }
    }
}

function rgbaStringToObject(rgbaStr) {
    // Use a regular expression to extract the numeric values from the string.
    const match = rgbaStr.match(
        /rgba?\((\d+),\s*(\d+),\s*(\d+),?\s*(\d*\.?\d+)?\)/i
    );

    if (match) {
        return {
            r: parseInt(match[1], 10),
            g: parseInt(match[2], 10),
            b: parseInt(match[3], 10),
            a: match[4] !== undefined ? parseFloat(match[4]) : 1, // Default alpha to 1 if not specified
        };
    } else {
        throw new Error("Invalid RGBA color string");
    }
}

function convertRGBAToRGBAString(rgb, alpha) {
    return `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${alpha})`;
}

function addGlassEffect(rgbColor, element = undefined) {
    const colorTrigger =
        element ?? colorInputContainer.find(".color-panel-trigger");
    // Convert the base color to two different RGBA strings with varying alpha values for the gradient
    const colorTop = convertRGBAToRGBAString(rgbColor, 0.5); // 50% transparency
    const colorBottom = convertRGBAToRGBAString(rgbColor, 0.1); // 10% transparency

    // Update the CSS styles dynamically with the new color values
    colorTrigger.css(
        "border-bottom",
        `1px solid rgba(${rgbColor.r}, ${rgbColor.g}, ${rgbColor.b}, 1)`
    );
    colorTrigger.css(
        "background",
        `linear-gradient(180deg, ${colorTop} 24.31%, ${colorBottom} 90.92%)`
    );
    colorTrigger.css("box-shadow", "0px 4px 4px 0px rgba(0, 0, 0, 0.25)");
    colorTrigger.css("backdrop-filter", "blur(15px)");
}
